<?php
namespace Avris\Micrus;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;
use Avris\Micrus\Tool\Config\ParametersProvider;

final class MicrusModule implements ModuleInterface, ParametersProvider
{
    use ModuleTrait;

    /**
     * @codeCoverageIgnore
     */
    public function getParametersDefaults(): array
    {
        return [
            'APP_ENV' => 'dev',
            'APP_DEBUG' => '1',
            'APP_SECRET' => '%generate(secret)%',

            'URL_SCHEME' => 'http',
            'URL_HOST' => 'localhost',
            'URL_BASE' => '',
        ];
    }
}
