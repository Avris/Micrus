<?php
namespace Avris\Micrus\Model;

use Avris\Container\ContainerInterface;
use Avris\Bag\QueueBag;

class ContainerMatchProvider implements MatchProvider
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function supports(\ReflectionParameter $parameter): bool
    {
        return $parameter->getClass() && $this->container->has($parameter->getClass()->getName());
    }

    public function fetch(\ReflectionParameter $parameter, QueueBag $tags)
    {
        return $this->container->get($parameter->getClass()->getName());
    }
}
