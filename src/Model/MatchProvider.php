<?php
namespace Avris\Micrus\Model;

use Avris\Bag\QueueBag;

interface MatchProvider
{
    public function supports(\ReflectionParameter $parameter): bool;

    public function fetch(\ReflectionParameter $parameter, QueueBag $tags);
}
