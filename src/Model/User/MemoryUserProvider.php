<?php
namespace Avris\Micrus\Model\User;

use Avris\Bag\Bag;

class MemoryUserProvider implements UserProviderInterface
{
    /** @var Bag */
    private $users;

    public function __construct(Bag $configSecurity_users)
    {
        $this->users = $configSecurity_users;
    }

    public function getUser(string $identifier): ?UserInterface
    {
        $userData = $this->users->get($identifier);

        return $userData ? new MemoryUser($identifier, $userData) : null;
    }
}
