<?php
namespace Avris\Micrus\Model\User;

interface UserInterface
{
    public function getIdentifier();

    /**
     * @return iterable|AuthenticatorInterface[]
     */
    public function getAuthenticators(string $type = null): iterable;

    public function createAuthenticator(
        string $type,
        string $payload,
        \DateTime $validUntil = null
    ): AuthenticatorInterface;

    public function getRoles(): iterable;

    public function hasRole(string $role): bool;

    public function isAdmin(): bool;

    public function __toString(): string;
}
