<?php
namespace Avris\Micrus\Model\User;

class MemoryAuhtenticator implements AuthenticatorInterface
{
    /** @var string */
    protected $type;

    /** @var string */
    protected $payload;

    /** @var bool */
    protected $valid;

    /**
     * @param string $type
     * @param string $payload
     */
    public function __construct($type, $payload)
    {
        $this->type = $type;
        $this->payload = $payload;
        $this->valid = true;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getPayload()
    {
        return $this->payload;
    }

    public function isValid()
    {
        return $this->valid;
    }

    public function invalidate()
    {
        $this->valid = false;
    }
}
