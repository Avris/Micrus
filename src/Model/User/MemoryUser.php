<?php
namespace Avris\Micrus\Model\User;

class MemoryUser implements UserInterface, \ArrayAccess
{
    /** @var string */
    protected $identifier;

    /** @var string[] */
    protected $roles;

    /** @var AuthenticatorInterface[] */
    protected $authenticators = [];

    /** @var array */
    protected $data;

    public function __construct(string $identifier, array $data)
    {
        $this->identifier = $identifier;

        $this->roles = isset($data['roles']) ? $data['roles'] : [];
        if (isset($data['authenticators'])) {
            foreach ($data['authenticators'] as $type => $payload) {
                $this->authenticators[] = new MemoryAuhtenticator($type, $payload);
            }
        }

        $this->data = $data;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function getRoles(): iterable
    {
        return $this->roles;
    }

    public function hasRole(string $role): bool
    {
        return in_array($role, $this->roles);
    }

    public function isAdmin(): bool
    {
        return $this->hasRole('ROLE_ADMIN');
    }

    public function __toString(): string
    {
        return $this->identifier;
    }

    public function getAuthenticators(string $type = null): iterable
    {
        if (!$type) {
            return $this->authenticators;
        }

        $auths = [];
        foreach ($this->authenticators as $auth) {
            if ($auth->getType() === $type && $auth->isValid()) {
                $auths[] = $auth;
            }
        }

        return $auths;
    }

    public function createAuthenticator(
        string $type,
        string $payload,
        \DateTime $validUntil = null
    ): AuthenticatorInterface {
        $auth = new MemoryAuhtenticator($type, $payload);
        $this->authenticators[] = $auth;

        return $auth;
    }

    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }
}
