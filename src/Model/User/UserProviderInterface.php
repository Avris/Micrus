<?php
namespace Avris\Micrus\Model\User;

interface UserProviderInterface
{
    public function getUser(string $identifier): ?UserInterface;
}
