<?php
namespace Avris\Micrus\Model\User;

interface AuthenticatorInterface
{
    /**
     * @return string
     */
    public function getType();

    /**
     * @return mixed
     */
    public function getPayload();

    /**
     * @return bool
     */
    public function isValid();

    /**
     * @return $this
     */
    public function invalidate();
}
