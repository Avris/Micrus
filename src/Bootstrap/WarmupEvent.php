<?php
namespace Avris\Micrus\Bootstrap;

use Avris\Container\ContainerInterface;
use Avris\Dispatcher\Event;

final class WarmupEvent extends Event
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getName(): string
    {
        return 'warmup';
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function setValue($value): Event
    {
        return $this;
    }

    public function getValue()
    {
        return null;
    }
}
