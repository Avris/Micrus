<?php
namespace Avris\Micrus\Bootstrap;

use Avris\Bag\Bag;
use Avris\Container\Container;
use Avris\Container\ContainerCompiler;
use Avris\Container\ContainerInterface;
use Avris\Container\Service\ServiceDefinition;
use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Http\Response\ResponseInterface;
use Avris\Micrus\Exception\Handler\ErrorHandler;
use Avris\Micrus\Exception\LogLevelException;
use Avris\Micrus\MicrusModule;
use Avris\Micrus\Tool\Cache\CacherInterface;
use Avris\Micrus\Tool\Config\ArrayHelper;
use Avris\Micrus\Tool\Config\ConfigBag;
use Avris\Micrus\Tool\Config\ConfigLoader;
use Avris\Micrus\Tool\DefaultLogger;
use Avris\Micrus\Tool\Dumper;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

abstract class AbstractApp
{
    const VERSION = '4.0.0';

    const PROJECT_DIR = 'PROJECT_DIR';
    const PUBLIC_DIR = 'PUBLIC_DIR';
    const CACHE_DIR = 'CACHE_DIR';
    const LOGS_DIR = 'LOGS_DIR';

    /** @var ConfigBag */
    protected $configBag;

    /** @var ModuleInterface[] */
    protected $modules = [];

    /** @var ContainerInterface */
    protected $container;

    /** @var CacherInterface */
    protected $cacher;

    /** @var EventDispatcherInterface */
    protected $dispatcher;

    public function __construct(string $env, bool $debug)
    {
        $this->configBag = new ConfigBag($env, $debug, $this->buildProjectDir(), $this->defineDirs($env));

        $this->modules['Avris\\Micrus'] = new MicrusModule;
        foreach ($this->registerModules() as $module) {
            $this->modules[$module->getName()] = $module;
        }

        $this->cacher = $this->buildCacher();
    }

    public function warmup(): ContainerInterface
    {
        $this->container = $this->compileContainer(
            $this->warmupContainer($this->buildContainer())
        );
        $this->extendConfig();
        $this->registerSubscribers();
        $this->dispatcher->trigger(new WarmupEvent($this->container));

        return $this->container;
    }

    public function handle(RequestInterface $request = null): ResponseInterface
    {
        $handler = ErrorHandler::register(
            new DefaultLogger(),
            new Dumper($this->configBag->isDebug())
        );

        try {
            if (!$this->container) {
                $this->container = $this->warmup();
            }
        } catch (\Throwable $e) {
            return $handler->handleException(new LogLevelException($e, LogLevel::CRITICAL));
        } finally {
            ErrorHandler::unregister();
        }

        $this->container->setWithAlias($request, RequestInterface::class);
        $this->container->get(RequestProviderInterface::class)->reset($request);

        $handler = ErrorHandler::register(
            $this->container->get(LoggerInterface::class),
            new Dumper($this->configBag->isDebug()),
            $this->dispatcher
        );

        try {
            return $this->container->get(RequestHandlerInterface::class)->handle($request);
        } catch (\Throwable $e) {
            return $handler->handleException($e);
        } finally {
            ErrorHandler::unregister();
        }
    }

    private function warmupContainer(ContainerInterface $container): ContainerInterface
    {
        $container
            ->set('env', $this->configBag->getEnv())
            ->set('debug', $this->configBag->isDebug())
            ->set('projectDir', $this->configBag->getDir())
            ->setWithAlias($this, self::class)
            ->setWithAlias($this->cacher, CacherInterface::class)
            ->setWithAlias($this->cacher->getPool(), CacheItemPoolInterface::class)
        ;

        foreach ($this->modules as $module) {
            $container->set(get_class($module), $module, ['module']);
        }

        $container->set('config', $this->loadConfig());

        return $container;
    }

    public function terminate()
    {
        if ($this->dispatcher) {
            $this->dispatcher->trigger(new TerminateEvent($this->container));
        }
    }

    public function getContainer(): ?ContainerInterface
    {
        return $this->container;
    }

    /**
     * @return ModuleInterface[]
     */
    public function getModules(): array
    {
        return $this->modules;
    }

    public function getEnv(): string
    {
        return $this->configBag->getEnv();
    }

    /**
     * @return ModuleInterface[]
     */
    abstract protected function registerModules(): iterable;

    abstract protected function buildProjectDir(): string;

    protected function defineDirs(string $env)
    {
        return [
            self::PUBLIC_DIR => 'public',
            self::CACHE_DIR => 'var/cache/' . $env,
            self::LOGS_DIR => 'var/logs',
        ];
    }

    abstract protected function buildCacher(): CacherInterface;

    protected function buildContainer()
    {
        return new Container($this->configBag);
    }

    protected function loadConfig(): Bag
    {
        return $this->cacher->cache('config', function () {
            $loader = new ConfigLoader($this->configBag);

            foreach ($this->modules as $module) {
                $loader->load($module, 'config');
                $loader->load($module, 'config/' . $this->configBag->getEnv());
            }

            return $loader->get();
        });
    }

    protected function compileContainer(ContainerInterface $container): ContainerInterface
    {
        $definitions = $this->cacher->cache('container', function () use ($container) {
            return (new ContainerCompiler(
                $container, // @codeCoverageIgnore
                $container->get('config.services'),
                $container->get('config.container.instanceof')
            ))->compile();
        });

        /** @var ServiceDefinition $definition */
        foreach ($definitions as $name => $definition) {
            if (!$container->has($name)) {
                $container->setDefinition($name, $definition);
            }
        }

        return $container;
    }

    private function extendConfig()
    {
        $extendedConfig = $this->cacher->cache('configExtended', function () {
            $helper = new ArrayHelper();
            $config = $this->container->get('config')->all();
            /** @var ConfigExtension $extension */
            foreach ($this->container->getByTag('configExtension') as $extension) {
                $config = $helper->merge($config, $extension->extendConfig());
            }

            return new Bag($config);
        });

        $this->container->set('config', $extendedConfig);
    }

    private function registerSubscribers()
    {
        $this->dispatcher = $this->container->get(EventDispatcherInterface::class);
        foreach ($this->container->getByTag('subscriber') as $subscriber) {
            $this->dispatcher->registerSubscriber($subscriber);
        }
    }
}
