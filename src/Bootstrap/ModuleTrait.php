<?php
namespace Avris\Micrus\Bootstrap;

trait ModuleTrait
{
    /** @var string */
    private $name;

    /** @var string */
    private $dir;

    public function getName(): string
    {
        return $this->name ?? $this->name = substr(static::class, 0, strrpos(static::class, '\\'));
    }

    public function getDir(): string
    {
        return $this->dir ?? $this->dir = dirname(dirname((new \ReflectionObject($this))->getFileName()));
    }
}
