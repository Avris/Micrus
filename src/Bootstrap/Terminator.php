<?php
namespace Avris\Micrus\Bootstrap;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Micrus\Controller\ResponseEvent;

class Terminator implements EventSubscriberInterface
{
    /** @var callable[] */
    protected $callables = [];

    public function attach(callable $callable): self
    {
        $this->callables[] = $callable;

        return $this;
    }

    public function onResponse(ResponseEvent $event)
    {
        if (count($this->callables)) {
            $event->getResponse()->continueProcessing();
        }
    }

    public function onTerminate(TerminateEvent $event)
    {
        foreach ($this->callables as $callable) {
            $callable($event);
        }
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscribedEvents(): iterable
    {
        yield 'response:999' => [$this, 'onResponse'];
        yield 'terminate' => [$this, 'onTerminate'];
    }
}
