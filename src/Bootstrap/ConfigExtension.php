<?php
namespace Avris\Micrus\Bootstrap;

interface ConfigExtension
{
    public function extendConfig(): array;
}
