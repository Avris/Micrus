<?php
namespace Avris\Micrus\Bootstrap;

use Avris\Http\Request\RequestInterface;
use Avris\Http\Response\ResponseInterface;

interface RequestHandlerInterface
{
    public function handle(RequestInterface $request): ResponseInterface;
}
