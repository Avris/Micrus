<?php
namespace Avris\Micrus\Bootstrap;

interface ModuleInterface
{
    public function getName(): string;

    public function getDir(): string;
}
