<?php
namespace Avris\Micrus\Bootstrap;

use Avris\Container\ContainerInterface;
use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Micrus\Controller\ArgumentsResolverInterface;
use Avris\Micrus\Controller\ControllerEvent;
use Avris\Micrus\Controller\ControllerResolverInterface;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Http\Response\ResponseInterface;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Controller\ViewEvent;
use Avris\Micrus\Controller\RequestEvent;
use Avris\Micrus\Controller\ResponseEvent;
use Avris\Micrus\Exception\Http\NotFoundHttpException;
use Avris\Micrus\Exception\InvalidArgumentException;
use Psr\Log\LoggerInterface;

class RequestHandler implements RequestHandlerInterface
{
    /** @var ContainerInterface */
    protected $container;

    /** @var EventDispatcherInterface */
    protected $dispatcher;

    /** @var LoggerInterface */
    protected $logger;

    /** @var RequestProviderInterface */
    protected $requestProvider;

    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        LoggerInterface $logger,
        RequestProviderInterface $requestProvider
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
        $this->requestProvider = $requestProvider;
    }

    public function handle(RequestInterface $request): ResponseInterface
    {
        $this->logger->info(sprintf('Request: %s', $request));

        $routeMatch = $this->requestProvider->getRouteMatch();
        if (!$routeMatch) {
            throw new NotFoundHttpException(sprintf('Route for "%s" not found', $request));
        }

        $request->getQuery()->add($routeMatch->getQuery());

        $response = $this->doHandle($request, $routeMatch);

        /** @var ResponseInterface $response */
        $response = $this->dispatcher->trigger(new ResponseEvent($response));

        $this->logger->info(sprintf('Response status %s', $response->getStatus()));

        return $response;
    }

    private function doHandle(RequestInterface $request, RouteMatch $routeMatch): ResponseInterface
    {
        $response = $this->dispatcher->trigger(new RequestEvent($this->container, $request));

        if ($response instanceof ResponseInterface) {
            return $response;
        }

        $this->logger->debug(sprintf(
            'Calling controller %s with params [%s]',
            $routeMatch->getTarget(),
            join(', ', $routeMatch->getTags())
        ));

        $controller = $this->container->get(ControllerResolverInterface::class)
            ->resolveController($routeMatch);
        $arguments = $this->container->get(ArgumentsResolverInterface::class)
            ->resolveArguments($controller, $routeMatch);

        list($response, $controller, $arguments) = $this->dispatcher->trigger(
            new ControllerEvent($request, $routeMatch->getTarget(), $controller, $arguments)
        );

        if ($response instanceof ResponseInterface) {
            return $response;
        }

        $response = call_user_func_array($controller, $arguments);

        if ($response instanceof ResponseInterface) {
            return $response;
        }

        $response = $this->dispatcher->trigger(new ViewEvent($response));

        if ($response instanceof ResponseInterface) {
            return $response;
        }

        throw new InvalidArgumentException('Controller must return an instance of ResponseInterface');
    }
}
