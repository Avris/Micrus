<?php
namespace Avris\Micrus\View;

use Avris\Micrus\Exception\NotFoundException;

class PhpEngine implements EngineInterface
{
    /** @var string[] */
    protected $dirs;

    /** @var TemplateHelper */
    protected $helper;

    /** @var AppVar */
    protected $app;

    public function __construct(array $dirs, TemplateHelper $helper)
    {
        $this->dirs = $dirs;
        $this->helper = $helper;
        $this->app = $helper->getApp();
    }

    public function render(string $template, array $vars): string
    {
        extract($vars);
        $_path = $this->getTemplatePath($template);
        unset($vars, $template);

        ob_start();
        try {
            require $_path;
            return ob_get_clean();
        } catch (\Exception $e) {
            ob_get_clean();
            throw $e;
        }
    }

    public function hasTemplate(string $template): bool
    {
        try {
            $this->getTemplatePath($template);
            return true;
        } catch (NotFoundException $e) {
            return false;
        }
    }

    private function partial($template, $vars = [])
    {
        extract($vars);
        $_path = $this->getTemplatePath($template);
        unset($vars, $template);

        ob_start();
        require $_path;
        return ob_get_clean();
    }

    /**
     * @param string $view
     * @return string
     * @throws NotFoundException
     */
    protected function getTemplatePath($view)
    {
        if (strpos($view, '.') === false) {
            $view .= '.phtml';
        }

        foreach ($this->dirs as $dir) {
            $path = $dir . '/' . $view;
            if (file_exists($path)) {
                return $path;
            }
        }

        throw new NotFoundException(sprintf(
            'Template "%s" cannot be found in any of the registered template directories',
            $view
        ));
    }

    protected function route(string $route, array $params = []): string
    {
        return $this->helper->route($route, $params);
    }

    protected function routeExists(string $route): bool
    {
        return $this->helper->routeExists($route);
    }

    protected function asset(string $asset, $cacheBooster = false): string
    {
        return $this->helper->asset($asset, $cacheBooster);
    }

    protected function isGranted($requiredRoles): bool
    {
        return $this->helper->isGranted($requiredRoles);
    }

    public function canAccess(string $check, $object = null): bool
    {
        return $this->helper->canAccess($check, $object);
    }
}
