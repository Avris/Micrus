<?php

namespace Avris\Micrus\View;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Container\Service\Resolver;

class TemplateDirsResolver implements Resolver
{
    /** @var ModuleInterface[] */
    private $modules;

    public function __construct(array $modules)
    {
        $this->modules = array_reverse($modules);
    }

    public function resolve()
    {
        $dirs = [];

        foreach ($this->modules as $module) {
            if (is_dir($dir = $module->getDir() . '/templates')) {
                $dirs[] = $dir;
            }
        }

        return $dirs;
    }
}
