<?php
namespace Avris\Micrus\View;

use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\GuardianInterface;
use Avris\Micrus\Tool\Security\RoleCheckerInterface;
use Avris\Micrus\Tool\Security\SecurityManagerInterface;

class TemplateHelper
{
    /** @var AppVar */
    protected $app;

    /** @var RouterInterface */
    protected $router;

    /** @var SecurityManagerInterface */
    protected $securityManager;

    /** @var RoleCheckerInterface */
    protected $roleChecker;

    /** @var GuardianInterface */
    protected $guardian;

    /** @var AssetProvider */
    protected $assetProvider;

    public function __construct(
        RouterInterface $router,
        SecurityManagerInterface $securityManager,
        RoleCheckerInterface $roleChecker,
        GuardianInterface $guardian,
        FlashBag $flashBag,
        RequestProviderInterface $requestProvider,
        SessionInterface $session,
        AssetProvider $assetProvider
    ) {
        $this->router = $router;
        $this->securityManager = $securityManager;
        $this->roleChecker = $roleChecker;
        $this->guardian = $guardian;
        $this->app = new AppVar($securityManager, $flashBag, $requestProvider, $session);

        $this->assetProvider = $assetProvider;
    }

    public function getApp(): AppVar
    {
        return $this->app;
    }

    public function route($route, array $params = []): string
    {
        return $this->router->generate($route, $params);
    }

    public function routeExists($route): bool
    {
        return $this->router->routeExists($route);
    }

    public function asset(string $asset, bool $cacheBooster = false): string
    {
        return $this->assetProvider->getLink($asset, $cacheBooster);
    }

    public function isGranted($requiredRoles): bool
    {
        return $this->roleChecker->isUserGranted(
            $requiredRoles, // @codeCoverageIgnore
            $this->securityManager->getUser()
        );
    }

    public function canAccess(string $check, $object = null): bool
    {
        return $this->guardian->check(
            $this->securityManager->getUser(),
            $check, // @codeCoverageIgnore
            $object
        );
    }
}
