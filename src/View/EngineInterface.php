<?php
namespace Avris\Micrus\View;

interface EngineInterface
{
    public function render(string $template, array $vars): string;

    public function hasTemplate(string $template): bool;
}
