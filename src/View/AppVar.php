<?php
namespace Avris\Micrus\View;

use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\SecurityManagerInterface;

class AppVar
{
    /** @var SecurityManagerInterface */
    private $securityManager;

    /** @var FlashBag */
    private $flashBag;

    /** @var SessionInterface */
    private $session;

    /** @var RequestProviderInterface */
    private $requestProvider;

    public function __construct(
        SecurityManagerInterface $securityManager,
        FlashBag $flashBag,
        RequestProviderInterface $requestProvider,
        SessionInterface $session
    ) {
        $this->securityManager = $securityManager;
        $this->flashBag = $flashBag;
        $this->requestProvider = $requestProvider;
        $this->session = $session;
    }

    public function getUser(): ?UserInterface
    {
        return $this->securityManager->getUser();
    }

    public function getFlashBag(): FlashBag
    {
        return $this->flashBag;
    }

    public function getRequest(): RequestInterface
    {
        return $this->requestProvider->getRequest();
    }

    public function getRouteMatch(): ?RouteMatch
    {
        return $this->requestProvider->getRouteMatch();
    }

    public function getSession(): SessionInterface
    {
        return $this->session;
    }
}
