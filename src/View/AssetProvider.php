<?php
namespace Avris\Micrus\View;

use Avris\Bag\Bag;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;

class AssetProvider
{
    /** @var string[] */
    private $assetsManifest;

    /** @var string */
    private $prefix;

    /** @var string */
    private $base;

    /** @var string */
    private $publicDir;

    public function __construct(Bag $configAssets, string $base, string $envPublicDir)
    {
        $this->assetsManifest = $this->readAssetsManifest($configAssets->get('manifest', ''));
        $this->prefix = $configAssets->get('prefixAll', '');
        $this->base = $base;
        $this->publicDir = $envPublicDir;
    }

    private function readAssetsManifest(string $manifestFilename): array
    {
        return file_exists($manifestFilename)
            ? json_decode(file_get_contents($manifestFilename), true)
            : [];
    }

    public function getLink(string $asset, bool $cacheBooster = false): string
    {
        $link = $this->assetsManifest[$this->prefix . $asset]
            ?? sprintf('%s/%s%s', $this->base, $this->prefix, $asset);

        if ($cacheBooster) {
            $path = $this->getPath($asset);

            $link .= file_exists($path)
                ? '?' . substr(md5(filemtime($path)), 0, 6)
                : '';
        }

        return $link;
    }

    public function getPath(string $asset): string
    {
        $filename = $this->assetsManifest[$this->prefix . $asset] ?? $this->prefix . $asset;
        if ($this->base && substr($filename, 0, strlen($this->base)) === $this->base) {
            $filename = substr($filename, strlen($this->base));
        }

        return strpos($filename, '://') === false
            ? $this->publicDir . '/' . $filename
            : $filename;
    }

    public function read(string $asset): string
    {
        return @file_get_contents($this->getPath($asset));
    }
}
