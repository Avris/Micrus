<?php
namespace Avris\Micrus\View;

use Avris\Micrus\Exception\NotFoundException;

class Templater implements TemplaterInterface
{
    /** @var EngineInterface[] */
    protected $engines;

    public function __construct(array $templateEngines)
    {
        $this->engines = $templateEngines;
    }

    public function render(string $template, array $vars): string
    {
        foreach ($this->engines as $engine) {
            if ($engine->hasTemplate($template)) {
                return $engine->render($template, $vars);
            }
        }

        throw new NotFoundException(sprintf(
            'Template "%s" not found',
            $template
        ));
    }

    public function hasTemplate(string $template): bool
    {
        foreach ($this->engines as $engine) {
            if ($engine->hasTemplate($template)) {
                return true;
            }
        }

        return false;
    }
}
