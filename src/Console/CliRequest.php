<?php
namespace Avris\Micrus\Console;

use Avris\Http\Request\FileBag;
use Avris\Http\Header\HeaderBag;
use Avris\Http\Request\RequestInterface;
use Avris\Bag\Bag;

class CliRequest implements RequestInterface
{
    /** @var Bag */
    protected $url;

    public function __construct(Bag $configUrl)
    {
        $this->url = $configUrl;
    }

    public function __toString(): string
    {
        return 'CLI';
    }

    public function getMethod(): string
    {
        return 'CLI';
    }

    public function isPost(): bool
    {
        return false;
    }

    public function getScheme(): string
    {
        return $this->url->get('scheme', 'http');
    }

    public function getHost(): string
    {
        return $this->url->get('host', 'localhost');
    }

    public function getAbsoluteBase(): string
    {
        return sprintf(
            '%s://%s',
            $this->getScheme(),
            $this->getHost()
        );
    }

    public function getBase(): string
    {
        return (string) $this->url->get('base');
    }

    public function getFrontController(): string
    {
        return '';
    }

    public function getUrl(): string
    {
        return '';
    }

    public function getCleanUrl(): string
    {
        return '';
    }

    public function getFullUrl(): string
    {
        return $this->getAbsoluteBase() . $this->getBase();
    }

    public function getIp(bool $safe = true): string
    {
        return '127.0.0.1';
    }

    public function isAjax(): bool
    {
        return false;
    }

    public function getQuery(): Bag
    {
        return new Bag();
    }

    public function getData(): Bag
    {
        return new Bag();
    }

    public function getCookies(): Bag
    {
        return new Bag();
    }

    public function getServer(): Bag
    {
        return new Bag();
    }

    public function getFiles(): FileBag
    {
        return new FileBag();
    }

    public function getHeaders(): HeaderBag
    {
        return new HeaderBag();
    }

    public function getBody(): string
    {
        return '';
    }

    public function getJsonBody()
    {
        return [];
    }
}
