<?php
namespace Avris\Micrus\Console\Command;

use Avris\Micrus\Tool\Security\CryptInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SecurityGenerateSymmericKeyCommand extends Command
{
    /** @var CryptInterface */
    private $crypt;

    public function __construct(CryptInterface $crypt)
    {
        $this->crypt = $crypt;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('security:keys:sym')
            ->setDescription('Generate a symmetric encryption key')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $secret = $this->crypt->generateSymmetricKey();
        $output->writeln('<info>' . $secret . '</info>');
    }
}
