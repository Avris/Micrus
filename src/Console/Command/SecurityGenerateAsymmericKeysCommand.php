<?php
namespace Avris\Micrus\Console\Command;

use Avris\Micrus\Tool\Security\CryptInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SecurityGenerateAsymmericKeysCommand extends Command
{
    /** @var CryptInterface */
    private $crypt;

    public function __construct(CryptInterface $crypt)
    {
        $this->crypt = $crypt;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('security:keys:asym')
            ->setDescription('Generate a pair of asymmetric encryption keys')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        list($privateKey, $publicKey) = $this->crypt->generateAsymmetricKeys();
        $output->writeln('<info>' . $privateKey . '</info>');

        $output->writeln('<info>' . $publicKey . '</info>');
    }
}
