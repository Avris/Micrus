<?php
namespace Avris\Micrus\Console\Command;

use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Micrus\Tool\Cache\CacheWarmupEvent;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CacheWarmupCommand extends Command
{
    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @var string */
    private $env;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        string $envAppEnv
    ) {
        $this->dispatcher = $eventDispatcher;
        $this->env = $envAppEnv;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('cache:warmup')
            ->setDescription('Warmup cache')
            ->addOption('optional', 'o', null, 'Include optional warmups')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->dispatcher->trigger(new CacheWarmupEvent($input->getOption('optional')));

        $output->writeln('Cache warmed up for env: <info>' . $this->env . '</info>');
    }
}
