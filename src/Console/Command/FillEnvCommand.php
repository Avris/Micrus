<?php
namespace Avris\Micrus\Console\Command;

use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Micrus\Tool\Config\ParametersProvider;
use Avris\Dotenv\Command\FillCommand;

class FillEnvCommand extends FillCommand
{
    /** @var AbstractApp */
    private $app;

    public function __construct(AbstractApp $app)
    {
        $this->app = $app;
        parent::__construct();
    }

    protected function getDefaults(): iterable
    {
        foreach ($this->app->getModules() as $module) {
            if ($module instanceof ParametersProvider) {
                yield $module->getName() => $module->getParametersDefaults();
            }
        }
    }
}
