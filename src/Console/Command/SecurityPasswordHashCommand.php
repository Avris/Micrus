<?php
namespace Avris\Micrus\Console\Command;

use Avris\Micrus\Tool\Security\CryptInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class SecurityPasswordHashCommand extends Command
{
    /** @var CryptInterface */
    private $crypt;

    public function __construct(CryptInterface $crypt)
    {
        $this->crypt = $crypt;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('security:password:hash')
            ->setDescription('Generate a hash for a given password')
            ->addArgument('password', InputArgument::OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $password = $input->getArgument('password')
            ?: $helper->ask($input, $output, (new Question('Please type a password to be hashed: '))->setHidden(true));

        $hashed = $this->crypt->passwordHash($password);

        $output->writeln('<info>' . $hashed . '</info>');
    }
}
