<?php
namespace Avris\Micrus\Console\Command;

use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Micrus\Tool\Cache\CacheClearEvent;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CacheClearCommand extends Command
{
    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @var string */
    private $env;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        string $envAppEnv
    ) {
        $this->dispatcher = $eventDispatcher;
        $this->env = $envAppEnv;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('cache:clear')
            ->setDescription('Clear cache')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->dispatcher->trigger(new CacheClearEvent());

        $output->writeln('Cache cleared for env: <info>' . $this->env . '</info>');
    }
}
