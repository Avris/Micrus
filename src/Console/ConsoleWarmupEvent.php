<?php
namespace Avris\Micrus\Console;

use Avris\Dispatcher\Event;

final class ConsoleWarmupEvent extends Event
{
    /** @var ConsoleApplication */
    private $app;

    public function __construct(ConsoleApplication $app)
    {
        $this->app = $app;
    }

    public function getName(): string
    {
        return 'consoleWarmup';
    }

    public function getApp(): ConsoleApplication
    {
        return $this->app;
    }

    public function setValue($value): Event
    {
        return $this;
    }

    public function getValue()
    {
        return null;
    }
}
