<?php
namespace Avris\Micrus\Console;

use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Container\ContainerInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;

class ConsoleApplication extends Application
{
    /** @var ContainerInterface */
    protected $container;

    public function __construct(AbstractApp $app)
    {
        parent::__construct('Micrus', AbstractApp::VERSION);

        $this->getDefinition()->addOptions([
            new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, '', $app->getEnv()),
            new InputOption('--no-debug', null, InputOption::VALUE_NONE)
        ]);

        $this->container = $app->warmup();

        /** @var Command $command */
        foreach ($this->container->getByTag('command') as $command) {
            $this->add($command);
        }

        $this->container->get(EventDispatcherInterface::class)->trigger(new ConsoleWarmupEvent($this));
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }
}
