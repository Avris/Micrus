<?php
namespace Avris\Micrus\Tool\Test;

use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Micrus\Exception\ConfigException;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Tool\DirectoryCleaner;
use PHPUnit\Framework\TestCase;

abstract class WebTestCase extends TestCase
{
    /** @var WebClient */
    protected static $client;

    /** @var string */
    private static $failDir;

    public static function init(AbstractApp $app, string $failDir = null)
    {
        static::$client = new WebClient($app);

        if ($failDir) {
            (new DirectoryCleaner())->emptyDir($failDir);
            if (!file_exists($failDir)) {
                mkdir($failDir, 0777, true);
            }
            self::$failDir = $failDir;
        }
    }

    public static function setUpBeforeClass()
    {
        if (!static::$client) {
            throw new ConfigException('You need to invoke WebTestCase::init() in your bootstrap file');
        }
    }

    protected function onNotSuccessfulTest(\Throwable $t)
    {
        if (static::$client->getResponse() && self::$failDir) {
            file_put_contents(
                sprintf('%s/%s.html', self::$failDir, str_replace(['::', '\\', '#'], '_', $this->toString())),
                static::$client->getResponse()->getContent()
            );
        }

        throw $t;
    }

    protected function assertUrl(string $expected)
    {
        $expected = sprintf('%s://%s%s', getenv('URL_SCHEME'), getenv('URL_HOST'), $expected);
        $actual = static::$client->getRequest()->getUri();

        $this->assertEquals(
            $expected, // @codeCoverageIgnore
            $actual, // @codeCoverageIgnore
            sprintf('Failed asserting that request URI %s matches expected %s', $actual, $expected)
        );
    }

    protected function assertUrlMatches(string $expected)
    {
        $expected = sprintf('%s://%s%s', getenv('URL_SCHEME'), getenv('URL_HOST'), $expected);
        $actual = static::$client->getRequest()->getUri();

        $this->assertStringMatchesFormat(
            $expected, // @codeCoverageIgnore
            $actual, // @codeCoverageIgnore
            sprintf('Failed asserting that request URI %s matches expected %s', $actual, $expected)
        );
    }

    protected function assertSuccessful()
    {
        $this->assertStatus(200);
    }

    protected function assertStatus(int $expected)
    {
        $actual = static::$client->getResponse()->getStatus();

        $this->assertEquals(
            $expected, // @codeCoverageIgnore
            $actual, // @codeCoverageIgnore
            sprintf('Failed asserting that response status %s matches expected %s', $actual, $expected)
        );
    }

    protected function assertContentTypeJson()
    {
        $this->assertContentType('application/json');
    }

    protected function assertContentType(string $expected)
    {
        $actual = static::$client->getResponse()->getHeader('Content-Type');

        $this->assertEquals(
            $expected, // @codeCoverageIgnore
            $actual, // @codeCoverageIgnore
            sprintf('Failed asserting that response content type %s matches expected %s', $actual, $expected)
        );
    }

    protected function assertLoggedOut()
    {
        $this->assertNull(static::$client->getCurrentUser(), 'Failed asserting that no user is logged in');
    }

    protected function assertLoggedInAs(string $identifier)
    {
        $user = static::$client->getCurrentUser();
        $this->assertInstanceOf(UserInterface::class, $user, 'Failed asserting that user is logged in');
        $this->assertEquals($identifier, $user->getIdentifier(), sprintf(
            'Failed asserting that current user is "%s"',
            $identifier
        ));
    }

    protected function assertLoggedInAsRole(string $role)
    {
        $user = static::$client->getCurrentUser();
        $this->assertInstanceOf(UserInterface::class, $user, 'Failed asserting that user is logged in');
        $this->assertTrue(in_array($role, $user->getRoles()), sprintf(
            'Failed asserting that current user has role "%s"',
            $role
        ));
    }
}
