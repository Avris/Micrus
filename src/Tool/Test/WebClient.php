<?php
namespace Avris\Micrus\Tool\Test;

use Avris\Http\Request\Request;
use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Http\Request\FileBag;
use Avris\Http\Request\RequestInterface;
use Avris\Http\Response\Response;
use Avris\Http\Request\UploadedFile;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Container\ContainerInterface;
use Avris\Micrus\Tool\Security\SecurityManagerInterface;
use Symfony\Component\BrowserKit as Kit;

class WebClient extends Kit\Client
{
    /** @var AbstractApp */
    private $app;

    /** @var RequestInterface */
    private $micrusRequest;

    /** @var Response */
    private $micrusResponse;

    public function __construct(AbstractApp $app)
    {
        $this->app = $app;

        $server = [
            'HTTP_HOST' => getenv('URL_HOST'),
        ];

        if (getenv('URL_SCHEME') === 'https') {
            $server['HTTPS'] = 'on';
        }

        parent::__construct($server);
    }

    /**
     * @param Kit\Request $request
     * @return Kit\Response
     */
    protected function doRequest($request)
    {
        $this->micrusRequest = $this->buildMicrusRequest($request);
        $this->micrusResponse = $this->app->handle($this->micrusRequest);

        return $this->buildKitResponse($this->micrusResponse);
    }

    private function buildMicrusRequest(Kit\Request $request): RequestInterface
    {
        $components = parse_url($request->getUri());

        $server = [
            'REQUEST_METHOD' => $request->getMethod(),
            'REQUEST_SCHEME' => $components['scheme'] ?? 'http',
            'HTTP_HOST' => $components['host'] ?? 'localhost',
            'PATH_INFO' => $components['path'] ?? '/',
            'QUERY_STRING' => $components['query'] ?? '',
        ];

        $server['REQUEST_URI'] = $server['PATH_INFO'] .
            ($server['QUERY_STRING'] ? ('?' . $server['QUERY_STRING']) : '');

        parse_str($server['QUERY_STRING'], $query);

        return Request::fromArrays(
            array_merge($request->getServer(), $server),
            $query, // @codeCoverageIgnore
            $request->getParameters(),
            $request->getCookies(),
            $this->buildFileBag($request->getFiles())
        );
    }

    private function buildFileBag(array $files): FileBag
    {
        $fileBag = new FileBag();

        foreach ($files as $key => $value) {
            if (is_array($value) && array_keys($value) == ['name', 'type', 'tmp_name', 'error', 'size']) {
                if ($value['name']) {
                    $fileBag->set($key, new UploadedFile($value));
                }
            } else {
                $fileBag->set($key, $this->buildFileBag($value));
            }
        }

        return $fileBag;
    }


    private function buildKitResponse(Response $response): Kit\Response
    {
        $headers = array_map('strval', $response->getHeaders()->all());

        if ($response->getCookies()) {
            $cookies = [];
            foreach ($response->getCookies()->all() as $name => $cookie) {
                $cookies[] = new Kit\Cookie(
                    $name, // @codeCoverageIgnore
                    $cookie->getValue(),
                    $cookie->getExpire(),
                    $cookie->getPath(),
                    $cookie->getDomain(),
                    $cookie->isSecure(),
                    $cookie->isHttpOnly()
                );
            }
            $headers['Set-Cookie'] = $cookies;
        }

        return new Kit\Response(
            $response->getContent(),
            $response->getStatus(),
            $headers
        );
    }

    public function getContainer(): ?ContainerInterface
    {
        return $this->app->getContainer();
    }

    public function getMicrusRequest(): RequestInterface
    {
        return $this->micrusRequest;
    }

    public function getMicrusResponse(): Response
    {
        return $this->micrusResponse;
    }

    public function getCurrentUser(): ?UserInterface
    {
        return $this->getContainer() ? $this->getContainer()->get(SecurityManagerInterface::class)->getUser() : null;
    }
}
