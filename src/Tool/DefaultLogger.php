<?php
namespace Avris\Micrus\Tool;

use Psr\Log\AbstractLogger;

class DefaultLogger extends AbstractLogger
{
    /**
     * @codeCoverageIgnore
     */
    public function __construct()
    {
        if (!defined('STDERR')) {
            define('STDERR', fopen('php://stderr', 'w'));
        }
    }

    public function log($level, $message, array $context = [])
    {
        fwrite(STDERR, sprintf('[%s] app.%s: %s %s', date('Y-m-d H:i:s'), $level, $message, @json_encode($context)));
    }
}
