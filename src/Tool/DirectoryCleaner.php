<?php
namespace Avris\Micrus\Tool;

class DirectoryCleaner
{
    public function removeDir(string $dir): bool
    {
        if (!is_dir($dir)) {
            return true;
        }

        return $this->emptyDir($dir) && rmdir($dir);
    }

    public function emptyDir(string $dir)
    {
        if (!is_dir($dir)) {
            return true;
        }

        $it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($it, \RecursiveIteratorIterator::CHILD_FIRST);

        $result = true;
        /** @var \SplFileInfo $file */
        foreach ($files as $file) {
            if ($file->isLink()) {
                @rmdir($file->getPathname());  // Windows
                @unlink($file->getPathname()); // Linux
            } elseif ($file->isDir()) {
                $result = rmdir($file->getPathname()) && $result;
            } else {
                $result = unlink($file->getPathname()) && $result;
            }
        }

        return $result;
    }
}
