<?php
namespace Avris\Micrus\Tool\Config;

interface ParametersProvider
{
    public function getParametersDefaults(): array;
}
