<?php
namespace Avris\Micrus\Tool\Config;

use Avris\Bag\Bag;
use Avris\Bag\BagHelper;
use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Exception\ConfigException;

class ArrayHelper
{
    public function merge($arr1, $arr2)
    {
        if (!is_array($arr1) || !is_array($arr2)) {
            return $arr2;
        }

        $delete = BagHelper::toArray($arr2['_delete'] ?? []);
        unset($arr2['_delete']);
        foreach ($delete as $deleteKey) {
            unset($arr1[$deleteKey]);
        }

        foreach ($arr2 as $key2 => $value2) {
            $arr1[$key2] = $this->merge(
                $arr1[$key2] ?? null,
                $value2
            );
        }

        return $arr1;
    }

    public function extras(array $data, callable $import = null): array
    {
        $result = [];

        $defaults = [];

        foreach ($data as $key => $values) {
            if (!is_null($import) && $key === '_import') {
                foreach (BagHelper::toArray($values) as $filename) {
                    $result = $this->merge($result, $import($filename));
                }
                continue;
            }

            if ($key === '_defaults') {
                $defaults = BagHelper::toArray($values);
                continue;
            }

            $result[$key] = is_array($values)
                ? $this->extras($this->merge($defaults, $values), $import)
                : $values;
        }

        return $result;
    }
}
