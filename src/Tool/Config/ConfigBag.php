<?php
namespace Avris\Micrus\Tool\Config;

use Avris\Container\Parameters\ParameterProvider;
use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Micrus\Exception\NotFoundException;

class ConfigBag implements ParameterProvider
{
    /** @var string */
    private $env;

    /** @var bool */
    private $debug;

    /** @var string */
    private $projectDir;

    /** @var string[] */
    private $dirs;

    public function __construct(string $env, bool $debug, string $projectDir, array $dirs)
    {
        $this->env = $env;
        $this->debug = $debug;
        $this->projectDir = $projectDir;
        $this->dirs = $dirs;
    }

    public function getEnv(): string
    {
        return $this->env;
    }

    public function isDebug(): bool
    {
        return $this->debug;
    }

    public function getDir(string $dirName = null): string
    {
        if ($dirName === null || $dirName === AbstractApp::PROJECT_DIR) {
            return $this->projectDir;
        }

        if (!isset($this->dirs[$dirName])) {
            throw new NotFoundException(sprintf('Directory "%s" is not defined', $dirName));
        }

        return $this->projectDir . '/' . $this->dirs[$dirName];
    }

    public function replaceParameters(string $content, $context = null): string
    {
        return preg_replace_callback('#%([A-Z_]+)(?:\(([^%]+)\))?%#', function ($matches) use ($context) {
            return $this->getParameter($matches[1], ['module' => $context, 'param' => $matches[2] ?? null]);
        }, $content);
    }
    
    public function getParameter(string $name, $context = null)
    {
        switch ($name) {
            case 'MODULE_DIR':
                return $context['module']->getDir();
            case 'MODULE_NAME':
                return $context['module']->getName();
            case 'APP_ENV':
                return $this->env;
            case 'APP_DEBUG':
                return $this->debug;
            case 'DATE':
                return date($context['param']);
            default:
                if ($this->dirDefined($name)) {
                    return $this->getDir($name);
                }

                $env = getenv($name);
                if ($env === false) {
                    throw new NotFoundException(sprintf('Undefined environmental variable %s', $name));
                }

                return $env;
        }
    }

    private function dirDefined(string $dirName)
    {
        return $dirName === AbstractApp::PROJECT_DIR || isset($this->dirs[$dirName]);
    }
}
