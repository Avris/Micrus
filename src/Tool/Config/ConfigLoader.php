<?php
namespace Avris\Micrus\Tool\Config;

use Avris\Bag\Bag;
use Avris\Bag\BagHelper;
use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Exception\ConfigException;

class ConfigLoader
{
    /** @var ConfigBag */
    private $configBag;

    /** @var ArrayHelper */
    private $helper;

    /** @var array */
    private $config = [];

    public function __construct(ConfigBag $configBag)
    {
        $this->configBag = $configBag;
        $this->helper = new ArrayHelper();
    }

    public function load(ModuleInterface $module, string $directory)
    {
        foreach ($this->loadDirectory($module, $directory) as $section => $config) {
            $this->config[$section] = $this->helper->merge($this->config[$section] ?? [], $config);
        }
    }

    protected function loadDirectory(ModuleInterface $module, string $directory): iterable
    {
        $importCallback = function (string $filename) use ($module, $directory): array {
            return $this->loadFile(
                $module, // @codeCoverageIgnore
                sprintf('%s/%s/%s.yml', $module->getDir(), $directory, $filename)
            );
        };

        foreach (glob(sprintf('%s/%s/*.yml', $module->getDir(), $directory)) as $filename) {
            preg_match('#/([^/\\\\]+)\.yml$#', $filename, $matches);

            if (substr($matches[1], 0, 1) === '_') {
                continue;
            }

            yield $matches[1] => $this->helper->extras($this->loadFile($module, $filename), $importCallback);
        }
    }

    protected function loadFile(ModuleInterface $module, string $filename): array
    {
        return BagHelper::toArray(
            \Spyc::YAMLLoadString(
                $this->configBag->replaceParameters(file_get_contents($filename), $module)
            )
        );
    }

    public function get(): Bag
    {
        return new Bag($this->config);
    }
}
