<?php
namespace Avris\Micrus\Tool;

use Avris\Http\Response\Response;
use Avris\Micrus\Exception\Http\HttpException;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\AbstractDumper;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;

final class Dumper implements DumperInterface
{
    /** @var bool */
    private $debug;

    /** @var VarCloner */
    private $cloner;

    /** @var AbstractDumper */
    private $dumper;

    public function __construct(bool $envAppDebug)
    {
        $this->debug = $envAppDebug;

        if ($this->debug) {
            $this->cloner = new VarCloner();
            $this->dumper = PHP_SAPI === 'cli' ? new CliDumper() : new HtmlDumper();
        }
    }

    public function dump($variable): string
    {
        if ($this->debug) {
            return $this->dumper->dump($this->cloner->cloneVar($variable), true);
        }

        return '';
    }

    public function dumpResponse($variable, int $status = 200): Response
    {
        return new Response($this->dump($variable), $status);
    }

    public function dumpExceptionResponse(\Throwable $e): Response
    {
        return new Response($this->dump($e), $e instanceof HttpException ? $e->getCode() : 500);
    }
}
