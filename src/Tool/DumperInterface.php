<?php

namespace Avris\Micrus\Tool;

use Avris\Http\Response\Response;

interface DumperInterface
{
    public function dump($variable): string;

    public function dumpResponse($variable, int $status = 200): Response;

    public function dumpExceptionResponse(\Throwable $e): Response;
}
