<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Micrus\Model\User\UserInterface;

interface RoleCheckerInterface
{
    /**
     * @return string[][]
     */
    public function getRoles();

    /**
     * @param string[]| string $requiredRoles
     * @param UserInterface|null $user
     * @return bool true if user has any of $requiredRoles
     */
    public function isUserGranted($requiredRoles, UserInterface $user = null);
}
