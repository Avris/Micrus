<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Micrus\Exception\ConfigException;

class Crypt implements CryptInterface
{
    const SALT_LENGTH = 16;

    /** @var string */
    private $secret;

    /** @var string */
    private $method;

    public function __construct(string $envAppSecret, string $method = 'AES-128-CTR')
    {
        $this->secret = $envAppSecret;

        if (!in_array($method, openssl_get_cipher_methods())) {
            throw new ConfigException(sprintf('Unrecognised encryption method: %s', $method));
        }

        $this->method = $method;
    }

    public function encrypt(string $data): string
    {
        $this->assertSecretValid();

        $iv = openssl_random_pseudo_bytes($this->ivLength());

        return bin2hex($iv) . openssl_encrypt($data, $this->method, $this->secret, 0, $iv);
    }

    public function decrypt(string $data): ?string
    {
        $this->assertSecretValid();

        $ivStrlen = 2  * $this->ivLength();
        if (!preg_match("/^(.{" . $ivStrlen . "})(.+)$/", $data, $regs)) {
            return null;
        }

        list(, $iv, $cryptedString) = $regs;
        $binaryIv = @hex2bin($iv);
        if ($binaryIv === false) {
            return null;
        }

        return openssl_decrypt($cryptedString, $this->method, $this->secret, 0, $binaryIv);
    }

    private function ivLength()
    {
        return openssl_cipher_iv_length($this->method);
    }

    private function assertSecretValid()
    {
        if (empty($this->secret)) {
            throw new ConfigException(
                'To encrypt securely, you must generate a secret (Run "bin/micrus security:secret")'
            );
        }
    }

    public function passwordHash(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function passwordNeedsRehash(string $hash): bool
    {
        return password_needs_rehash($hash, PASSWORD_DEFAULT);
    }

    public function passwordVerify(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }

    public function generateSymmetricKey(): string
    {
        return hash('sha256', mt_rand());
    }

    public function generateAsymmetricKeys(): array
    {
        $keyResource = @openssl_pkey_new(['private_key_bits' => 2048]);
        $details = openssl_pkey_get_details($keyResource);
        $publicKey = $details['key'];
        openssl_pkey_export($keyResource, $privateKey);

        return [$privateKey, $publicKey];
    }
}
