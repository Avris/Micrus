<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Http\Cookie\CookieBag;
use Avris\Http\Header\HeaderBag;
use Avris\Micrus\Controller\RequestEvent;
use Avris\Micrus\Controller\ResponseEvent;
use Avris\Bag\Bag;

final class SecurityEnhancer implements EventSubscriberInterface
{
    /** @var CryptInterface */
    private $crypt;

    /** @var Bag */
    private $config;
    
    private static $headers = [
        'Strict-Transport-Security' => 'max-age=31536000; includeSubDomains',
        'X-Frame-Options' => 'SAMEORIGIN',
        'Server' => '',
        'X-Powered-By' => '',
        'X-XSS-Protection' => '1; mode=block',
        'X-Content-Type-Options' => 'nosniff',
        'Content-Security-Policy' => null,
    ];

    public function __construct(CryptInterface $crypt, Bag $configSecurity)
    {
        $this->crypt = $crypt;
        $this->config = $configSecurity;
    }

    public function onWarmup()
    {
        if (session_id()) {
            return;
        }

        if ($this->config->get('preventSessionFixation')) {
            ini_set('session.use_cookies', 1);
            ini_set('session.use_only_cookies', 1);
            ini_set('session.use_trans_sid', 1);
        }
        if ($this->config->get('cookiesOnlyHttp')) {
            ini_set('session.cookie_httponly', 1);
        }
        if ($this->config->get('ssl')) {
            ini_set('session.cookie_secure', 1);
        }
    }

    public function onRequest(RequestEvent $event)
    {
        if ($this->config->get('encryptCookies')) {
            $this->decryptCookies($event->getRequest()->getCookies());
        }
    }

    public function onResponse(ResponseEvent $event)
    {
        if ($cookieBag = $event->getResponse()->getCookies()) {
            $this->secureCookies(
                $cookieBag, // @codeCoverageIgnore
                $this->config->get('encryptCookies'),
                $this->config->get('ssl'),
                $this->config->get('cookiesOnlyHttp')
            );
        }

        $this->secureHeaders(
            $event->getResponse()->getHeaders(),
            new Bag($this->config->get('secureHeaders'))
        );
    }

    private function decryptCookies(Bag $cookies)
    {
        foreach ($cookies as $name => $value) {
            if ($name === session_name()) {
                continue;
            }

            $cookies->delete($name);
            $decryptedValue = $this->crypt->decrypt($value);

            if ($decryptedValue !== false) {
                $cookies->set($name, $decryptedValue);
            }
        }
    }

    private function secureCookies(CookieBag $cookies, $encrypt, $enforceSecure, $enforceHttpOnly)
    {
        foreach ($cookies->all() as $name => $cookie) {
            $cookies->set(
                $cookie->getName(),
                $encrypt ? $this->crypt->encrypt($cookie->getValue()) : $cookie->getValue(),
                $cookie->getExpire(),
                $cookie->getPath(),
                $cookie->getDomain(),
                $enforceSecure ? true : $cookie->isSecure(),
                $enforceHttpOnly ? true : $cookie->isHttpOnly()
            );
        }
    }

    private function secureHeaders(HeaderBag $headers, Bag $config)
    {
        if (!$config->get('enabled')) {
            return;
        }

        foreach (static::$headers as $key => $defaultValue) {
            $value = $config->has($key) ? $config->get($key) : $defaultValue;
            if ($value !== null) {
                $headers->set($key, $value);
            }
        }
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscribedEvents(): iterable
    {
        yield 'warmup' => [$this, 'onWarmup'];
        yield 'request:999' => [$this, 'onRequest'];
        yield 'response:-999' => [$this, 'onResponse'];
    }
}
