<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Bag\BagHelper;
use Avris\Micrus\Bootstrap\ConfigExtension;
use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Micrus\Controller\ControllerEvent;
use Avris\Micrus\Controller\ControllerInterface;
use Avris\Http\Cookie\CookieBag;
use Avris\Http\Response\RedirectResponse;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\ResponseEvent;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Exception\Http\ForbiddenHttpException;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Model\User\UserProviderInterface;
use Avris\Bag\Bag;
use Avris\Container\ContainerInterface;

class LogoutController implements ConfigExtension, ControllerInterface
{
    public function logoutAction(RouterInterface $router, SecurityManagerInterface $securityManager): RedirectResponse
    {
        $responseCookies = new CookieBag();
        $route = $securityManager->logout($responseCookies);

        return new RedirectResponse(
            $router->generate($route),
            302,
            $responseCookies
        );
    }

    /**
     * @codeCoverageIgnore
     */
    public function extendConfig(): array
    {
        return [
            'routing' => [
                'logout' => '/logout -> ' . __CLASS__ . '/logout',
            ],
        ];
    }
}
