<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Micrus\Model\User\UserInterface;

class Guardian implements GuardianInterface
{
    /** @var GuardInterface[] */
    private $guards;

    public function __construct(array $guards)
    {
        $this->guards = $guards;
    }

    public function check(UserInterface $user, string $check, $object = null): bool
    {
        foreach ($this->guards as $guard) {
            if ($guard->supports($check, $object) && !$guard->check($user, $check, $object)) {
                return false;
            }
        }

        return true;
    }
}
