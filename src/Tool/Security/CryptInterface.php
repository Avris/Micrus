<?php
namespace Avris\Micrus\Tool\Security;

interface CryptInterface
{
    public function encrypt(string $data): string;

    public function decrypt(string $data): ?string;

    public function passwordHash(string $password): string;

    public function passwordNeedsRehash(string $hash): bool;

    public function passwordVerify(string $password, string $hash): bool;

    public function generateSymmetricKey(): string;

    /**
     * @return string[] [private, public]
     */
    public function generateAsymmetricKeys(): array;
}
