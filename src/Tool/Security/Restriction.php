<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Bag\BagHelper;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Exception\Http\ForbiddenHttpException;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Bag\Bag;

final class Restriction
{
    /** @var string|null */
    private $pattern;

    /** @var string|null */
    private $target;

    /** @var string[] */
    private $roles;

    /** @var string|null */
    private $check;

    /** @var mixed|null */
    private $object;

    public function __construct(
        ?string $pattern = null,
        ?string $target = null,
        $roles = [],
        ?string $check = null,
        $object = null
    ) {
        $this->pattern = $pattern;
        $this->target = $target;
        $this->roles = BagHelper::toArray($roles);
        $this->check = $check;
        $this->object = $object;
    }

    /**
     * @param array|Bag $config
     * @return self[]
     */
    public static function fromConfig($config): array
    {
        $restrictions = [];

        foreach (BagHelper::toArray($config) as $key => $value) {
            $restrictions[$key] = new self(
                $value['pattern'] ?? null,
                $value['target'] ?? null,
                $value['roles'] ?? [],
                $value['check'] ?? null,
                $value['object'] ?? null
            );
        }

        return $restrictions;
    }

    public function matches(RequestInterface $request, string $target): bool
    {
        if ($this->pattern && preg_match('#' . $this->pattern. '#', $request->getCleanUrl())) {
            return true;
        }

        if ($this->target && $this->matchesRouteTarget($target)) {
            return true;
        }

        return false;
    }

    private function matchesRouteTarget($target)
    {
        list($targetController, $targetAction) = explode('/', $target . '/');
        list($restrictionController, $restrictionAction) = explode('/', $this->target . '/');

        return $restrictionController === $targetController
            && (!$restrictionAction || $restrictionAction === $targetAction);
    }

    public function validateRoles(RoleCheckerInterface $roleChecker, UserInterface $user)
    {
        if (empty($this->roles)) {
            return;
        }

        if (!$roleChecker->isUserGranted($this->roles, $user)) {
            throw new ForbiddenHttpException(sprintf('Required role: %s', implode(', ', $this->roles)));
        }
    }

    public function validateCheck(GuardianInterface $guardian, UserInterface $user, array $context)
    {
        if (empty($this->check)) {
            return;
        }

        $object = $context[$this->object] ?? null;
        if (!$guardian->check($user, $this->check, $object)) {
            throw new ForbiddenHttpException(sprintf('Unmet condition "%s"', $this->check));
        }
    }
}
