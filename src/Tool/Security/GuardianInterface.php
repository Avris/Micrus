<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Micrus\Model\User\UserInterface;

interface GuardianInterface
{
    public function check(UserInterface $user, string $check, $object = null): bool;
}
