<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Bag\Bag;
use Avris\Micrus\Model\User\UserInterface;

class RoleChecker implements RoleCheckerInterface
{
    /** @var string[][] */
    private $hierarchy;

    /** @var string[][] */
    private $roles;

    public function __construct(Bag $configSecurity_roles)
    {
        $this->hierarchy = $configSecurity_roles->all();

        foreach (array_keys($this->hierarchy) as $role) {
            $this->roles[$role] = $this->buildInheritedRoles($role);
        }
    }

    private function buildInheritedRoles(string $parent, array $roles = []): array
    {
        $roles[] = $parent;

        if (!isset($this->hierarchy[$parent])) {
            return $roles;
        }

        foreach ((array) $this->hierarchy[$parent] as $child) {
            if (!in_array($child, $roles)) {
                $roles = $this->buildInheritedRoles($child, $roles);
            }
        }

        return $roles;
    }

    /**
     * @return string[][]
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param string[]| string $requiredRoles
     * @param UserInterface|null $user
     * @return bool true if user has any of $requiredRoles
     */
    public function isUserGranted($requiredRoles, UserInterface $user = null)
    {
        if (empty($requiredRoles)) {
            return true;
        }

        if (!$user) {
            return false;
        }

        $requiredRoles = (array) $requiredRoles;

        foreach ($user->getRoles() as $role) {
            $actualRoles = isset($this->roles[$role])
                ? $this->roles[$role]
                : [$role];

            if (count(array_intersect($requiredRoles, $actualRoles)) > 0) {
                return true;
            }
        }

        return false;
    }
}
