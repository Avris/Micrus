<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Micrus\Model\User\UserInterface;

interface GuardInterface
{
    public function supports(string $check, $object = null): bool;

    public function check(UserInterface $user, string $check, $object = null): bool;
}
