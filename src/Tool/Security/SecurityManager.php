<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Micrus\Controller\ControllerEvent;
use Avris\Http\Cookie\CookieBag;
use Avris\Http\Response\RedirectResponse;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Model\User\UserProviderInterface;
use Avris\Bag\Bag;

final class SecurityManager implements EventSubscriberInterface, SecurityManagerInterface
{
    const AUTHENTICATOR_PASSWORD = 'password';
    const AUTHENTICATOR_COOKIE = 'cookie';

    const COOKIE_NAME = 'rememberme';

    const USER_SESSION_KEY = '_user';
    const CSRF_SESSION_KEY = '_csrf';

    /** @var Bag */
    private $config;

    /** @var RequestProviderInterface */
    private $requestProvider;

    /** @var SessionInterface */
    private $session;

    /** @var RouterInterface */
    private $router;

    /** @var UserProviderInterface  */
    private $userProvider;

    /** @var RoleChecker */
    private $roleChecker;

    /** @var Restriction[] */
    private $publicPaths;

    /** @var Restriction[] */
    private $restrictions;

    /** @var UserInterface */
    private $user;

    /** @var GuardianInterface */
    private $guardian;

    public function __construct(
        Bag $configSecurity,
        RequestProviderInterface $requestProvider,
        SessionInterface $session,
        RouterInterface $router,
        UserProviderInterface $userProvider,
        RoleCheckerInterface $roleChecker,
        GuardianInterface $guardian
    ) {
        $this->config = $configSecurity;
        $this->requestProvider = $requestProvider;
        $this->session = $session;
        $this->router = $router;
        $this->userProvider = $userProvider;
        $this->roleChecker = $roleChecker;

        $this->restrictions = Restriction::fromConfig($this->config->get('restrictions'));
        $this->publicPaths = Restriction::fromConfig($this->config->get('public'));

        $this->guardian = $guardian;
    }

    public function onController(ControllerEvent $event)
    {
        if (!$this->checkAccess($this->getUser(), $event->getRequest(), $event->getTarget(), $event->getArguments())) {
            $response = new RedirectResponse($this->router->generate(
                $this->config->get('loginPath', 'login'),
                ['url' => $event->getRequest()->getCleanUrl()]
            ));

            $event->stopPropagation();
            $event->setResponse($response);
        }
    }

    public function checkAccess(
        ?UserInterface $user,
        RequestInterface $request,
        string $target,
        array $context
    ): bool {
        foreach ($this->publicPaths as $publicPath) {
            if ($publicPath->matches($request, $target)) {
                return true;
            }
        }

        foreach ($this->restrictions as $restriction) {
            if (!$restriction->matches($request, $target)) {
                continue;
            }

            if (!$user) {
                return false;
            }

            $restriction->validateRoles($this->roleChecker, $user);
            $restriction->validateCheck($this->guardian, $user, $context);
        }

        return true;
    }

    public function login(UserInterface $user, ?CookieBag $cookiesIfRemember = null): UserInterface
    {
        $this->session->set(self::USER_SESSION_KEY, $user->getIdentifier());

        if ($cookiesIfRemember) {
            $this->generateRememberMeCookie($user, $cookiesIfRemember);
        }

        return $this->user = $user;
    }

    private function generateRememberMeCookie(UserInterface $user, CookieBag $responseCookies)
    {
        $token = hash('sha256', uniqid(mt_rand(), true));
        $validUntil = new \DateTime('+' . $this->config->get('rememberMeForDays', 30) . ' days');

        $auth = $user->createAuthenticator(
            SecurityManager::AUTHENTICATOR_COOKIE,
            $token, // @codeCoverageIgnore
            $validUntil
        );

        $responseCookies->set(
            static::COOKIE_NAME,
            $user->getIdentifier() . '|' . $token,
            $validUntil, // @codeCoverageIgnore
            $this->requestProvider->getRequest()->getBase(),
            $this->requestProvider->getRequest()->getHost()
        );

        return $auth;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user ?: $this->getUserFromSession() ?: $this->loginFromCookie();
    }

    private function getUserFromSession(): ?UserInterface
    {
        $identifier = $this->session->get(self::USER_SESSION_KEY);
        if (!$identifier) {
            return null;
        }

        if ($user = $this->userProvider->getUser($identifier)) {
            return $this->user = $user;
        }

        $this->session->set(self::USER_SESSION_KEY, null);

        return null;
    }

    private function loginFromCookie(): ?UserInterface
    {
        $cookie = $this->requestProvider->getRequest()->getCookies()->get(static::COOKIE_NAME);

        if (!$cookie) {
            return null;
        }

        $data = explode('|', $cookie);

        if (count($data) !== 2) {
            return null;
        }

        $user = $this->userProvider->getUser($data[0]);

        if (!$user) {
            return null;
        }

        foreach ($user->getAuthenticators(static::AUTHENTICATOR_COOKIE) as $auth) {
            if ($auth->getPayload() === $data[1]) {
                return $this->login($user);
            }
        }

        return null;
    }

    public function logout(CookieBag $responseCookies = null): string
    {
        if ($responseCookies) {
            $responseCookies->delete(
                static::COOKIE_NAME,
                $this->requestProvider->getRequest()->getBase(),
                $this->requestProvider->getRequest()->getHost()
            );
        }

        $this->session->destroy();

        $this->user = null;

        return $this->config->get('afterLogoutRoute') ?: RouterInterface::DEFAULT_ROUTE;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscribedEvents(): iterable
    {
        yield 'controller' => [$this, 'onController'];
    }
}
