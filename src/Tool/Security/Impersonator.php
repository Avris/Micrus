<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Http\Response\RedirectResponse;
use Avris\Micrus\Controller\RequestEvent;
use Avris\Http\Request\RequestInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Exception\Http\ForbiddenHttpException;
use Avris\Micrus\Exception\Http\NotFoundHttpException;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Model\User\UserProviderInterface;

final class Impersonator implements EventSubscriberInterface
{
    const QUERY_PARAMETER = '_impersonate';
    const EXIT_VALUE = '_exit';
    const SESSION_IMPERSONATOR = '_impersonator';
    const SESSION_IMPERSONATOR_DATA = '_impersonator_data';
    const ROLE = 'ROLE_IMPERSONATOR';

    /** @var SecurityManagerInterface */
    private $sm;

    /** @var UserProviderInterface */
    private $userProvider;

    /** @var RoleChecker */
    private $roleChecker;

    /** @var SessionInterface */
    private $session;

    public function __construct(
        SecurityManagerInterface $sm,
        UserProviderInterface $userProvider,
        RoleCheckerInterface $roleChecker,
        SessionInterface $session
    ) {
        $this->sm = $sm;
        $this->userProvider = $userProvider;
        $this->roleChecker = $roleChecker;
        $this->session = $session;
    }

    public function onRequest(RequestEvent $event): ?RedirectResponse
    {
        $identifier = $event->getRequest()->getQuery()->get(self::QUERY_PARAMETER);

        if (!$identifier) {
            return null;
        }

        if ($identifier === self::EXIT_VALUE) {
            $this->exitImpersonation();

            return $this->redirect($event->getRequest());
        }

        $this->impersonate($this->getUser($identifier));

        return $this->redirect($event->getRequest());
    }

    public function impersonate(UserInterface $user)
    {
        $impersonator = $this->sm->getUser();

        if (!$impersonator || !$this->roleChecker->isUserGranted(self::ROLE, $impersonator)) {
            throw new ForbiddenHttpException(sprintf('Role %s is required to impersonate', self::ROLE));
        }

        $oldData = $this->session->all();
        foreach ($oldData as $key => $value) {
            $this->session->delete($key);
        }
        $this->session->set(self::SESSION_IMPERSONATOR, $impersonator->getIdentifier());
        $this->session->set(self::SESSION_IMPERSONATOR_DATA, $oldData);

        $this->sm->login($user);
    }

    public function exitImpersonation()
    {
        $identifier = $this->session->get(self::SESSION_IMPERSONATOR);

        if (!$identifier) {
            return;
        }

        $oldData = $this->session->get(self::SESSION_IMPERSONATOR_DATA, []);
        foreach ($this->session->all() as $key => $value) {
            $this->session->delete($key);
        }
        foreach ($oldData as $key => $value) {
            $this->session->set($key, $value);
        }

        $this->sm->login($this->getUser($identifier));
    }

    private function getUser(string $identifier): UserInterface
    {
        $user = $this->userProvider->getUser($identifier);

        if (!$user) {
            throw new NotFoundHttpException(sprintf('User "%s" does not exist', $identifier));
        }

        return $user;
    }

    private function redirect(RequestInterface $request): RedirectResponse
    {
        $toBeRemoved = http_build_query([self::QUERY_PARAMETER => $request->getQuery()->get(self::QUERY_PARAMETER)]);

        $newUrl = strtr($request->getUrl(), [
            '?' . $toBeRemoved . '&' => '?',
            '?' . $toBeRemoved => '',
            '&' . $toBeRemoved => '',
        ]);

        return new RedirectResponse($newUrl);
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscribedEvents(): iterable
    {
        yield 'request' => [$this, 'onRequest'];
    }
}
