<?php

namespace Avris\Micrus\Tool\Security;

use Avris\Http\Cookie\CookieBag;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Model\User\UserInterface;

interface SecurityManagerInterface
{
    public function login(UserInterface $user, ?CookieBag $cookiesIfRemember = null): UserInterface;

    public function getUser(): ?UserInterface;

    public function checkAccess(?UserInterface $user, RequestInterface $request, string $target, array $context): bool;

    public function logout(CookieBag $responseCookies = null): string;
}
