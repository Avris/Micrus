<?php
namespace Avris\Micrus\Tool\Cache;

use Avris\Dispatcher\Event;

final class CacheWarmupEvent extends Event
{
    /** @var bool */
    private $optional;

    public function __construct(bool $optional)
    {
        $this->optional = $optional;
    }

    public function getName(): string
    {
        return 'cacheWarmup';
    }

    public function includeOptional(): bool
    {
        return $this->optional;
    }

    public function setValue($value): Event
    {
        return $this;
    }

    public function getValue()
    {
        return null;
    }
}
