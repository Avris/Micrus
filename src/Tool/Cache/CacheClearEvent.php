<?php
namespace Avris\Micrus\Tool\Cache;

use Avris\Dispatcher\Event;

final class CacheClearEvent extends Event
{
    public function getName(): string
    {
        return 'cacheClear';
    }

    public function setValue($value): Event
    {
        return $this;
    }

    public function getValue()
    {
        return null;
    }
}
