<?php
namespace Avris\Micrus\Tool\Cache;

use Psr\Cache\CacheItemPoolInterface;

interface CacherInterface
{
    public function cache($key, callable $generator);

    public function getPool(): CacheItemPoolInterface;
}
