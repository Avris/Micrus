<?php
namespace Avris\Micrus\Tool\Cache;

use Psr\Cache\CacheItemInterface;

final class CacheItem implements CacheItemInterface
{
    const DEFAULT_TTL = 30 * 24 * 60 * 60;

    /** @var string */
    protected $key;

    /** @var bool */
    protected $hit;

    /** @var mixed */
    protected $value;

    /** @var \DateTime */
    protected $expiresAt;

    public function __construct(string $key, bool $hit, $value = null, int $expiresAt = null)
    {
        $this->key = $key;
        $this->hit = $hit;
        $this->value = $value;
        if ($expiresAt) {
            $this->expiresAt = (new \DateTime())->setTimestamp($expiresAt);
        }
    }

    public function getKey()
    {
        return $this->key;
    }

    public function get()
    {
        return $this->isHit() ? $this->value : null;
    }

    public function isHit()
    {
        return $this->hit && ($this->expiresAt === null || $this->expiresAt > new \DateTime);
    }

    public function set($value)
    {
        $this->value = $value;
        $this->hit = true;

        return $this;
    }

    public function expiresAt($expiration)
    {
        if ($expiration === null) {
            return $this->expiresAfter(self::DEFAULT_TTL);
        }

        $this->expiresAt = $expiration;

        return $this;
    }

    public function expiresAfter($time)
    {
        if ($time === null) {
            return $this->expiresAfter(self::DEFAULT_TTL);
        }

        $this->expiresAt = new \DateTime();

        if (!$time instanceof \DateInterval) {
            $time = new \DateInterval('PT'.((int) $time).'S');
        }

        $this->expiresAt->add($time);

        return $this;
    }

    public function getExpiresAt(): ?int
    {
        return $this->expiresAt ? (int) $this->expiresAt->format('U') : null;
    }
}
