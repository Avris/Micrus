<?php
namespace Avris\Micrus\Tool\Cache;

use Psr\Cache\CacheItemPoolInterface;

class Cacher implements CacherInterface
{
    /** @var CacheItemPoolInterface */
    private $pool;

    /** @var bool */
    private $debug;

    public function __construct(
        CacheItemPoolInterface $pool,
        bool $envAppDebug
    ) {
        $this->pool = $pool;
        $this->debug = $envAppDebug;
    }

    public function cache($key, callable $generator)
    {
        if ($this->debug) {
            return $generator();
        }

        $item = $this->pool->getItem($this->normaliseKey($key));
        if (!$item->isHit()) {
            $item->set($generator());
            $item->expiresAfter(null);
            $this->pool->save($item);
        }

        return $item->get();
    }

    private function normaliseKey(string $key): string
    {
        return str_replace(['{', '}', '(', ')', '/', '\\', '@', ':'], '_', $key);
    }

    public function getPool(): CacheItemPoolInterface
    {
        return $this->pool;
    }
}
