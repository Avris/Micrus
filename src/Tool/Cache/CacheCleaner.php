<?php
namespace Avris\Micrus\Tool\Cache;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Micrus\Tool\DirectoryCleaner;
use Psr\Cache\CacheItemPoolInterface;

final class CacheCleaner implements EventSubscriberInterface
{
    /** @var CacheItemPoolInterface */
    private $pool;

    /** @var DirectoryCleaner */
    private $directoryCleaner;

    /** @var string */
    private $cacheDir;

    public function __construct(
        CacheItemPoolInterface $pool,
        DirectoryCleaner $directoryCleaner,
        string $envCacheDir
    ) {
        $this->directoryCleaner = $directoryCleaner;
        $this->cacheDir = $envCacheDir;
        $this->pool = $pool;
    }

    public function clearCache()
    {
        $this->pool->clear();
        $this->directoryCleaner->removeDir($this->cacheDir);
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscribedEvents(): iterable
    {
        yield 'cacheClear:999' => [$this, 'clearCache'];
    }
}
