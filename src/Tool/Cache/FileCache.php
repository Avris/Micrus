<?php
namespace Avris\Micrus\Tool\Cache;

use Avris\Micrus\Exception\ConfigException;
use Avris\Micrus\Tool\DirectoryCleaner;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;

class FileCache implements CacheItemPoolInterface
{
    /** @var string */
    private $dir;

    /** @var DirectoryCleaner */
    private $directoryCleaner;

    /** @var CacheItemInterface[] */
    private $deferred = [];

    public function __construct(string $dir, DirectoryCleaner $directoryCleaner)
    {
        if (!is_dir($dir)) {
            if (!@mkdir($dir, 0777, true)) {
                throw new ConfigException('Cannot write cache in ' . $dir);
            }
        }

        $this->dir = realpath($dir);
        $this->directoryCleaner = $directoryCleaner;
    }

    public function getItem($key)
    {
        if (!$this->hasItem($key)) {
            return new CacheItem($key, false);
        }

        $data = @unserialize(file_get_contents($this->buildPath($key)));
        return $data && is_array($data) && count($data) == 2
            ? new CacheItem($key, true, $data[0], $data[1])
            : new CacheItem($key, false);
    }

    public function getItems(array $keys = [])
    {
        $items = [];

        foreach ($keys as $key) {
            $items[$key] = $this->getItem($key);
        }

        return $items;
    }

    public function hasItem($key)
    {
        return file_exists($this->buildPath($key));
    }

    public function clear()
    {
        return $this->directoryCleaner->removeDir($this->dir);
    }

    public function deleteItem($key)
    {
        return $this->hasItem($key)
            ? unlink($this->buildPath($key))
            : true;
    }

    public function deleteItems(array $keys)
    {
        $result = true;

        foreach ($keys as $key) {
            $result = $result && $this->deleteItem($key);
        }

        return $result;
    }

    public function save(CacheItemInterface $item)
    {
        $path = $this->buildPath($item->getKey());

        $data = serialize([$item->get(), $item->getExpiresAt()]);

        return (bool) file_put_contents($path, $data, LOCK_EX);
    }

    public function saveDeferred(CacheItemInterface $item)
    {
        $this->deferred[$item->getKey()] = $item;

        return true;
    }

    public function commit()
    {
        $result = true;

        foreach ($this->deferred as $item) {
            $result = $result && $this->save($item);
        }

        return $result;
    }

    private function buildPath(string $key): string
    {
        return $this->dir. '/' . $key . '.obj';
    }
}
