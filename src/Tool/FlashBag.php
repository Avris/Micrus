<?php
namespace Avris\Micrus\Tool;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Http\Session\SessionInterface;

class FlashBag implements EventSubscriberInterface
{
    const SESSION_KEY = '_flash_bag';

    const SUCCESS = 'success';
    const WARNING = 'warning';
    const INFO = 'info';
    const DANGER = 'danger';

    /** @var SessionInterface */
    protected $session;

    /** @var array */
    protected $newFlashBag = [];

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /** @return string[][] */
    public function all()
    {
        $messages = $this->session->get(self::SESSION_KEY) ?: [];

        $this->session->set(self::SESSION_KEY, []);

        return $messages;
    }

    /**
     * @param string $type
     * @param string $message
     * @param bool $deferred
     * @return $this
     */
    public function add($type, $message, $deferred = true)
    {
        $flash = ['type' => (string) $type, 'message' => (string) $message];

        if ($deferred) {
            $this->newFlashBag[] = $flash;
        } else {
            $val = $this->session->get(self::SESSION_KEY);
            $val[] = $flash;
            $this->session->set(self::SESSION_KEY, $val);
        }

        return $this;
    }

    public function update()
    {
        $messages = $this->session->get(self::SESSION_KEY) ?: [];

        $this->session->set(self::SESSION_KEY, array_merge($messages, $this->newFlashBag));

        $this->newFlashBag = [];
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscribedEvents(): iterable
    {
        yield 'response' => [$this, 'update'];
    }
}
