<?php
namespace Avris\Micrus\Exception\Http;

class NotFoundHttpException extends HttpException
{
    public function __construct($message = '', \Throwable $previous = null)
    {
        parent::__construct(404, $message, $previous);
    }
}
