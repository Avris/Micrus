<?php
namespace Avris\Micrus\Exception\Http;

use Avris\Micrus\Exception\LogLevelInterface;
use Psr\Log\LogLevel;

class HttpException extends \RuntimeException implements LogLevelInterface
{
    public function __construct($code, $message = '', \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function getLevel(): string
    {
        return $this->code >= 400 && $this->code < 500 ? LogLevel::WARNING : LogLevel::ERROR;
    }
}
