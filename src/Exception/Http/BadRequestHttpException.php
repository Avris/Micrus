<?php
namespace Avris\Micrus\Exception\Http;

class BadRequestHttpException extends HttpException
{
    public function __construct($message = '', \Throwable $previous = null)
    {
        parent::__construct(400, $message, $previous);
    }
}
