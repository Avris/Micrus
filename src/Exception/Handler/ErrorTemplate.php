<?php
namespace Avris\Micrus\Exception\Handler;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Http\Response\Response;
use Avris\Micrus\Exception\Http\HttpException;
use Avris\Micrus\View\TemplaterInterface;

final class ErrorTemplate implements EventSubscriberInterface
{
    /** @var TemplaterInterface */
    private $templater;

    /** @var bool */
    private $debug;

    public function __construct(TemplaterInterface $templater, bool $envAppDebug)
    {
        $this->templater = $templater;
        $this->debug = $envAppDebug;
    }

    public function onError(ErrorEvent $event)
    {
        if ($this->debug) {
            return null;
        }

        $exception = $event->getException();
        $httpCode = $exception instanceof HttpException ? $exception->getCode() : 500;

        return new Response(
            $this->templater->render('Error/error', [
                'errorCode' => $httpCode,
                'exception' => $exception,
            ]),
            $httpCode
        );
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscribedEvents(): iterable
    {
        yield 'error:-100' => [$this, 'onError'];
    }
}
