<?php
namespace Avris\Micrus\Exception\Handler;

use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Http\Response\ResponseInterface;
use Avris\Micrus\Exception\Error as Error;
use Avris\Micrus\Exception\LogLevelInterface;
use Avris\Micrus\Tool\Dumper;
use Avris\Micrus\Tool\DumperInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

final class ErrorHandler
{
    /** @var ErrorHandler */
    private static $instance;

    const SEVERITIES = [
        E_NOTICE => Error\NoticeException::class,
        E_WARNING => Error\WarningException::class,
        E_STRICT => Error\StrictException::class,
        E_PARSE => Error\ParseException::class,
        E_DEPRECATED => Error\DeprecatedException::class,
        E_ERROR => Error\ErrorException::class,
    ];

    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @var LoggerInterface */
    private $logger;

    /** @var DumperInterface */
    private $dumper;

    /** @var bool */
    private $enabled = false;

    /** @var string */
    private $silencedMessage;

    /** @var int */
    private $originalErrorReporting;

    /** @var int */
    private $originalDisplayErrors;

    private function __construct(
        LoggerInterface $logger,
        DumperInterface $dumper,
        ?EventDispatcherInterface $dispatcher = null
    ) {
        $this->logger = $logger;
        $this->dumper = $dumper;
        $this->dispatcher = $dispatcher;

        $this->originalErrorReporting = error_reporting();
        error_reporting(E_ALL);
        $this->originalDisplayErrors = ini_get('display_errors');
        ini_set('display_errors', 0);

        set_error_handler([$this,'handleError'], E_ALL);
        register_shutdown_function([$this,'handleShutdown']);

        $this->enabled = true;
    }

    /**
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    public function __destruct()
    {
        if ($this->enabled) {
            error_reporting($this->originalErrorReporting);
            ini_set('display_errors', $this->originalDisplayErrors);

            restore_error_handler();
            $this->enabled = false;
        }
    }

    public static function register(
        LoggerInterface $logger,
        DumperInterface $dumper,
        ?EventDispatcherInterface $dispatcher = null
    ): self {
        return static::$instance ?: static::$instance = new ErrorHandler($logger, $dumper, $dispatcher);
    }

    public static function unregister()
    {
        if (static::$instance) {
            static::$instance->__destruct();
            static::$instance = null;
        }
    }

    public function handleException(\Throwable $e): ResponseInterface
    {
        try {
            $this->log($e);

            if ($this->dispatcher && $response = $this->dispatcher->trigger(new ErrorEvent($e))) {
                return $response;
            }
        } catch (\Throwable $eventException) {
            $this->log($eventException);

            $e = $eventException;
        }

        return $this->dumper->dumpExceptionResponse($e);
    }

    public function handleError(int $severity, string $message, string $file, int $line)
    {
        if (0 === error_reporting()) {
            $this->silencedMessage = $message;

            return;
        }

        $errorClass = static::SEVERITIES[$severity] ?? Error\ErrorException::class;

        throw new $errorClass($message, 500, $severity, $file, $line);
    }

    public function handleShutdown()
    {
        $error = error_get_last();

        if (!$this->enabled || !$error || !$error['type'] || $error['message'] === $this->silencedMessage) {
            return;
        }

        try {
            error_reporting(E_ALL);
            $this->handleError($error['type'], $error['message'], $error['file'], $error['line']);
        } catch (\Throwable $e) {
            $response = $this->handleException($e);
            $response->send();
        }
    }

    private function log(\Throwable $e)
    {
        $this->logger->log(
            $e instanceof LogLevelInterface ? $e->getLevel() : LogLevel::ERROR,
            $e
        );
    }
}
