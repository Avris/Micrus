<?php
namespace Avris\Micrus\Exception\Handler;

use Avris\Dispatcher\Event;
use Avris\Http\Response\ResponseInterface;

class ErrorEvent extends Event
{
    /** @var \Throwable */
    private $exception;

    /** @var ResponseInterface|null */
    private $response;

    public function __construct(\Throwable $exception)
    {
        $this->exception = $exception;
    }

    public function getName(): string
    {
        return 'error';
    }

    public function getException(): \Throwable
    {
        return $this->exception;
    }

    public function getResponse(): ?ResponseInterface
    {
        return $this->response;
    }

    public function setResponse(ResponseInterface $response): self
    {
        $this->response = $response;

        return $this;
    }

    public function setValue($value): Event
    {
        return $this->setResponse($value);
    }

    public function getValue()
    {
        return $this->getResponse();
    }
}
