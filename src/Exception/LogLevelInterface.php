<?php
namespace Avris\Micrus\Exception;

interface LogLevelInterface
{
    public function getLevel(): string;
}
