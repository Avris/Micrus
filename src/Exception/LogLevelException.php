<?php
namespace Avris\Micrus\Exception;

class LogLevelException extends \Exception implements LogLevelInterface
{
    /** @var string */
    private $logLevel;

    public function __construct(\Throwable $exception = null, string $logLevel)
    {
        parent::__construct($exception->getMessage(), $exception->getCode(), $exception);

        $this->logLevel = $logLevel;
    }

    public function getLevel(): string
    {
        return $this->logLevel;
    }
}
