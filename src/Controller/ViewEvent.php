<?php
namespace Avris\Micrus\Controller;

use Avris\Dispatcher\Event;
use Avris\Http\Response\ResponseInterface;

final class ViewEvent extends Event
{
    /** @var ResponseInterface|mixed */
    private $response;

    public function __construct($response = null)
    {
        $this->response = $response;
    }

    public function getName(): string
    {
        return 'view';
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setResponse(ResponseInterface $response = null): self
    {
        $this->response = $response;

        return $this;
    }

    public function setValue($value): Event
    {
        $this->response = $value;

        return $this;
    }

    public function getValue()
    {
        return $this->response;
    }
}
