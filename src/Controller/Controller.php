<?php
namespace Avris\Micrus\Controller;

use Avris\Forms\WidgetFactory;
use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Dispatcher\Event;
use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Http as Http;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Forms\Form;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Container\ContainerInterface;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\GuardianInterface;
use Avris\Micrus\Tool\Security\RoleCheckerInterface;
use Avris\Micrus\Tool\Security\SecurityManagerInterface;
use Avris\Micrus\View\TemplaterInterface;

abstract class Controller implements ControllerInterface
{
    /** @var ContainerInterface */
    protected $container;

    /** @var \Avris\Http\Cookie\CookieBag */
    protected $responseCookies;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->responseCookies = new Http\Cookie\CookieBag();
    }

    protected function get(string $serviceName)
    {
        return $this->container->get($serviceName);
    }

    protected function trigger(Event $event)
    {
        return $this->container->get(EventDispatcherInterface::class)->trigger($event);
    }

    protected function getProjectDir(): string
    {
        return $this->container->getParameter(AbstractApp::PROJECT_DIR);
    }

    protected function setCookie(
        $name,
        $value,
        $expire = 0,
        $path = null,
        $domain = null,
        $secure = false,
        $httpOnly = false
    ) {
        /** @var \Avris\Http\Request\RequestInterface $request */
        $request = $this->container->get(Http\Request\RequestInterface::class);

        $this->responseCookies->set(
            $name, // @codeCoverageIgnore
            $value, // @codeCoverageIgnore
            $expire, // @codeCoverageIgnore
            $path === null ? $request->getBase() : '/',
            $domain === null ? $request->getHost() : '',
            $secure, // @codeCoverageIgnore
            $httpOnly
        );

        return $this;
    }

    protected function addFlash(string $type, string $message, $deferred = true): self
    {
        $this->container->get(FlashBag::class)->add($type, $message, $deferred);

        return $this;
    }

    /**
     * @return UserInterface|\App\Entity\User|null
     */
    protected function getUser()
    {
        return $this->container->get(SecurityManagerInterface::class)->getUser();
    }

    protected function isGranted(string $role)
    {
        return $this->container->get(RoleCheckerInterface::class)->isUserGranted($role, $this->getUser());
    }

    protected function canAccess(string $check, $object = null): bool
    {
        return $this->container->get(GuardianInterface::class)->check($this->getUser(), $check, $object);
    }

    protected function generateUrl(string $route = RouterInterface::DEFAULT_ROUTE, array $params = []): string
    {
        return $this->container->get(RouterInterface::class)->generate($route, $params);
    }

    protected function render(
        array $vars = [],
        string $template = null,
        int $status = 200,
        array $headers = []
    ): Http\Response\Response {
        if (!$template) {
            $template = $this->container->get(RequestProviderInterface::class)->getRouteMatch()->getTarget();
            $template = strtr($template, ['App\Controller\\' => '', 'Controller' => '']);
        }

        return new Http\Response\Response(
            $this->container->get(TemplaterInterface::class)->render($template, $vars),
            $status, // @codeCoverageIgnore
            $headers, // @codeCoverageIgnore
            $this->responseCookies
        );
    }

    protected function renderJson($vars, int $status = 200): Http\Response\JsonResponse
    {
        return new Http\Response\JsonResponse($vars, $status, $this->responseCookies);
    }

    protected function redirect(string $url, int $status = 302): Http\Response\RedirectResponse
    {
        return new Http\Response\RedirectResponse($url, $status, $this->responseCookies);
    }

    protected function redirectToRoute(
        string $route = RouterInterface::DEFAULT_ROUTE,
        array $params = []
    ): Http\Response\RedirectResponse {
        return $this->redirect($this->generateUrl($route, $params));
    }

    protected function form(
        string $class,
        $object = null,
        Http\Request\RequestInterface $request = null,
        array $options = []
    ): Form {
        /** @var Form $form */
        $form = $this->get(WidgetFactory::class)->build($class, $options);

        if ($object !== null) {
            $form->bindObject($object);
        }

        if ($request !== null) {
            $form->bindRequest($request);
        }

        return $form;
    }

    protected function handleForm(Form $form, $object = null)
    {
        return $form->isValid() ? $form->buildObject($object) : null;
    }

    /**
     * @codeCoverageIgnore
     */
    public function __debugInfo()
    {
        $attr = get_object_vars($this);

        unset($attr['container']);

        return $attr;
    }
}
