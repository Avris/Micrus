<?php
namespace Avris\Micrus\Controller;

use Avris\Micrus\Controller\Routing\Model\RouteMatch;

interface ControllerResolverInterface
{
    public function resolveController(RouteMatch $routeMatch);
}
