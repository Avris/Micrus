<?php
namespace Avris\Micrus\Controller;

use Avris\Dispatcher\Event;
use Avris\Container\ContainerInterface;
use Avris\Http\Request\RequestInterface;
use Avris\Http\Response\ResponseInterface;

final class RequestEvent extends Event
{
    /** @var ContainerInterface */
    private $container;

    /** @var RequestInterface */
    private $request;

    /** @var ResponseInterface */
    private $response;

    public function __construct(ContainerInterface $container, RequestInterface $request)
    {
        $this->container = $container;
        $this->request = $request;
    }

    public function getName(): string
    {
        return 'request';
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function getRequest(): RequestInterface
    {
        return $this->request;
    }

    public function getResponse(): ?ResponseInterface
    {
        return $this->response;
    }

    public function setResponse(ResponseInterface $response): self
    {
        $this->response = $response;

        return $this;
    }

    public function setValue($value): Event
    {
        $this->response = $value;

        return $this;
    }

    public function getValue()
    {
        return $this->response;
    }
}
