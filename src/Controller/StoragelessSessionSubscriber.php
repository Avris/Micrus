<?php
namespace Avris\Micrus\Controller;

use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Http\Session\StoragelessSession;

/**
 * @codeCoverageIgnore
 */
class StoragelessSessionSubscriber implements EventSubscriberInterface
{
    /** @var StoragelessSession */
    private $session;

    public function __construct(StoragelessSession $session)
    {
        $this->session = $session;
    }

    public function getSubscribedEvents(): iterable
    {
        yield 'request' => function (RequestEvent $event) {
            $this->session->onRequest($event->getRequest());
        };
        yield 'response' => function (ResponseEvent $event) {
            $this->session->onResponse($event->getResponse());
        };
    }
}
