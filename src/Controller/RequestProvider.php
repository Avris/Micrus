<?php
namespace Avris\Micrus\Controller;

use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;

class RequestProvider implements RequestProviderInterface
{
    /** @var RouterInterface */
    private $router;

    /** @var RequestInterface */
    private $request;

    /** @var RouteMatch|null */
    private $routeMatch;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function reset(RequestInterface $request)
    {
        $this->request = $request;
        $this->routeMatch = null;
    }

    public function getRequest(): RequestInterface
    {
        return $this->request;
    }

    public function getRouteMatch(): ?RouteMatch
    {
        return $this->routeMatch ?: $this->routeMatch = $this->router->findRouteMatch($this->request);
    }
}
