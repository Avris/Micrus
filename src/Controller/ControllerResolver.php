<?php
namespace Avris\Micrus\Controller;

use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Container\ContainerInterface;

class ControllerResolver implements ControllerResolverInterface
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function resolveController(RouteMatch $routeMatch)
    {
        list($controller, $action) = explode('/', $routeMatch->getTarget());

        return [
            $this->container->get($controller),
            $action . 'Action',
        ];
    }
}
