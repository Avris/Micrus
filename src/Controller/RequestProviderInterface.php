<?php
namespace Avris\Micrus\Controller;

use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;

interface RequestProviderInterface
{
    public function reset(RequestInterface $request);

    public function getRequest(): RequestInterface;

    public function getRouteMatch(): ?RouteMatch;
}
