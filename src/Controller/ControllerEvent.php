<?php
namespace Avris\Micrus\Controller;

use Avris\Dispatcher\Event;
use Avris\Http\Request\RequestInterface;
use Avris\Http\Response\ResponseInterface;

final class ControllerEvent extends Event
{
    /** @var RequestInterface */
    private $request;

    /** @var string */
    private $target;

    /** @var callable */
    private $controller;

    /** @var array */
    private $arguments;

    /** @var ResponseInterface */
    private $response;

    public function __construct(RequestInterface $request, string $target, callable $controller, array $arguments)
    {
        $this->request = $request;
        $this->target = $target;
        $this->controller = $controller;
        $this->arguments = $arguments;
    }

    public function getName(): string
    {
        return 'controller';
    }

    public function getRequest(): RequestInterface
    {
        return $this->request;
    }

    public function getTarget(): string
    {
        return $this->target;
    }

    public function getController(): callable
    {
        return $this->controller;
    }

    public function setController(callable $controller): self
    {
        $this->controller = $controller;

        return $this;
    }

    public function getArguments(): array
    {
        return $this->arguments;
    }

    public function setArguments(array $arguments): self
    {
        $this->arguments = $arguments;

        return $this;
    }

    public function getResponse(): ?ResponseInterface
    {
        return $this->response;
    }

    public function setResponse(ResponseInterface $response): self
    {
        $this->response = $response;

        return $this;
    }

    public function setValue($value): Event
    {
        list($this->response, $this->controller, $this->arguments) = $value;

        return $this;
    }

    public function getValue()
    {
        return [$this->response, $this->controller, $this->arguments];
    }
}
