<?php
namespace Avris\Micrus\Controller\Routing\Service;

use Avris\Micrus\Controller\Routing\Model\CompiledRoute;
use Avris\Micrus\Controller\Routing\Model\Route;

final class RouteCompiler
{
    public function compile(Route $route): CompiledRoute
    {
        $tagReplacements = [];
        $tagNames = [];

        preg_match_all('#(\.|\/|-)?{(.*)}#Ui', $route->getPattern(), $matches, PREG_SET_ORDER);

        foreach ($matches as list(, $pre, $name)) {
            $preEscaped = $pre ? '\\' . $pre : '';

            $replacementKey = $preEscaped . '{' . addcslashes($name, '/+-.$^') . '}';
            $replacementValue = sprintf(
                '(?:%s(%s))%s',
                $preEscaped, // @codeCoverageIgnore
                $route->getRequirement($name) ?: ('[^\/\.]' . ($route->hasDefault($name) ? '*' : '+')),
                $route->hasDefault($name) ? '?' : ''
            );
            $tagReplacements[$replacementKey] = $replacementValue;

            $tagNames[] = $name;
        }

        $regex = sprintf('/^%s$/Uui', strtr(
            addcslashes($route->getPattern(), '/+-$^.'),
            $tagReplacements
        ));

        return new CompiledRoute($route, $regex, $tagNames);
    }
}
