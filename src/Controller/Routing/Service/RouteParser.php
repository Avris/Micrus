<?php
namespace Avris\Micrus\Controller\Routing\Service;

use Avris\Micrus\Controller\Routing\Model\Route;
use Avris\Micrus\Exception\ConfigException;
use Avris\Micrus\Exception\InvalidArgumentException;

final class RouteParser
{
    const REGEX_METHOD = '(?:([A-Z,]+)\s+)?';
    const REGEX_SCHEMAS = '(?:([a-z,]+)://)?';
    const REGEX_HOST = '([^/]+)?';
    const REGEX_PATTERN = '(\S+)';
    const REGEX_TARGET = '(\S+)';
    const REGEX_OPTIONS = '(?:\s+(.*))?';

    const REGEX = '#^' . self::REGEX_METHOD . self::REGEX_SCHEMAS . self::REGEX_HOST . self::REGEX_PATTERN
        . '\s+->\s+' . self::REGEX_TARGET . self::REGEX_OPTIONS . '$#';

    public function parse($name, $string, $base = ''): Route
    {
        $escapedRegexes = [];

        $escapedString = preg_replace_callback('#<(.*)>#Ui', function ($regex) use (&$escapedRegexes) {
            $key = '@' . count($escapedRegexes) . '@';
            $escapedRegexes[$key] = $regex[1];

            return $key;
        }, $string);

        if (!preg_match(self::REGEX, $escapedString, $matches)) {
            throw new InvalidArgumentException(sprintf('Cannot parse "%s" as route string', $string));
        }

        $methods = $matches[1] ? explode(',', $matches[1]) : [];
        $schemas = $matches[2] ? explode(',', $matches[2]) : [];
        $host = $matches[3] ? strtr($matches[3], $escapedRegexes) : null;
        $pattern = $matches[4];
        $target = $matches[5];
        $options = isset($matches[6]) ? json_decode($matches[6], true) : [];

        $defaults = [];
        $requirements = [];

        $pattern = preg_replace_callback(
            '#{(.*)}#Ui',
            function ($matches) use ($escapedRegexes, &$defaults, &$requirements) {
                list($requirement, $name, $default) = $this->splitTagName($matches[1], $escapedRegexes);

                if ($requirement !== null) {
                    $requirements[$name] = $requirement;
                }

                if ($default !== null) {
                    $defaults[$name] = $default;
                }

                return '{' . $name . '}';
            },
            $pattern
        );

        return new Route(
            $name,  // @codeCoverageIgnore
            rtrim($base . $pattern, '/'),
            $target, // @codeCoverageIgnore
            $defaults, // @codeCoverageIgnore
            $requirements, // @codeCoverageIgnore
            $methods, // @codeCoverageIgnore
            $host, // @codeCoverageIgnore
            $schemas, // @codeCoverageIgnore
            $options // @codeCoverageIgnore
        );
    }

    private function splitTagName(string $tagName, array $escapedRegexes): array
    {
        $restriction = $name = $default = null;

        if (!preg_match('#^(?:([^:]+):)?([\w\.]+)(?:=([^=]*))?$#', $tagName, $matches)) {
            throw new ConfigException(sprintf('Invalid route tag name "%s"', $tagName));
        }

        $name = $matches[2];

        if (isset($matches[3])) {
            $default = $matches[3];
        }

        if (isset($matches[1]) && $matches[1]) {
            $restriction = Route::expandShortcut($matches[1], $default !== null);
        }

        return [
            $restriction !== null ? strtr($restriction, $escapedRegexes) : null,
            strtr($name, $escapedRegexes),
            $default !== null ? strtr($default, $escapedRegexes) : null,
        ];
    }
}
