<?php
namespace Avris\Micrus\Controller\Routing\Service;

use Avris\Bag\Bag;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\Routing\Model\CompiledRoute;
use Avris\Micrus\Controller\Routing\Model\Route;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;

interface RouterInterface
{
    const DEFAULT_ROUTE = 'home';

    /**
     * @param Route[]|array[]|string[]|Bag $routes
     */
    public function addRoutes($routes): self;

    /**
     * @param CompiledRoute|Route|array|string $route
     */
    public function addRoute(string $name, $route): self;

    public function findRouteMatch(RequestInterface $request): ?RouteMatch;

    /**
     * @param string|string[] $routeName
     */
    public function generate(
        $routeName = self::DEFAULT_ROUTE,
        array $params = [],
        bool $absolute = false,
        ?string $frontController = null
    ): string;

    public function prependToUrl(string $url, bool $absolute = false, ?string $frontController = null): string;

    /**
     * @param string|string[] $routeName
     */
    public function routeExists($routeName): bool;

    public function getDefaultRoute(): string;

    /**
     * @return CompiledRoute[]
     */
    public function getRoutes(): array;
}
