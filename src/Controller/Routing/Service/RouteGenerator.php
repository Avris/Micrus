<?php
namespace Avris\Micrus\Controller\Routing\Service;

use Avris\Micrus\Controller\Routing\Model\Route;
use Avris\Micrus\Exception\InvalidArgumentException;

final class RouteGenerator
{
    public function generate(Route $route, array $params = []): string
    {
        $oneWayDefaults = (array) $route->getOption('oneWayDefault');

        $url = preg_replace_callback(
            '/(\.|\/|-)?{(.*)}/Ui',
            function ($matches) use ($route, &$params, $oneWayDefaults) {
                list(, $pre, $tagName) = $matches;

                $default = $route->getDefault($tagName);
                $showDefault = in_array($tagName, $oneWayDefaults);

                if (isset($params[$tagName])) {
                    $val = $params[$tagName];
                    unset($params[$tagName]);

                    if (!$val) {
                        return $showDefault ? $pre . $default : '';
                    }

                    if (!$showDefault && $val == $default) {
                        return '';
                    }

                    $this->assertTagValueCorrect($route, $tagName, $val);

                    return $pre . $val;
                }

                if ($default === null) {
                    throw new InvalidArgumentException(sprintf(
                        'Parameter "%s" is required for route "%s"',
                        $tagName, // @codeCoverageIgnore
                        $route->getName()
                    ));
                }

                return $showDefault ? $pre . $default : '';
            },
            $route->getPattern()
        );

        return str_replace('?', '', $url) . (count($params) ? '?' . http_build_query($params) : '');
    }

    private function assertTagValueCorrect(Route $route, string $tagName, string $tagValue)
    {
        if (!$route->hasRequirement($tagName)) {
            return;
        }

        $requirement = $route->getRequirement($tagName);
        if (!preg_match('#^' . $requirement . '$#', $tagValue)) {
            throw new InvalidArgumentException(sprintf(
                'Value "%s" invalid for tag "%s" in route "%s" (required "%s")',
                $tagValue, // @codeCoverageIgnore
                $tagName, // @codeCoverageIgnore
                $route->getName(),
                $requirement // @codeCoverageIgnore
            ));
        }
    }
}
