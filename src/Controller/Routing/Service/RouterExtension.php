<?php
namespace Avris\Micrus\Controller\Routing\Service;

use Avris\Micrus\Controller\Routing\Model\CompiledRoute;
use Avris\Micrus\Controller\Routing\Model\Route;

interface RouterExtension
{
    public function add(Route $route): Route;

    /**
     * @return array [string $generated, ?CompiledRoute $route, array $params]
     */
    public function generate(string $name, ?CompiledRoute $route, array $params = []): array;
}
