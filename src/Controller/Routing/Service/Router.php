<?php
namespace Avris\Micrus\Controller\Routing\Service;

use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\Routing\Model\CompiledRoute;
use Avris\Micrus\Controller\Routing\Model\Route;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Exception\NotFoundException;
use Avris\Bag\Bag;
use Avris\Micrus\Tool\Cache\CacherInterface;
use Psr\Log\LoggerInterface;

final class Router implements RouterInterface
{
    /** @var CompiledRoute[]|Bag */
    private $routes;

    /** @var string */
    private $frontController;

    /** @var string */
    private $base;

    /** @var string */
    private $absoluteBase;

    /** @var LoggerInterface */
    private $logger;

    /** @var RouterExtension[] */
    private $extensions;

    /** @var RouteParser */
    private $parser;

    /** @var RouteCompiler */
    private $compiler;

    /** @var RouteGenerator */
    private $generator;

    public function __construct(
        RequestInterface $request,
        CacherInterface $cacher,
        LoggerInterface $logger,
        array $routerExtensions,
        Bag $configRouting,
        RouteParser $parser,
        RouteCompiler $compiler,
        RouteGenerator $generator
    ) {
        $this->base = $request->getBase();
        $this->absoluteBase = $request->getAbsoluteBase();
        $this->frontController = $request->getFrontController();
        $this->logger = $logger;
        $this->extensions = $routerExtensions;

        $this->parser = $parser;
        $this->compiler = $compiler;
        $this->generator = $generator;

        $this->routes = $cacher->cache('routing', function () use ($configRouting) {
            $this->routes = new Bag();
            $this->addRoutes($configRouting);
            return $this->routes;
        });
    }

    public function addRoutes($routes): RouterInterface
    {
        if (!empty($routes)) {
            foreach ($routes as $name => $route) {
                $this->addRoute($name, $route);
            }
        }

        return $this;
    }
    
    public function addRoute(string $name, $route): RouterInterface
    {
        if ($route instanceof CompiledRoute) {
            $this->routes->set($name, $route);

            return $this;
        }

        if (!$route instanceof Route) {
            $route = is_array($route) ? Route::fromArray($name, $route) : $this->parser->parse($name, $route);
        }

        foreach ($this->extensions as $extension) {
            $route = $extension->add($route);
        }

        $this->routes->set($route->getName(), $this->compiler->compile($route));

        return $this;
    }

    public function findRouteMatch(RequestInterface $request): ?RouteMatch
    {
        foreach ($this->routes as $name => $route) {
            $this->logger->debug(sprintf('Checking route %s', $name));

            if ($match = $route->matches($request)) {
                $this->logger->info(sprintf('Route %s matched', $name));
                return $match;
            }
        }

        return null;
    }

    /**
     * @param string|string[] $routeName
     */
    public function generate(
        $routeName = self::DEFAULT_ROUTE,
        array $params = [],
        bool $absolute = false,
        ?string $frontController = null
    ): string {
        if (!is_array($routeName)) {
            return $this->getUrlSingle($routeName, $params, $absolute, $frontController);
        }

        foreach ($routeName as $singleRoute) {
            if ($this->routes->offsetExists($singleRoute)) {
                return $this->getUrlSingle($singleRoute, $params, $absolute, $frontController);
            }
        }

        throw new NotFoundException(sprintf('None of the routes (%s) found', implode(', ', $routeName)));
    }

    private function getUrlSingle(
        string $routeName,
        array $params = [],
        bool $absolute = false,
        ?string $frontController = null
    ): string {
        /** @var CompiledRoute $route */
        $route = $this->routes->get($routeName);

        foreach ($this->extensions as $extension) {
            list($generated, $route, $params) = $extension->generate($routeName, $route, $params);
            if ($generated !== null) {
                return $this->prependToUrl($generated, $absolute, $frontController);
            }
        }

        if (!$route) {
            throw new NotFoundException(sprintf('Route named "%s" not found', $routeName));
        }

        return $this->prependToUrl($this->generator->generate($route, $params), $absolute, $frontController);
    }

    public function prependToUrl(string $url, bool $absolute = false, ?string $frontController = null): string
    {
        return sprintf(
            '%s%s%s%s',
            $absolute ? $this->absoluteBase : '',
            $this->base,
            $frontController === null ? $this->frontController : $frontController,
            $url
        );
    }

    /**
     * @param string|string[] $routeName
     */
    public function routeExists($routeName): bool
    {
        if (!is_array($routeName)) {
            return $this->routes->offsetExists($routeName);
        }

        foreach ($routeName as $singleRoute) {
            if ($this->routes->offsetExists($singleRoute)) {
                return true;
            }
        }

        return false;
    }

    public function getDefaultRoute(): string
    {
        return static::DEFAULT_ROUTE;
    }

    public function getRoutes(): array
    {
        return $this->routes->all();
    }
}
