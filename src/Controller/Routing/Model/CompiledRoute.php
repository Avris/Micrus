<?php
namespace Avris\Micrus\Controller\Routing\Model;

use Avris\Http\Request\RequestInterface;

class CompiledRoute extends Route
{
    /** @var string */
    protected $regex;

    /** @var string[] */
    protected $tagNames;

    public function __construct(Route $route, $regex, array $tagNames)
    {
        parent::__construct(
            $route->getName(),
            $route->getPattern(),
            $route->getTarget(),
            $route->getDefaults(),
            $route->getRequirements(),
            $route->getMethods(),
            $route->getHost(),
            $route->getSchemes(),
            $route->getOptions()
        );

        $this->regex = $regex;
        $this->tagNames = $tagNames;
    }

    /**
     * @param RequestInterface $request
     * @return RouteMatch
     */
    public function matches(RequestInterface $request)
    {
        if (!$this->methods->isEmpty() && !$this->methods->has($request->getMethod())) {
            return null;
        }

        if (!$this->schemes->isEmpty() && !$this->schemes->has($request->getScheme())) {
            return null;
        }

        if ($this->host) {
            if (!preg_match('#^' . $this->host . '$#i', $request->getHost())) {
                return null;
            }
        }

        if (!preg_match($this->regex, $request->getCleanUrl(), $matches)) {
            return null;
        }

        return new RouteMatch(
            $this, // @codeCoverageIgnore
            $matches, // @codeCoverageIgnore
            $this->defaults,
            $this->tagNames
        );
    }

    /**
     * @return string[]
     */
    public function getTagNames()
    {
        return $this->tagNames;
    }
}
