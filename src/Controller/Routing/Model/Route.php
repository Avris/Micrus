<?php
namespace Avris\Micrus\Controller\Routing\Model;

use Avris\Bag\Set;

class Route implements \JsonSerializable
{
    protected static $requirementShortcuts = [
        'int' => ['\d*', '\d+'],
        'float' => ['(?:\d+(?:\.\d+)?)?', '\d+(?:\.\d+)?'],
        'path' => '.*',
        'uuid' => '[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}',
    ];

    /** @var string */
    protected $name;

    /** @var string */
    protected $pattern;

    /** @var string */
    protected $target;

    /** @var string[] */
    protected $defaults;

    /** @var string[] */
    protected $requirements;

    /** @var Set|string[] */
    protected $methods;

    /** @var string */
    protected $host;

    /** @var Set|string[] */
    protected $schemes;

    /** @var array */
    protected $options;

    /**
     * @param string $name
     * @param string $pattern           eg. '/post/{id}/show.html'
     * @param string $target            eg. 'App\Controller\PostController/show'
     * @param array $defaults           eg. ['id' => 1]
     * @param array $requirements       eg. ['id' => '\d+']
     * @param Set|string[] $methods     eg. ['POST']
     * @param string|null $host         eg. '.*\.website\.com'
     * @param Set|string[] $schemes     eg. ['http', 'https']
     * @param array $options            eg. ['oneWayDefault' => ['id'], 'immutable' => true]
     */
    public function __construct(
        string $name,
        string $pattern,
        string $target,
        array $defaults = [],
        array $requirements = [],
        $methods = [],
        string $host = null,
        $schemes = [],
        array $options = []
    ) {
        $this->setName($name);
        $this->setPattern($pattern);
        $this->setTarget($target);
        $this->setDefaults($defaults);
        $this->setRequirements($requirements);
        $this->setMethods($methods);
        $this->setHost((string) $host);
        $this->setSchemes($schemes);
        $this->setOptions($options);
    }

    public static function fromArray(string $name, array $options): self
    {
        return new static(
            $name, // @codeCoverageIgnore
            $options['pattern'],
            $options['target'],
            isset($options['defaults']) ? $options['defaults'] : [],
            isset($options['requirements']) ? $options['requirements'] : [],
            isset($options['methods']) ? $options['methods'] : [],
            isset($options['host']) ? $options['host'] : null,
            isset($options['schemes']) ? $options['schemes'] : [],
            isset($options['options']) ? $options['options'] : []
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPattern()
    {
        return $this->pattern;
    }

    public function setPattern($pattern): self
    {
        $this->pattern = '/' . trim(trim($pattern), '/');

        return $this;
    }

    public function getTarget(): string
    {
        return $this->target;
    }

    public function setTarget(string $target): self
    {
        $this->target = $target;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getDefaults(): array
    {
        return $this->defaults;
    }

    public function getDefault(string $tag): ?string
    {
        return isset($this->defaults[$tag]) ? $this->defaults[$tag] : null;
    }

    public function hasDefault(string $tag): bool
    {
        return isset($this->defaults[$tag]);
    }

    /**
     * @param string[] $defaults
     */
    public function setDefaults(array $defaults): self
    {
        $this->defaults = $defaults;

        return $this;
    }

    public function addDefault(string $tag, string $default): self
    {
        $this->defaults[$tag] = $default;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getRequirements(): array
    {
        return $this->requirements;
    }

    public function getRequirement(string $tag): ?string
    {
        return isset($this->requirements[$tag]) ? $this->requirements[$tag] : null;
    }

    public function hasRequirement(string $tag): bool
    {
        return isset($this->requirements[$tag]);
    }

    /**
     * @param string[] $requirements
     */
    public function setRequirements(array $requirements): self
    {
        $this->requirements = $requirements;

        return $this;
    }

    public function addRequirement(string $tag, string $requirement): self
    {
        $this->requirements[$tag] = (string) $requirement;

        return $this;
    }

    /**
     * @return Set|string[]
     */
    public function getMethods(): Set
    {
        return $this->methods;
    }

    /**
     * @param Set|string[] $methods
     */
    public function setMethods($methods): self
    {
        $this->methods = new Set($methods, 'strtoupper');

        return $this;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function setHost(string $host): self
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @return Set|string[]
     */
    public function getSchemes(): Set
    {
        return $this->schemes;
    }

    /**
     * @param Set|string[] $schemes
     */
    public function setSchemes($schemes): self
    {
        $this->schemes = new Set($schemes, 'strtolower');

        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getOption(string $option)
    {
        return isset($this->options[$option]) ? $this->options[$option] : null;
    }

    public function setOptions(array $options): self
    {
        $this->options = [];
        foreach ($options as $key => $value) {
            $this->setOption($key, $value);
        }

        return $this;
    }

    public function setOption(string $option, $value): self
    {
        if ($option === 'defaults') {
            foreach ($value as $defaultKey => $defaultValue) {
                $this->addDefault($defaultKey, $defaultValue);
            }
            return $this;
        }

        if ($option === 'requirements') {
            foreach ($value as $requirementKey => $requirementsValue) {
                $this->addRequirement($requirementKey, $requirementsValue);
            }
            return $this;
        }

        $this->options[$option] = $value;

        return $this;
    }

    public function jsonSerialize()
    {
        $data = [
            'name' => $this->name,
            'pattern' => $this->pattern,
            'target' => $this->target,
        ];

        foreach (['defaults', 'requirements', 'host', 'options'] as $key) {
            if (!empty($this->$key)) {
                $data[$key] = $this->$key;
            }
        }

        foreach (['methods', 'schemes'] as $key) {
            if (!$this->$key->isEmpty()) {
                $data[$key] = $this->$key;
            }
        }

        return $data;
    }

    public function __toString(): string
    {
        $out = '';

        if (!$this->methods->isEmpty()) {
            $out .= implode(',', $this->methods->all()) . ' ';
        }

        if (!$this->schemes->isEmpty()) {
            $out .= implode(',', $this->schemes->all()) . '://';
        }

        if ($this->host) {
            $out .= $this->escapeRegex($this->host);
        }

        $defaults = $this->defaults;
        $requirements = $this->requirements;
        $options = $this->options;

        $out .= preg_replace_callback('#{(.*)}#Ui', function ($matches) use (&$defaults, &$requirements) {
            $out = $tagName = $matches[1];

            if (isset($defaults[$tagName])) {
                $out .= '=' . $this->escapeRegex($defaults[$tagName]);
                unset($defaults[$tagName]);
            }

            if (isset($requirements[$tagName])) {
                $out = $this->escapeRegex($requirements[$tagName]) . ':' . $out;
                unset($requirements[$tagName]);
            }

            return '{' . $out . '}';
        }, $this->pattern);

        $out .= ' -> ' . $this->target;

        if (count($defaults)) {
            $options['defaults'] = $defaults;
        }

        if (count($requirements)) {
            $options['requirements'] = $requirements;
        }

        if (count($options)) {
            $out .= ' ' . json_encode($options);
        }

        return $out;
    }

    protected function escapeRegex($string)
    {
        if (isset(static::$requirementShortcuts[$string])) {
            return '<' . $string . '>';
        }

        $shortcut = static::shortcut($string);
        if ($shortcut !== null) {
            return $shortcut;
        }

        $toRemove = ['\\', '^', '$', '{', '}', '[', ']', '(', ')', '.', '*', '+', '?', '|', '-', '&', ' ', '='];
        $cleared = str_replace($toRemove, '', $string);

        return $string === $cleared
            ? $string
            : '<' . $string . '>';
    }

    public static function addRequirementShortcut($shortcut, $requirement)
    {
        static::$requirementShortcuts[$shortcut] = $requirement;
    }

    public static function expandShortcut($requirement, $hasDefault)
    {
        if (!isset(static::$requirementShortcuts[$requirement])) {
            return $requirement;
        }

        $shortcut = static::$requirementShortcuts[$requirement];
        return is_array($shortcut)
            ? ($hasDefault ? $shortcut[0] : $shortcut[1])
            : $shortcut;
    }

    public static function shortcut($requirement)
    {
        foreach (static::$requirementShortcuts as $shortcut => $expansion) {
            if (in_array($requirement, (array) $expansion)) {
                return $shortcut;
            }
        }

        return null;
    }
}
