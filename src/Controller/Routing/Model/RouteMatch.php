<?php
namespace Avris\Micrus\Controller\Routing\Model;

class RouteMatch implements \JsonSerializable
{
    /** @var Route */
    private $route;

    /** @var string[] */
    private $tags;

    /** @var string[] */
    private $query;

    /**
     * @param Route $route
     * @param string[] $matches
     * @param string[] $defaults
     * @param string[] $tagNames
     */
    public function __construct(Route $route, array $matches, array $defaults, array $tagNames)
    {
        $this->route = $route;
        $this->query = $defaults;
        $this->tags = $this->extractTags($matches, $tagNames);
    }

    /**
     * @param string[] $matches
     * @param string[] $tagNames
     * @return string[]
     */
    private function extractTags(array $matches, array $tagNames): array
    {
        $tags = [];
        for ($i = 1; $i < count($matches); $i++) {
            $tagName = $tagNames[$i - 1];
            $tagValue = $matches[$i];
            if (!$tagValue && isset($this->query[$tagName])) {
                $tagValue = $this->query[$tagName];
            }
            $tags[$tagName] = $tagValue;
            unset($this->query[$tagName]);
        }

        foreach ($this->query as $tagName => $defaultValue) {
            if (in_array($tagName, $tagNames)) {
                $tags[$tagName] = $defaultValue;
                unset($this->query[$tagName]);
            }
        }

        return $tags;
    }

    public function getRoute(): Route
    {
        return $this->route;
    }

    public function getTarget(): string
    {
        return $this->route->getTarget();
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function getQuery(): array
    {
        return $this->query;
    }

    public function jsonSerialize()
    {
        return [
            'route' => $this->route,
            'tags' => $this->tags,
            'query' => $this->query,
        ];
    }
}
