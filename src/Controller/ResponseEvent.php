<?php
namespace Avris\Micrus\Controller;

use Avris\Dispatcher\Event;
use Avris\Http\Response\ResponseInterface;

final class ResponseEvent extends Event
{
    /** @var ResponseInterface */
    private $response;

    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function getName(): string
    {
        return 'response';
    }

    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    public function setResponse(ResponseInterface $response): self
    {
        $this->response = $response;

        return $this;
    }

    public function setValue($value): Event
    {
        return $this->setResponse($value);
    }

    public function getValue()
    {
        return $this->getResponse();
    }
}
