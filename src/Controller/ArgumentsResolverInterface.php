<?php
namespace Avris\Micrus\Controller;

use Avris\Micrus\Controller\Routing\Model\RouteMatch;

interface ArgumentsResolverInterface
{
    public function resolveArguments(callable $controller, RouteMatch $routeMatch): array;
}
