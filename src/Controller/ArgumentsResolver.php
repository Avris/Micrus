<?php
namespace Avris\Micrus\Controller;

use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Model\MatchProvider;
use Avris\Bag\QueueBag;

class ArgumentsResolver implements ArgumentsResolverInterface
{
    /** @var MatchProvider[] */
    private $providers;

    public function __construct(array $matchProviders)
    {
        $this->providers = $matchProviders;
    }

    public function resolveArguments(callable $controller, RouteMatch $routeMatch): array
    {
        $parameters = $this->getReflector($controller)->getParameters();

        if (empty($parameters)) {
            return [];
        }

        $arguments = [];
        $tags = new QueueBag(array_filter(
            $routeMatch->getTags(),
            function ($value, $key) {
                return substr($key, 0, 1) !== '_';
            },
            ARRAY_FILTER_USE_BOTH
        ));

        foreach ($parameters as $parameter) {
            $arguments['$' . $parameter->getName()] = $this->fetchValueForParameter($parameter, $tags);
        }

        return $arguments;
    }

    private function getReflector(callable $callable): \ReflectionFunctionAbstract
    {
        if ($callable instanceof \Closure) {
            return new \ReflectionFunction($callable);
        }

        if (is_object($callable)) {
            return new \ReflectionMethod($callable, '__invoke');
        }

        if (is_string($callable)) {
            $callable = explode('::', $callable);
        }

        if (count($callable) == 1) {
            return new \ReflectionFunction($callable[0]);
        }

        return new \ReflectionMethod(is_object($callable[0]) ? get_class($callable[0]) : $callable[0], $callable[1]);
    }

    private function fetchValueForParameter(\ReflectionParameter $parameter, QueueBag $tags)
    {
        foreach ($this->providers as $provider) {
            if ($provider->supports($parameter)) {
                return $provider->fetch($parameter, $tags);
            }
        }

        if ($tags->isEmpty()) {
            if ($parameter->isDefaultValueAvailable()) {
                return $parameter->getDefaultValue();
            }

            throw new \RuntimeException(sprintf(
                'Argument %s $%s is expected for a controller, '
                . 'but there\'s not enough route tags and no MatchProvider supports it',
                $parameter->getType(),
                $parameter->getName()
            ));
        }

        list(, $value) = $tags->dequeue();

        return $value;
    }
}
