# Micrus • Simple, but powerful, PHP framework

A framework doesn't have to be overly complex!
Micrus provides you with a quick, easy and comfortable
way of creating neatly structured, modular MVC websites,
which can be easily extended and configured.

Our goal is to keep the framework as simple as possible,
while offering all the most important features.

* Object oriented
* Very clear MVC structure
* Easily extendable via modules
* Dependency Injection Container (PSR-11 compliant)
* Autowiring of services
* Event dispatching
* ORM agnostic, [support](https://gitlab.com/Avris/Micrus-Doctrine) for [Doctrine](http://www.doctrine-project.org/projects/orm.html)
* Routing
* [CRUD generator](https://gitlab.com/Avris/Micrus-Crud)
* Template agnostic, support for [Twig](http://twig.sensiolabs.org/) and plain PHP
* Authentication and authorization
* Easy configuration using [YAML](http://yaml.org/)
* Console tasks using [Symfony Console](http://symfony.com/doc/current/components/console/introduction.html)
* Elegant [forms](https://gitlab.com/Avris/Micrus-Forms)
* [Localisation](https://gitlab.com/Avris/Micrus-Localisator)
* Logging with [Monolog](https://github.com/Seldaek/monolog)  (PSR-3 compliant)
* Caching (PSR-6 compliant)
* Flash messages

## Documentation

Full documentation is available at **[docs.avris.it/micrus](https://docs.avris.it/micrus/)**

## Instalation

    $ composer create-project avris/micrus-demo my_new_project
    $ cd my_new_project
    $ bin/env
    $ bin/micrus db:schema:create
    $ bin/micrus db:fixtures
    $ yarn
    $ yarn server
    $ php -S localhost:8070 -t public/

Your website should be available under:

	http://localhost:8070

## Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
