<?php
namespace Avris\Micrus\Controller;

use Avris\Container\ContainerInterface;
use Avris\FunctionMock\FunctionMock;
use Avris\Http\Request\Request;
use Avris\Http\Response\Response;
use Avris\Micrus\Exception\Http\NotFoundHttpException;
use Avris\Micrus\MicrusModule;
use TestApp\TestApp;
use PHPUnit\Framework\TestCase;
use TestApp\TestAppBroken;

/**
 * @covers \Avris\Micrus\Bootstrap\AbstractApp
 * @covers \Avris\Micrus\Exception\LogLevelException
 */
class AppIntegrationTest extends TestCase
{
    /** @var FunctionMock */
    private $fwriteMock;

    protected function setUp()
    {
        putenv('APP_SECRET=abcde');
        putenv('URL_SCHEME=https');
        putenv('URL_HOST=micrus.dev');
        putenv('URL_BASE=localhost');

        $this->fwriteMock = FunctionMock::create('Avris\Micrus\Tool', 'fwrite', true)->disable();
    }

    protected function tearDown()
    {
        FunctionMock::clean();
    }

    public function testHandle()
    {
        $app = new TestApp('test', true);

        $request = Request::create('GET', '/');

        $response = $app->handle($request);
        $app->terminate();

        $this->assertEquals(new Response('OK'), $response);

        $this->assertInstanceOf(ContainerInterface::class, $app->getContainer());
        $this->assertEquals('test', $app->getEnv());

        $micrusModule = new MicrusModule();
        $micrusModule->getDir();
        $this->assertEquals([
            'Avris\\Micrus' => $micrusModule,
            'TestApp' => $app,
        ], $app->getModules());
    }

    public function testHandleInvalidWarmup()
    {
        $app = new TestAppBroken('test', true);

        $request = Request::create('GET', '/');

        $this->fwriteMock->enable();

        $response = $app->handle($request);
        $app->terminate();

        $invocations = $this->fwriteMock->getInvocations();
        $this->assertCount(1, $invocations);
        $this->assertSame(STDERR, $invocations[0][0]);
        $this->assertContains('app.critical: RuntimeException: Emulate broken warmup', $invocations[0][1]);
        $this->fwriteMock->disable();

        $this->assertInstanceOf(Response::class, $response);
        $this->assertContains('Emulate broken warmup', $response->getContent());
        $this->assertContains('RuntimeException', $response->getContent());
        $this->assertEquals(500, $response->getStatus());
    }

    public function testHandleException()
    {
        $app = new TestApp('test', true);

        $request = Request::create('GET', '/fail');

        $response = $app->handle($request);
        $app->terminate();

        $this->assertInstanceOf(Response::class, $response);
        $this->assertContains('Nah', $response->getContent());
        $this->assertContains(NotFoundHttpException::class, $response->getContent());
        $this->assertEquals(404, $response->getStatus());
    }
}
