<?php
namespace Avris\Micrus\Model;

use Avris\Container\Container;
use Avris\Micrus\Controller\ControllerInterface;
use Avris\Micrus\Test\TestService;
use Avris\Bag\QueueBag;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Model\ContainerMatchProvider
 */
class ContainerMatchProviderTest extends TestCase
{
    public function testMatch()
    {
        $container = new Container();
        $service = new TestService();
        $container->set(TestService::class, $service);

        $parameters = (new \ReflectionObject($this))->getMethod('foo')->getParameters();
        $tags = new QueueBag();

        $provider = new ContainerMatchProvider($container);

        $this->assertTrue($provider->supports($parameters[0]));
        $this->assertSame($service, $provider->fetch($parameters[0], $tags));

        $this->assertFalse($provider->supports($parameters[1]));

        $this->assertFalse($provider->supports($parameters[2]));
    }

    public function foo(TestService $service, ControllerInterface $controller, $bar)
    {
    }
}
