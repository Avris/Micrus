<?php
namespace Avris\Micrus\Model\User;;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Model\User\MemoryUser
 */
class MemoryUserTest extends TestCase
{
    public function testSimple()
    {
        $user = new MemoryUser('tester', [
            'id' => 5,
            'roles' => ['ROLE_USER'],
            'foo' => 'bar',
            'authenticators' => [
                'password' => 'pwd',
            ],
        ]);
        $this->assertInstanceOf(UserInterface::class, $user);

        $this->assertEquals(5, $user['id']);
        $this->assertEquals('tester', $user->getIdentifier());
        $this->assertEquals('tester', (string) $user);
        $this->assertEquals(['ROLE_USER'], $user->getRoles());
        $this->assertTrue($user->hasRole('ROLE_USER'));
        $this->assertFalse($user->hasRole('ROLE_ADMIN'));
        $this->assertFalse($user->isAdmin());

        $expectedAuth1 = new MemoryAuhtenticator('password', 'pwd');
        $this->assertEquals([$expectedAuth1], $user->getAuthenticators());

        $expectedAuth2 = new MemoryAuhtenticator('type', 'ok');
        $auth = $user->createAuthenticator('type', 'ok');
        $this->assertEquals($expectedAuth2, $auth);
        $this->assertEquals([$expectedAuth1, $expectedAuth2], $user->getAuthenticators());
        $this->assertEquals([$expectedAuth2], $user->getAuthenticators('type'));
        $this->assertEquals([], $user->getAuthenticators('otherType'));

        $this->assertTrue(isset($user['foo']));
        $this->assertFalse(isset($user['lorem']));
        $this->assertEquals('bar', $user['foo']);
        $user['foo'] = 'baz';
        $this->assertEquals('baz', $user['foo']);
        unset($user['foo']);
        $this->assertFalse(isset($user['foo']));
    }
}