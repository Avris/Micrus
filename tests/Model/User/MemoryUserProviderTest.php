<?php
namespace Avris\Micrus\Model\User;

use Avris\Bag\Bag;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Model\User\MemoryUserProvider
 */
class MemoryUserProviderTest extends TestCase
{
    public function testSimple()
    {
        $provider = new MemoryUserProvider(new Bag([
            'user' => [
                'authenticators' => [
                    'password' => 'pswd',
                ],
            ],
        ]));

        $user = $provider->getUser('user');
        $this->assertInstanceOf(UserInterface::class, $user);
        $auths = $user->getAuthenticators('password');
        $this->assertCount(1, $auths);
        $this->assertInstanceOf(AuthenticatorInterface::class, $auths[0]);
        $this->assertEquals('pswd', $auths[0]->getPayload());

        $this->assertNull($provider->getUser('foobar'));
    }
}