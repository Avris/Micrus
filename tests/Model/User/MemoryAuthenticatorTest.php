<?php
namespace Avris\Micrus\Model\User;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Model\User\MemoryAuhtenticator
 */
class MemoryAuthenticatorTest extends TestCase
{
    public function testSimple()
    {
        $auth = new MemoryAuhtenticator('pass', 'abc');
        $this->assertSame('pass', $auth->getType());
        $this->assertSame('abc', $auth->getPayload());
        $this->assertTrue($auth->isValid());

        $auth->invalidate();
        $this->assertFalse($auth->isValid());
    }
}