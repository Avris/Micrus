<?php
namespace Avris\Micrus\Controller;

use Avris\Micrus\Test\TestModule;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Bootstrap\ModuleTrait
 */
class ModuleTraitTest extends TestCase
{
    public function testBasic()
    {
        $module = new TestModule();

        $this->assertEquals('Avris\Micrus\Test', $module->getName());
        $this->assertEquals(dirname(__DIR__), $module->getDir());
    }
}
