<?php
namespace Avris\Micrus\Bootstrap;

use Avris\Container\Container;
use Avris\Dispatcher\Event;
use Avris\Micrus\Console\ConsoleApplication;
use Avris\Micrus\Console\ConsoleWarmupEvent;
use Avris\Micrus\Controller\ControllerEvent;
use Avris\Http\Request\Request;
use Avris\Micrus\Controller\RequestEvent;
use Avris\Http\Response\Response;
use Avris\Micrus\Controller\ResponseEvent;
use Avris\Micrus\Controller\Routing\Event\AddRouteEvent;
use Avris\Micrus\Controller\Routing\Event\GenerateRouteEvent;
use Avris\Micrus\Controller\Routing\Model\CompiledRoute;
use Avris\Micrus\Controller\Routing\Model\Route;
use Avris\Micrus\Controller\ViewEvent;
use Avris\Micrus\Exception\Handler\ErrorEvent;
use Avris\Micrus\Exception\Http\NotFoundHttpException;
use Avris\Micrus\Tool\Cache\CacheClearEvent;
use Avris\Micrus\Tool\Cache\CacheWarmupEvent;
use PHPUnit\Framework\TestCase;

class EventsTest extends TestCase
{
    /**
     * @covers \Avris\Micrus\Bootstrap\TerminateEvent
     */
    public function testTerminateEvent()
    {
        $container = new Container();
        $event = new TerminateEvent($container);

        $this->doTestEvent($event, 'terminate', null, null);
        $this->assertSame($container, $event->getContainer());
    }

    /**
     * @covers \Avris\Micrus\Bootstrap\WarmupEvent
     */
    public function testWarmupEvent()
    {
        $container = new Container();
        $event = new WarmupEvent($container);

        $this->doTestEvent($event, 'warmup', null, null);
        $this->assertSame($container, $event->getContainer());
    }

    /**
     * @covers \Avris\Micrus\Console\ConsoleWarmupEvent
     */
    public function testConsoleWarmupEvent()
    {
        $app = $this->getMockBuilder(ConsoleApplication::class)->disableOriginalConstructor()->getMock();
        $event = new ConsoleWarmupEvent($app);

        $this->doTestEvent($event, 'consoleWarmup', null, null);
        $this->assertSame($app, $event->getApp());
    }

    /**
     * @covers \Avris\Micrus\Controller\ControllerEvent
     */
    public function testControllerEvent()
    {
        $request = Request::create('GET', '/');
        $target = 'Foo/bar';
        $controller = function () {};
        $arguments = ['foo' => 'bar'];

        $event = new ControllerEvent($request, $target, $controller, $arguments);

        $this->assertSame($request, $event->getRequest());
        $this->assertSame($target, $event->getTarget());
        $this->assertSame($controller, $event->getController());
        $this->assertSame($arguments, $event->getArguments());
        $this->assertSame(null, $event->getResponse());

        $this->doTestEvent(
            $event,
            'controller',
            [null, $controller, $arguments],
            [new Response('a'), function ($a) {}, ['abc' => 'xyz']]
        );

        $this->doTestSetter($event, 'Controller', function ($b) {});
        $this->doTestSetter($event, 'Arguments', []);
        $this->doTestSetter($event, 'Response', new Response('osiem'));
    }

    /**
     * @covers \Avris\Micrus\Controller\ViewEvent
     */
    public function testViewEvent()
    {
        $event = new ViewEvent();

        $this->assertSame(null, $event->getResponse());

        $this->doTestEvent($event, 'view', null, new Response('ok'));

        $this->doTestSetter($event, 'Response', new Response('osiem'));
    }

    /**
     * @covers \Avris\Micrus\Controller\RequestEvent
     */
    public function testRequestEvent()
    {
        $container = new Container();
        $request = Request::create('GET', '/');
        $event = new RequestEvent($container, $request);

        $this->assertSame($container, $event->getContainer());
        $this->assertSame($request, $event->getRequest());
        $this->assertSame(null, $event->getResponse());

        $this->doTestEvent($event, 'request', null, new Response('ok'));

        $this->doTestSetter($event, 'Response', new Response('osiem'));
    }

    /**
     * @covers \Avris\Micrus\Controller\ResponseEvent
     */
    public function testResponseEvent()
    {
        $response = new Response('aaa');
        $event = new ResponseEvent($response);

        $this->assertSame($response, $event->getResponse());

        $this->doTestEvent($event, 'response', $response, new Response('ok'));

        $this->doTestSetter($event, 'Response', new Response('osiem'));
    }

    /**
     * @covers \Avris\Micrus\Exception\Handler\ErrorEvent
     */
    public function testErrorEvent()
    {
        $exception = new NotFoundHttpException();
        $event = new ErrorEvent($exception);

        $this->assertSame($exception, $event->getException());
        $this->assertSame(null, $event->getResponse());

        $this->doTestEvent($event, 'error', null, new Response('ok'));

        $this->doTestSetter($event, 'Response', new Response('osiem'));
    }

    /**
     * @covers \Avris\Micrus\Tool\Cache\CacheClearEvent
     */
    public function testCacheClearEvent()
    {
        $this->doTestEvent(new CacheClearEvent(), 'cacheClear', null, null);
    }

    /**
     * @covers \Avris\Micrus\Tool\Cache\CacheWarmupEvent
     */
    public function testCacheWarmupEvent()
    {
        $event = new CacheWarmupEvent(false);
        $this->doTestEvent($event, 'cacheWarmup', null, null);
        $this->assertFalse($event->includeOptional());

        $event = new CacheWarmupEvent(true);
        $this->doTestEvent($event, 'cacheWarmup', null, null);
        $this->assertTrue($event->includeOptional());
    }

    private function doTestEvent(Event $event, string $name, $value1, $value2)
    {
        $this->assertSame($name, $event->getName());
        $this->assertFalse($event->isPropagationStopped());
        $this->assertSame($value1, $event->getValue());

        $this->assertSame($event, $event->setValue($value2));
        $this->assertSame($value2, $event->getValue());

        $event->stopPropagation();
        $this->assertTrue($event->isPropagationStopped());

        return $event;
    }

    private function doTestSetter(Event $event, string $field, $value)
    {
        $this->assertSame($event, call_user_func([$event, 'set' . $field], $value));
        $this->assertSame($value, call_user_func([$event, 'get' . $field]));
    }
}
