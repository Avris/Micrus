<?php
namespace Avris\Micrus\Controller;

use Avris\Container\Container;
use Avris\Micrus\Bootstrap\TerminateEvent;
use Avris\Micrus\Bootstrap\Terminator;
use Avris\Micrus\Controller\ResponseEvent;
use Avris\Http\Response\ResponseInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Bootstrap\Terminator
 */
class TerminatorTest extends TestCase
{
    public static $output = '';

    public function testBasic()
    {
        $terminator = new Terminator();

        $response = $this->getMockBuilder(ResponseInterface::class)->disableOriginalConstructor()->getMock();
        $response->expects($this->once())->method('continueProcessing');

        $container = new Container();
        $container->set('foo', 'bar');

        $terminator->attach(function () {
            TerminatorTest::$output .= 'one';
        });

        $terminator->attach(function (TerminateEvent $event) {
            TerminatorTest::$output .= $event->getContainer()->get('foo');
        });

        $terminator->onResponse(new ResponseEvent($response));
        $terminator->onTerminate(new TerminateEvent($container));

        $this->assertEquals('onebar', self::$output);
    }
}
