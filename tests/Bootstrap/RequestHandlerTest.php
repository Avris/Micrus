<?php
namespace Avris\Micrus\Controller;

use Avris\Container\Container;
use Avris\Container\ContainerInterface;
use Avris\Dispatcher\EventDispatcher;
use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Micrus\Bootstrap\RequestHandler;
use Avris\Micrus\Bootstrap\RequestHandlerInterface;
use Avris\Http\Response\JsonResponse;
use Avris\Http\Request\Request;
use Avris\Micrus\Controller\RequestEvent;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Http\Response\Response;
use Avris\Micrus\Controller\ResponseEvent;
use Avris\Http\Response\ResponseInterface;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Test\TestController;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * @covers \Avris\Micrus\Bootstrap\RequestHandler
 */
class RequestHandlerTest extends TestCase
{
    /** @var ContainerInterface */
    private $container;

    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @var LoggerInterface|MockObject */
    private $logger;

    /** @var RequestHandlerInterface */
    private $handler;

    /** @var RequestInterface */
    private $request;

    /** @var RequestProviderInterface */
    private $requestProvider;

    /** @var RouteMatch|MockObject */
    private $routeMatch;

    protected function setUp()
    {
        $this->routeMatch = $this->getMockBuilder(RouteMatch::class)->disableOriginalConstructor()->getMock();
        $this->routeMatch->expects($this->any())->method('getTarget')->willReturn('Avris\Micrus\Test\TestController/handler');
        $this->routeMatch->expects($this->any())->method('getTags')->willReturn(['id' => '123']);
        $this->routeMatch->expects($this->any())->method('getQuery')->willReturn(['_locale' => 'nl']);

        $this->requestProvider = $this->getMockBuilder(RequestProviderInterface::class)->getMock();

        $this->container = new Container();

        $this->dispatcher = new EventDispatcher();

        $this->logger = $this->getMockBuilder(LoggerInterface::class)->getMock();

        $this->handler = new RequestHandler($this->container, $this->dispatcher, $this->logger, $this->requestProvider);

        $this->request = Request::create('GET', '/posts/123', [], ['page' => '1']);
    }

    /**
     * @expectedException \Avris\Micrus\Exception\Http\NotFoundHttpException
     * @expectedExceptionMessage Route for "GET /posts/123" not found
     */
    public function testNoRouteMatch()
    {
        $this->logger->expects($this->once())->method('info')
            ->with('Request: GET /posts/123');

        $this->handler->handle($this->request);
    }

    public function testRequestEvent()
    {
        $this->logger->expects($this->exactly(2))->method('info')
            ->withConsecutive(['Request: GET /posts/123'], ['Response status 204']);

        $this->dispatcher->attachListener('request', function (RequestEvent $event) {
            return new JsonResponse($event->getRequest()->getQuery(), 204);
        });

        $this->requestProvider->expects($this->any())->method('getRouteMatch')->willReturn($this->routeMatch);

        $this->assertCorrectResponseOne($this->handler->handle($this->request));
    }

    public function testControllerEvent()
    {
        $this->expectAllLogs();
        $this->setUpResolvers('handlerAction');

        $this->dispatcher->attachListener('controller', function (ControllerEvent $event) {
            $event->setResponse(new JsonResponse($event->getRequest()->getQuery(), 204));
        });

        $this->requestProvider->expects($this->any())->method('getRouteMatch')->willReturn($this->routeMatch);

        $this->assertCorrectResponseOne($this->handler->handle($this->request));
    }

    public function testController()
    {
        $this->expectAllLogs();
        $this->setUpResolvers('handlerAction');

        $this->requestProvider->expects($this->any())->method('getRouteMatch')->willReturn($this->routeMatch);

        $this->assertCorrectResponseTwo($this->handler->handle($this->request));
    }

    public function testViewEvent()
    {
        $this->expectAllLogs();
        $this->setUpResolvers('handlerViewAction');

        $this->dispatcher->attachListener('view', function (ViewEvent $event) {
            return new Response($event->getValue(), 204);
        });

        $this->requestProvider->expects($this->any())->method('getRouteMatch')->willReturn($this->routeMatch);

        $this->assertCorrectResponseTwo($this->handler->handle($this->request));
    }

    /**
     * @expectedException \Avris\Micrus\Exception\InvalidArgumentException
     * @expectedExceptionMessage Controller must return an instance of ResponseInterface
     */
    public function testNoResponse()
    {
        $this->logger->expects($this->once())->method('info')
            ->with('Request: GET /posts/123');
        $this->logger->expects($this->once())->method('debug')
            ->with('Calling controller Avris\Micrus\Test\TestController/handler with params [123]');

        $this->setUpResolvers('handlerViewAction');

        $this->requestProvider->expects($this->any())->method('getRouteMatch')->willReturn($this->routeMatch);

        $this->handler->handle($this->request);
    }

    public function testResponseEvent()
    {
        $this->logger->expects($this->exactly(2))->method('info')
            ->withConsecutive(
                ['Request: GET /posts/123'],
                ['Response status 204']
            );

        $this->dispatcher->attachListener('request', function (RequestEvent $event) {
            return new Response('foo123');
        });

        $this->dispatcher->attachListener('response', function (ResponseEvent $event) {
            return new Response(substr($event->getResponse(), 3), 204);
        });

        $this->requestProvider->expects($this->any())->method('getRouteMatch')->willReturn($this->routeMatch);

        $this->assertCorrectResponseTwo($this->handler->handle($this->request));
    }

    private function expectAllLogs()
    {
        $this->logger->expects($this->exactly(2))->method('info')
            ->withConsecutive(
                ['Request: GET /posts/123'],
                ['Response status 204']
            );

        $this->logger->expects($this->once())->method('debug')
            ->with('Calling controller Avris\Micrus\Test\TestController/handler with params [123]');
    }

    private function setUpResolvers($action)
    {
        $controller = [new TestController($this->container), $action];

        $controllerResolver = $this->getMockBuilder(ControllerResolverInterface::class)->getMock();
        $controllerResolver->expects($this->once())->method('resolveController')
            ->with($this->routeMatch)->willReturn($controller);

        $argumentsResolver = $this->getMockBuilder(ArgumentsResolverInterface::class)->getMock();
        $argumentsResolver->expects($this->once())->method('resolveArguments')
            ->with($controller, $this->routeMatch)->willReturn([123]);

        $this->container->set(ControllerResolverInterface::class, $controllerResolver);
        $this->container->set(ArgumentsResolverInterface::class, $argumentsResolver);
    }

    private function assertCorrectResponseOne(ResponseInterface $response)
    {
        $this->assertEquals(
            new JsonResponse(['page' => '1', '_locale' => 'nl'], 204),
            $response
        );
    }

    private function assertCorrectResponseTwo(ResponseInterface $response)
    {
        $this->assertEquals(
            new Response(123, 204),
            $response
        );
    }
}
