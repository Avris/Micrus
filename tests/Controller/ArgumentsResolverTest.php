<?php
namespace Avris\Micrus\Controller;

use App\Entity\Testie;
use Avris\Container\Container;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Test\TestController;
use Avris\Micrus\Test\TestMatchProvider;
use Avris\Micrus\Test\TestService;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Controller\ArgumentsResolver
 */
class ArgumentsResolverTest extends TestCase
{
    /** @var ArgumentsResolver */
    private $resolver;

    protected function setUp()
    {
        $this->resolver = new ArgumentsResolver([
            new TestMatchProvider(),
        ]);
    }

    /**
     * @dataProvider resolveArgumentsProvider
     */
    public function testResolveArguments(callable $controller, array $tags, $expectedArguments)
    {
        $routeMatch = $this->getMockBuilder(RouteMatch::class)->disableOriginalConstructor()->getMock();
        $routeMatch->expects($this->any())->method('getTags')->willReturn($tags);

        if ($expectedArguments instanceof \Throwable) {
            $this->expectException(get_class($expectedArguments));
            $this->expectExceptionMessage($expectedArguments->getMessage());
        }

        $arguments = $this->resolver->resolveArguments($controller, $routeMatch);

        $this->assertEquals($expectedArguments, $arguments);
    }

    public function resolveArgumentsProvider()
    {
        $testController = new TestController(new Container());
        $service = new TestService();
        $testie = new Testie('123');

        yield [
            [$testController, 'testMatcher1Action'],
            [],
            [],
        ];

        yield [
            [$testController, 'testMatcher1Action'],
            ['foo' => 'bar'],
            [],
        ];

        yield [
            [$testController, 'testMatcher2Action'],
            [],
            [
                '$service' => $service,
                '$testie' => null,
            ],
        ];

        yield [
            [$testController, 'testMatcher2Action'],
            ['id' => 123],
            [
                '$service' => $service,
                '$testie' => $testie,
            ],
        ];

        yield [
            [$testController, 'testMatcher3Action'],
            [],
            new \RuntimeException('Argument Avris\Container\ContainerInterface $container is expected for a controller, but there\'s not enough route tags and no MatchProvider supports it'),
        ];

        yield [
            [$testController, 'testMatcher4Action'],
            [],
            new \RuntimeException('Cannot dequeue an empty queue'),
        ];

        yield [
            [$testController, 'testMatcher4Action'],
            ['id' => 123],
            [
                '$service' => $service,
                '$testie' => $testie,
            ],
        ];

        yield [
            'Avris\Micrus\Test\TestController::testMatcherStaticAction',
            ['id' => 123, 'foo' => 'abc'],
            [
                '$testie' => $testie,
                '$foo' => 'abc',
            ],
        ];

        yield [
            [TestController::class, 'testMatcherStaticAction'],
            ['id' => 123],
            new \RuntimeException('Argument  $foo is expected for a controller, but there\'s not enough route tags and no MatchProvider supports it'),
        ];

        yield [
            $testController,
            ['id' => 123],
            [
                '$testie' => $testie,
            ],
        ];

        yield [
            'Avris\Micrus\Test\functionAction',
            ['id' => 123],
            [
                '$service' => $service,
                '$testie' => $testie,
                '$foo' => 'bar',
            ],
        ];

        yield [
            'Avris\Micrus\Test\functionAction',
            ['id' => 123, 'foo' => 'abc'],
            [
                '$service' => $service,
                '$testie' => $testie,
                '$foo' => 'abc',
            ],
        ];

        yield [
            function (TestService $service) {},
            [],
            [
                '$service' => $service,
            ],
        ];
    }
}
