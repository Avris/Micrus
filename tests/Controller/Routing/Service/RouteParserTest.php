<?php
namespace Avris\Micrus\Controller\Routing\Service;

use Avris\Bag\Bag;
use Avris\Bag\Set;
use Avris\Micrus\Controller\Routing\Model\Route;
use Avris\Micrus\Exception\ConfigException;
use Avris\Micrus\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Controller\Routing\Service\RouteParser
 */
class RouteParserTest extends TestCase
{
    /** @var RouteParser */
    protected $parser;

    protected function setUp()
    {
        $this->parser = new RouteParser();
    }

    /**
     * @dataProvider parseProvider
     */
    public function testParse($string, $expected)
    {
        if ($expected instanceof \Exception) {
            try {
                $this->parser->parse('name', $string);
                $this->fail('Exception expected');
            } catch (\Exception $e) {
                $this->assertEquals($expected, $e);
            }
            return;
        }

        $this->assertEquals(
            $expected,
            $this->parser->parse('name', $string)
        );
    }

    public function parseProvider()
    {
        return [
            [
                '',
                new InvalidArgumentException('Cannot parse "" as route string'),
            ],
            [
                '/',
                new InvalidArgumentException('Cannot parse "/" as route string'),
            ],
            [
                '/ -> controller/action',
                new Route('name', '/', 'controller/action'),
            ],
            [
                '/ -> @service/action',
                new Route('name', '/', '@service/action'),
            ],
            [
                '/foo/{int:bar=5} -> controller/action',
                new Route('name', '/foo/{bar}', 'controller/action', ['bar' => '5'], ['bar' => '\d*']),
            ],
            [
                '/foo/{<int>:bar=5} -> controller/action',
                new Route('name', '/foo/{bar}', 'controller/action', ['bar' => '5'], ['bar' => 'int']),
            ],
            [
                '/foo/{bar=<te{st>} -> controller/action',
                new Route('name', '/foo/{bar}', 'controller/action', ['bar' => 'te{st']),
            ],
            [
                '/{} -> controller/action',
                new ConfigException('Invalid route tag name ""'),
            ],
        ];
    }
}
