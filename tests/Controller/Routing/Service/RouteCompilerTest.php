<?php
namespace Avris\Micrus\Controller\Routing\Service;

use Avris\Bag\Bag;
use Avris\Bag\Set;
use Avris\Micrus\Controller\Routing\Model\CompiledRoute;
use Avris\Micrus\Controller\Routing\Model\Route;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Controller\Routing\Service\RouteCompiler
 */
class RouteCompilerTest extends TestCase
{
    /** @var RouteCompiler */
    protected $compiler;

    protected function setUp()
    {
        $this->compiler = new RouteCompiler();
    }

    /**
     * @dataProvider compileProvider
     */
    public function testCompile($pattern, $defaults, $requirements, $regex, $tagNames)
    {
        $route = new Route('name', $pattern, 'controller/action', $defaults, $requirements);

        $compiledRoute = $this->compiler->compile($route);

        $this->assertInstanceOf(CompiledRoute::class, $compiledRoute);
        $regexReflection = (new \ReflectionObject($compiledRoute))->getProperty('regex');
        $regexReflection->setAccessible(true);
        $this->assertSame($regex, $regexReflection->getValue($compiledRoute));
        $this->assertSame($tagNames, $compiledRoute->getTagNames());
    }

    public function compileProvider()
    {
        return [
            [
                '/',
                [],
                [],
                '/^\/$/Uui',
                [],
            ],
            [
                '/test',
                [],
                [],
                '/^\/test$/Uui',
                [],
            ],
            [
                '/test/{foo}',
                [],
                [],
                '/^\/test(?:\/([^\/\.]+))$/Uui',
                ['foo'],
            ],
            [
                '/test/{foo}',
                ['foo' => '5'],
                [],
                '/^\/test(?:\/([^\/\.]*))?$/Uui',
                ['foo'],
            ],
            [
                '/test/{foo}',
                [],
                ['foo' => '\d+'],
                '/^\/test(?:\/(\d+))$/Uui',
                ['foo'],
            ],
            [
                '/{foo}/{bar}',
                ['foo' => '5'],
                ['foo' => '\d+', 'bar' => 'a+'],
                '/^(?:\/(\d+))?(?:\/(a+))$/Uui',
                ['foo', 'bar'],
            ],
        ];
    }
}
