<?php
namespace Avris\Micrus\Controller\Routing\Service;

use Avris\Bag\Bag;
use Avris\Dispatcher\Event;
use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\Routing\Event\AddRouteEvent;
use Avris\Micrus\Controller\Routing\Event\GenerateRouteEvent;
use Avris\Micrus\Controller\Routing\Model\CompiledRoute;
use Avris\Micrus\Controller\Routing\Model\Route;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Test\TestRouterExtension;
use Avris\Micrus\Tool\Cache\Cacher;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * @covers \Avris\Micrus\Controller\Routing\Service\Router
 */
class RouterTest extends TestCase
{
    private function buildRouter(array $routes = [], array $extensions = [])
    {
        $request = $this->getMockBuilder(RequestInterface::class)->disableOriginalConstructor()->getMock();
        $request->expects($this->once())->method('getBase')->willReturn('/base');
        $request->expects($this->once())->method('getAbsoluteBase')->willReturn('http://project.dev');

        $cacher = $this->getMockBuilder(Cacher::class)->disableOriginalConstructor()->getMock();
        $cacher->expects($this->once())->method('cache')->willReturnCallback(function ($key, callable $generator) {
            return $generator();
        });

        $logger = $this->getMockBuilder(LoggerInterface::class)->disableOriginalConstructor()->getMock();

        return new Router(
            $request,
            $cacher,
            $logger,
            $extensions,
            new Bag($routes),
            new RouteParser(),
            new RouteCompiler(),
            new RouteGenerator()
        );
    }

    public function testConstructEmpty()
    {
        $router = $this->buildRouter();

        $routes = $router->getRoutes();
        $this->assertCount(0, $routes);
        $this->assertSame('home', $router->getDefaultRoute());
    }

    public function testConstructFull()
    {
        $router = $this->buildRouter([
            'route1' => '/test -> c/a1',
            'route2' => ['pattern' => '/{foo}', 'target' => 'c/a2'],
            'route3' => new Route('route3', '/r3', 'c/a3'),
            'route4' => new CompiledRoute(new Route('route4', '/r/4', 'c/a4'), '', []),
        ], [new TestRouterExtension]);


        $routes = $router->getRoutes();
        $this->assertCount(4, $routes);

        $this->assertInstanceOf(CompiledRoute::class, $routes['added_route1']);
        $this->assertSame('/test -> c/a1', (string) $routes['added_route1']);

        $this->assertInstanceOf(CompiledRoute::class, $routes['added_route2']);
        $this->assertSame('/{foo} -> c/a2', (string) $routes['added_route2']);

        $this->assertInstanceOf(CompiledRoute::class, $routes['added_route3']);
        $this->assertSame('/r3 -> c/a3', (string) $routes['added_route3']);

        $this->assertInstanceOf(CompiledRoute::class, $routes['route4']);
        $this->assertSame('/r/4 -> c/a4', (string) $routes['route4']);
    }

    public function testFindRouteMatch()
    {
        $request = $this->getMockBuilder(RequestInterface::class)->disableOriginalConstructor()->getMock();

        $match = $this->getMockBuilder(RouteMatch::class)->disableOriginalConstructor()->getMock();

        $route1 = $this->getMockBuilder(CompiledRoute::class)->disableOriginalConstructor()->getMock();
        $route1->expects($this->once())->method('matches')->with($request)->willReturn(null);

        $route2 = $this->getMockBuilder(CompiledRoute::class)->disableOriginalConstructor()->getMock();
        $route2->expects($this->once())->method('matches')->with($request)->willReturn($match);

        $route3 = $this->getMockBuilder(CompiledRoute::class)->disableOriginalConstructor()->getMock();
        $route3->expects($this->never())->method('matches');

        $router = $this->buildRouter([$route1, $route2, $route3]);
        $foundMatch = $router->findRouteMatch($request);

        $this->assertSame($match, $foundMatch);
    }

    public function testFindRouteMatchNotFound()
    {
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $route1 = $this->getMockBuilder(CompiledRoute::class)->disableOriginalConstructor()->getMock();
        $route1->expects($this->once())->method('matches')->with($request)->willReturn(null);

        $route2 = $this->getMockBuilder(CompiledRoute::class)->disableOriginalConstructor()->getMock();
        $route2->expects($this->once())->method('matches')->with($request)->willReturn(null);

        $route3 = $this->getMockBuilder(CompiledRoute::class)->disableOriginalConstructor()->getMock();
        $route3->expects($this->once())->method('matches')->with($request)->willReturn(null);


        $router = $this->buildRouter([$route1, $route2, $route3]);

        $foundMatch = $router->findRouteMatch($request);

        $this->assertNull($foundMatch);
    }

    public function testGenerateSingle()
    {
        $router = $this->buildRouter(['name' => '/test -> c/a']);

        $this->assertSame('/base/test', $router->generate('name'));
    }

    /**
     * @expectedException \Avris\Micrus\Exception\NotFoundException
     * @expectedExceptionMessage Route named "nope" not found
     */
    public function testGenerateSinlgeNotFound()
    {
        $router = $this->buildRouter(['name' => '/test -> c/a']);

        $router->generate('nope');
    }

    public function testGenerateSingleAbsolute()
    {
        $router = $this->buildRouter(['name' => '/test -> c/a']);

        $this->assertSame(
            'http://project.dev/base/app_test.php/test',
            $router->generate('name', [], true, '/app_test.php')
        );
    }

    public function testGenerateSingleExtensionGenerated()
    {
        $router = $this->buildRouter(
            ['generateTest' => '/test -> c/a'],
            [new TestRouterExtension]
        );

        $this->assertSame(
            'http://project.dev/base/app_test.php/generated',
            $router->generate('generateTest', [], true, '/app_test.php')
        );
    }

    public function testGenerateSingleExtensionExtended()
    {
        $router = $this->buildRouter(
            ['name' => '/test -> c/a'],
            [new TestRouterExtension]
        );

        $this->assertSame(
            'http://project.dev/base/app_test.php/test?foo=bar',
            $router->generate('added_name', [], true, '/app_test.php')
        );
    }

    public function testGenerateMultiple()
    {
        $router = $this->buildRouter(['name' => '/test -> c/a']);

        $this->assertSame('/base/test', $router->generate(['nope', 'name']));
    }

    /**
     * @expectedException \Avris\Micrus\Exception\NotFoundException
     * @expectedExceptionMessage None of the routes (notfound, nope) found
     */
    public function testGenerateMultipleNotFound()
    {
        $router = $this->buildRouter(['name' => '/test -> c/a']);

        $router->generate(['notfound', 'nope']);
    }

    public function testPrependToUrl()
    {
        $router = $this->buildRouter();

        $this->assertSame(
            '/base/ok',
            $router->prependToUrl('/ok')
        );

        $this->assertSame(
            'http://project.dev/base/ok',
            $router->prependToUrl('/ok', true)
        );

        $this->assertSame(
            '/base/app.php/ok',
            $router->prependToUrl('/ok', false, '/app.php')
        );
    }

    public function testRouteExist()
    {
        $router = $this->buildRouter(['foo' => '/test -> c/a']);

        $this->assertTrue($router->routeExists('foo'));
        $this->assertTrue($router->routeExists(['nope', 'foo']));
        $this->assertFalse($router->routeExists('nope'));
        $this->assertFalse($router->routeExists(['nope', 'nah']));
    }
}
