<?php
namespace Avris\Micrus\Controller\Routing\Service;

use Avris\Bag\Bag;
use Avris\Bag\Set;
use Avris\Micrus\Controller\Routing\Model\Route;
use Avris\Micrus\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Controller\Routing\Service\RouteGenerator
 */
class RouteGeneratorTest extends TestCase
{
    /** @var RouteGenerator */
    protected $generator;

    protected function setUp()
    {
        $this->generator = new RouteGenerator();
    }

    /**
     * @dataProvider generateProvider
     */
    public function testGenerate(Route $route, $params, $expected)
    {
        if ($expected instanceof \Exception) {
            try {
                $this->generator->generate($route, $params);
                $this->fail('Exception expected');
            } catch (\Exception $e) {
                $this->assertEquals($expected, $e);
            }
            return;
        }

        $this->assertEquals(
            $expected,
            $this->generator->generate($route, $params)
        );
    }

    public function generateProvider()
    {
        return [
            [
                new Route('name', '/test', 'controller/action'),
                [],
                '/test',
            ],
            [
                new Route('name', '/test', 'controller/action'),
                ['foo' => 'bar', 'lorem' => 'ipsum'],
                '/test?foo=bar&lorem=ipsum',
            ],
            [
                new Route('name', '/test/{foo}', 'controller/action'),
                ['foo' => 'bar'],
                '/test/bar',
            ],
            [
                new Route('name', '/test/{foo}', 'controller/action', ['foo' => 'bar']),
                [],
                '/test',
            ],
            [
                new Route('name', '/test/{foo}', 'controller/action', ['foo' => 'bar']),
                ['foo' => ''],
                '/test',
            ],
            [
                new Route('name', '/test/{foo}', 'controller/action', ['foo' => 'bar']),
                ['foo' => 'bar'],
                '/test',
            ],
            [
                new Route('name', '/test/{foo}', 'controller/action', ['foo' => 'bar']),
                ['foo' => 'BAZ'],
                '/test/BAZ',
            ],
            [
                new Route('name', '/test/{foo}.jpg', 'controller/action', ['foo' => 'bar']),
                [],
                '/test.jpg',
            ],
            [
                (new Route('name', '/test/{foo}.jpg', 'controller/action', ['foo' => 'bar']))->setOption('oneWayDefault', 'foo'),
                [],
                '/test/bar.jpg',
            ],
            [
                (new Route('name', '/test/{foo}.jpg', 'controller/action', ['foo' => 'bar']))->setOption('oneWayDefault', 'foo'),
                ['foo' => ''],
                '/test/bar.jpg',
            ],
            [
                new Route('name', '/test/{foo}', 'controller/action'),
                [],
                new InvalidArgumentException('Parameter "foo" is required for route "name"'),
            ],
            [
                new Route('name', '/test/{foo}.jpg', 'controller/action', [], ['foo' => '\d+']),
                ['foo' => 15],
                '/test/15.jpg',
            ],
            [
                new Route('name', '/test/{foo}.jpg', 'controller/action', [], ['foo' => '\d+']),
                ['foo' => 'aaa'],
                new InvalidArgumentException('Value "aaa" invalid for tag "foo" in route "name" (required "\d+")'),
            ],
        ];
    }
}
