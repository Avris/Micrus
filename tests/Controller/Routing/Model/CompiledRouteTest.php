<?php
namespace Avris\Micrus\Controller\Routing\Model;

use Avris\Http\Request\RequestInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Controller\Routing\Model\CompiledRoute
 */
class CompiledRouteTest extends TestCase
{
    public function testEmpty()
    {
        $route = new CompiledRoute(
            new Route('name', '/login/{method}', 'controller/action'),
            '/^\/login(?:\/([^\/\.]+))$/Uui',
            ['method']
        );

        $this->assertSame('name', $route->getName());
        $this->assertSame('/login/{method}', $route->getPattern());
        $this->assertSame('controller/action', $route->getTarget());
        $this->assertSame(['method'], $route->getTagNames());

        $this->assertDoesntMatchRequest($route, $this->buildRequestMock('/test'));
        $this->assertDoesntMatchRequest($route, $this->buildRequestMock('/login'));
        $this->assertMatchesRequest($route, $this->buildRequestMock('/login/facebook'), ['method' => 'facebook']);
    }

    public function testFull()
    {
        $route = new CompiledRoute(
            new Route(
                'name',
                '/login/{method}',
                'controller/action',
                [],
                [],
                ['POST','DELETE'],
                '\w+\.prod',
                ['https', 'ftp']
            ),
            '/^\/login(?:\/([^\/\.]+))$/Uui',
            ['method']
        );

        $this->assertSame('name', $route->getName());
        $this->assertSame('/login/{method}', $route->getPattern());
        $this->assertSame('controller/action', $route->getTarget());
        $this->assertSame(['method'], $route->getTagNames());

        $this->assertDoesntMatchRequest($route, $this->buildRequestMock('/login/facebook'));
        $this->assertDoesntMatchRequest($route, $this->buildRequestMock('/login/facebook', 'POST'));
        $this->assertDoesntMatchRequest($route, $this->buildRequestMock('/login/facebook', 'POST', 'https'));
        $this->assertMatchesRequest($route, $this->buildRequestMock('/login/facebook', 'POST', 'https', 'project.prod'), ['method' => 'facebook']);
        $this->assertMatchesRequest($route, $this->buildRequestMock('/login/facebook', 'DELETE', 'ftp', 'whatever.prod'), ['method' => 'facebook']);
        $this->assertDoesntMatchRequest($route, $this->buildRequestMock('/nope', 'POST', 'https', 'project.prod'));
    }

    /**
     * @param string $url
     * @param string $method
     * @param string $scheme
     * @param string $host
     * @return RequestInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected function buildRequestMock($url, $method = 'GET', $scheme = 'http', $host = 'project.test')
    {
        $request = $this->getMockBuilder(RequestInterface::class)->disableOriginalConstructor()->getMock();
        $request->expects($this->any())->method('getMethod')->willReturn($method);
        $request->expects($this->any())->method('getScheme')->willReturn($scheme);
        $request->expects($this->any())->method('getHost')->willReturn($host);
        $request->expects($this->any())->method('getCleanUrl')->willReturn($url);

        return $request;
    }

    protected function assertMatchesRequest(CompiledRoute $route, RequestInterface $request, array $tags = [])
    {
        $match = $route->matches($request);

        $this->assertInstanceOf(RouteMatch::class, $match);
        $this->assertSame($route, $match->getRoute());
        $this->assertSame($tags, $match->getTags());
    }

    protected function assertDoesntMatchRequest(CompiledRoute $route, RequestInterface $request)
    {
        $this->assertNull($route->matches($request));
    }
}
