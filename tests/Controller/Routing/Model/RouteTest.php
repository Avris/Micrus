<?php
namespace Avris\Micrus\Controller\Routing\Model;

use Avris\Bag\Bag;
use Avris\Bag\Set;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Controller\Routing\Model\Route
 */
class RouteTest extends TestCase
{
    public function testEmpty()
    {
        $route = new Route('name', 'test ', 'test/test');
        $this->assertSame('name', $route->getName());
        $this->assertSame('/test', $route->getPattern());
        $this->assertSame('test/test', $route->getTarget());

        $this->assertSame([], $route->getDefaults());
        $this->assertSame(null, $route->getDefault('any'));
        $this->assertFalse($route->hasDefault('any'));

        $this->assertSame([], $route->getRequirements());
        $this->assertSame(null, $route->getRequirement('any'));

        $this->assertEquals(new Set([], 'strtoupper'), $route->getMethods());
        $this->assertSame('', $route->getHost());
        $this->assertEquals(new Set([], 'strtolower'), $route->getSchemes());

        $this->assertSame([], $route->getOptions());
        $this->assertSame(null, $route->getOption('any'));

        $this->assertSame([
            'name' => 'name',
            'pattern' => '/test',
            'target' => 'test/test',
        ], $route->jsonSerialize());

        $this->assertSame(
            '/test -> test/test',
            (string) $route
        );
    }

    public function testFull()
    {
        $route = new Route(
            'name',
            '/test/{foo}.jpg',
            'foo/bar',
            ['foo' => '1'],
            ['foo' => '\d+'],
            ['get', 'POST'],
            'local.test',
            ['http', 'HTTPS'],
            [
                'immutable' => true,
                'defaults' => ['bar' => 2],
                'requirements' => ['bar' => '\d+'],
            ]
        );

        $this->assertSame('name', $route->getName());
        $this->assertSame('/test/{foo}.jpg', $route->getPattern());
        $this->assertSame('foo/bar', $route->getTarget());

        $this->assertSame(['foo' => '1', 'bar' => '2'], $route->getDefaults());
        $this->assertSame('1', $route->getDefault('foo'));
        $this->assertSame(null, $route->getDefault('nope'));
        $route->addDefault('nope', 'ok');
        $this->assertSame('ok', $route->getDefault('nope'));
        $this->assertTrue($route->hasDefault('nope'));

        $this->assertSame(['foo' => '\d+', 'bar' => '\d+'], $route->getRequirements());
        $this->assertSame('\d+', $route->getRequirement('foo'));
        $this->assertSame(null, $route->getRequirement('nope'));
        $route->addRequirement('nope', 'ok');
        $this->assertSame('ok', $route->getRequirement('nope'));
        $this->assertTrue($route->hasRequirement('nope'));

        $this->assertEquals(new Set(['GET', 'POST'], 'strtoupper'), $route->getMethods());
        $this->assertSame('local.test', $route->getHost());
        $this->assertEquals(new Set(['http', 'https'], 'strtolower'), $route->getSchemes());

        $this->assertSame(['immutable' => true], $route->getOptions());
        $this->assertSame(true, $route->getOption('immutable'));
        $this->assertSame(null, $route->getOption('nope'));
        $route->setOption('immutable', false);

        $this->assertEquals([
            'name' => 'name',
            'pattern' => '/test/{foo}.jpg',
            'target' => 'foo/bar',
            'defaults' => ['foo' => '1', 'bar' => '2', 'nope' => 'ok'],
            'requirements' => ['foo' => '\d+', 'bar' => '\d+', 'nope' => 'ok'],
            'methods' => new Set(['GET', 'POST'], 'strtoupper'),
            'host' => 'local.test',
            'schemes' => new Set(['http', 'https'], 'strtolower'),
            'options' => ['immutable' => false],
        ], $route->jsonSerialize());

        $this->assertSame(
            'GET,POST http,https://<local.test>/test/{int:foo=1}.jpg -> foo/bar {"immutable":false,"defaults":{"bar":"2","nope":"ok"},"requirements":{"bar":"\\\\d+","nope":"ok"}}',
            (string) $route
        );
    }

    public function testFromArray()
    {
        $route = Route::fromArray('test', [
            'pattern' => '/foo/{bar}',
            'target' => 'foo/bar',
            'requirements' => ['bar' => 'int'],
        ]);

        $this->assertSame('test', $route->getName());
        $this->assertSame('/foo/{bar}', $route->getPattern());
        $this->assertSame('foo/bar', $route->getTarget());

        $this->assertSame([], $route->getDefaults());
        $this->assertSame(['bar' => 'int'], $route->getRequirements());

        $this->assertEquals(new Set([], 'strtoupper'), $route->getMethods());
        $this->assertSame('', $route->getHost());
        $this->assertEquals(new Set([], 'strtolower'), $route->getSchemes());

        $this->assertSame([], $route->getOptions());
        $this->assertSame(null, $route->getOption('any'));

        $this->assertSame([
            'name' => 'test',
            'pattern' => '/foo/{bar}',
            'target' => 'foo/bar',
            'requirements' => ['bar' => 'int'],
        ], $route->jsonSerialize());

        $this->assertSame(
            '/foo/{<int>:bar} -> foo/bar',
            (string) $route
        );
    }

    public function testRequirementShortcuts()
    {
        $this->assertSame('test', Route::expandShortcut('test', true));
        $this->assertSame(null, Route::shortcut('xxx'));
        $this->assertSame(null, Route::shortcut('yyy'));
        $this->assertSame(null, Route::shortcut('zzz'));

        Route::addRequirementShortcut('test', 'xxx');
        $this->assertSame('xxx', Route::expandShortcut('test', true));
        $this->assertSame('xxx', Route::expandShortcut('test', false));
        $this->assertSame('test', Route::shortcut('xxx'));
        $this->assertSame(null, Route::shortcut('yyy'));
        $this->assertSame(null, Route::shortcut('zzz'));

        Route::addRequirementShortcut('test', ['yyy', 'zzz']);
        $this->assertSame('yyy', Route::expandShortcut('test', true));
        $this->assertSame('zzz', Route::expandShortcut('test', false));
        $this->assertSame(null, Route::shortcut('xxx'));
        $this->assertSame('test', Route::shortcut('yyy'));
        $this->assertSame('test', Route::shortcut('zzz'));
    }
}
