<?php
namespace Avris\Micrus\Controller\Routing\Model;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Controller\Routing\Model\RouteMatch
 */
class RouteMatchTest extends TestCase
{
    public function testAll()
    {
        $route = new Route(
            'name',
            '/pattern/{foo}/{bar}/{baz}',
            'controller/action',
            ['bar' => '6', 'baz' => '7', 'page' => '1']
        );

        $match = new RouteMatch(
            $route,
            ['/pattern/5/', '5', ''],
            ['bar' => '6', 'baz' => '7', 'page' => '1'],
            ['foo', 'bar', 'baz']
        );

        $this->assertSame($route, $match->getRoute());
        $this->assertSame('controller/action', $match->getTarget());
        $this->assertSame(['foo' => '5', 'bar' => '6', 'baz' => '7'], $match->getTags());
        $this->assertSame(['page' => '1'], $match->getQuery());
        $this->assertSame(
            '{"route":{"name":"name","pattern":"\/pattern\/{foo}\/{bar}\/{baz}","target":"controller\/action","defaults":{"bar":"6","baz":"7","page":"1"}},"tags":{"foo":"5","bar":"6","baz":"7"},"query":{"page":"1"}}',
            json_encode($match)
        );
    }
}
