<?php
namespace Avris\Micrus\Controller;

use Avris\Container\Container;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Test\TestController;
use Avris\Micrus\Test\TestControllerService;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Controller\ControllerResolver
 */
class ControllerResolverTest extends TestCase
{
    public function testResolveController()
    {
        $container = new Container();
        $container->set(TestController::class, new TestController($container));

        $resolver = new ControllerResolver($container);

        $routeMatch = $this->getMockBuilder(RouteMatch::class)->disableOriginalConstructor()->getMock();
        $routeMatch->expects($this->once())->method('getTarget')->willReturn('Avris\Micrus\Test\TestController/test');

        $callable = $resolver->resolveController($routeMatch);
        $this->assertTrue(is_callable($callable));
        $this->assertInstanceOf(TestController::class, $callable[0]);
        $this->assertEquals('testAction', $callable[1]);
    }
}
