<?php
namespace Avris\Micrus\Controller;

use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Controller\RequestProvider
 */
class RequestProviderTest extends TestCase
{
    public function testProvider()
    {
        $routeMatch1 = $this->getMockBuilder(RouteMatch::class)->disableOriginalConstructor()->getMock();
        $request1 = $this->getMockBuilder(RequestInterface::class)->getMock();

        $routeMatch2 = $this->getMockBuilder(RouteMatch::class)->disableOriginalConstructor()->getMock();
        $request2 = $this->getMockBuilder(RequestInterface::class)->getMock();

        $router = $this->getMockBuilder(RouterInterface::class)->getMock();
        $router->expects($this->exactly(2))->method('findRouteMatch')
            ->withConsecutive([$request1], [$request2])
            ->willReturnOnConsecutiveCalls($routeMatch1, $routeMatch2);

        $provider = new RequestProvider($router);
        $provider->reset($request1);
        $this->assertSame($request1, $provider->getRequest());
        $this->assertSame($routeMatch1, $provider->getRouteMatch());
        $this->assertSame($request1, $provider->getRequest());
        $this->assertSame($routeMatch1, $provider->getRouteMatch());

        $provider->reset($request2);
        $this->assertSame($request2, $provider->getRequest());
        $this->assertSame($routeMatch2, $provider->getRouteMatch());
        $this->assertSame($request2, $provider->getRequest());
        $this->assertSame($routeMatch2, $provider->getRouteMatch());
    }
}
