<?php
namespace Avris\Micrus\Controller;

use App\Entity\Testie;
use Avris\Container\Container;
use Avris\Container\Parameters\SimpleParameterProvider;
use Avris\Dispatcher\EventDispatcher;
use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Http\Cookie\Cookie;
use Avris\Http\Response\JsonResponse;
use Avris\Http\Response\RedirectResponse;
use Avris\Http\Request\Request;
use Avris\Http\Request\RequestInterface;
use Avris\Http\Response\Response;
use Avris\Http\Response\ResponseInterface;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Forms\Security\CsrfProviderInterface;
use Avris\Forms\WidgetFactory;
use Avris\Micrus\Model\User\MemoryUser;
use Avris\Micrus\Test\TestController;
use Avris\Micrus\Test\TestForm;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\GuardianInterface;
use Avris\Micrus\Tool\Security\RoleCheckerInterface;
use Avris\Micrus\Tool\Security\SecurityManagerInterface;
use Avris\Micrus\View\TemplaterInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Controller\Controller
 */
class ControllerTest extends TestCase
{
    /** @var TestController */
    private $controller;

    protected function setUp()
    {
        $dispatcher = new EventDispatcher();
        $dispatcher->attachListener('test', function () { return 'fooEvent'; });

        $container = new Container(new SimpleParameterProvider([AbstractApp::PROJECT_DIR => '/tmp']));

        $this->controller = new TestController($container);

        $container->set('env', 'testing');
        $container->set(EventDispatcherInterface::class, $dispatcher);

        $flashBag = $this->getMockBuilder(FlashBag::class)->disableOriginalConstructor()->getMock();
        $flashBag->expects($this->any())->method('add')->with('info', 'INFO', false)->willReturn($flashBag);
        $flashBag->expects($this->any())->method('all')->willReturn([['type' => 'info', 'message' => 'INFO']]);
        $container->set(FlashBag::class, $flashBag);

        $user = new MemoryUser('tester', []);

        $sm = $this->getMockBuilder(SecurityManagerInterface::class)->disableOriginalConstructor()->getMock();
        $sm->expects($this->any())->method('getUser')->willReturn($user);
        $container->set(SecurityManagerInterface::class, $sm);

        $roleChecker = $this->getMockBuilder(RoleCheckerInterface::class)->disableOriginalConstructor()->getMock();
        $roleChecker->expects($this->any())->method('isUserGranted')
            ->with('ROLE_TESTER', $user)->willReturn(true);
        $container->set(RoleCheckerInterface::class, $roleChecker);

        $guardian = $this->getMockBuilder(GuardianInterface::class)->disableOriginalConstructor()->getMock();
        $guardian->expects($this->any())->method('check')
            ->with($user, 'canTest', $this->controller)->willReturn(true);
        $container->set(GuardianInterface::class, $guardian);

        $router = $this->getMockBuilder(RouterInterface::class)->disableOriginalConstructor()->getMock();
        $router->expects($this->any())->method('generate')
            ->with('test', ['foo' => 'ok'])->willReturn('/test/ok');
        $container->set(RouterInterface::class, $router);

        $routeMatch = $this->getMockBuilder(RouteMatch::class)->disableOriginalConstructor()->getMock();
        $routeMatch->expects($this->any())->method('getTarget')
            ->willReturn('App\Controller\TestController');

        $requestProvider = $this->getMockBuilder(RequestProviderInterface::class)->getMock();
        $requestProvider->expects($this->any())->method('getRouteMatch')
            ->willReturn($routeMatch);
        $container->set(RequestProviderInterface::class, $requestProvider);

        $templater = $this->getMockBuilder(TemplaterInterface::class)->disableOriginalConstructor()->getMock();
        $templater->expects($this->any())->method('render')
            ->with('Test', ['foo' => 'bar'])->willReturnCallback(function ($template, $vars) { return serialize($vars); });
        $container->set(TemplaterInterface::class, $templater);

        $request = $this->getMockBuilder(RequestInterface::class)->disableOriginalConstructor()->getMock();
        $request->expects($this->any())->method('getBase')->willReturn('/base');
        $request->expects($this->any())->method('getHost')->willReturn('host.test');
        $container->set(RequestInterface::class, $request);

        $container->setDefinition(WidgetFactory::class, [
            'arguments' => ['$container' => $container],
            'public' => true,
        ]);

        $container->set(TestForm::class, new TestForm(
            $this->getMockBuilder(WidgetFactory::class)->disableOriginalConstructor()->getMock(),
            $this->getMockBuilder(CsrfProviderInterface::class)->disableOriginalConstructor()->getMock()
        ));
    }

    public function testHelpers()
    {
        $response = $this->controller->testAction();

        $this->assertEquals([
            'testing',
            'fooEvent',
            '/tmp',
            [['type' => 'info', 'message' => 'INFO']],
            'tester',
            true,
            true,
            '/test/ok',

        ], $response);
    }

    public function testRender()
    {
        $response = $this->controller->testRenderAction();

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(serialize(['foo' => 'bar']), $response->getContent());
        $this->assertExpectedCookies($response);
    }

    public function testForm()
    {
        $response = $this->controller->formAction(Request::create('GET', '/form'));

        $this->assertInstanceOf(TestForm::class, $response['form']);
        $this->assertEquals(new Testie, $response['testie']);
        $this->assertFalse($response['saved']);

        $response = $this->controller->formAction(Request::create('POST', '/form', [], [], ['id' => 5]));

        $this->assertInstanceOf(TestForm::class, $response['form']);
        $this->assertEquals(new Testie(5), $response['testie']);
        $this->assertFalse($response['saved']);

        $response = $this->controller->formAction(Request::create('POST', '/form', [], [], ['id' => 123, 'value' => 'ok']));

        $this->assertInstanceOf(TestForm::class, $response['form']);
        $this->assertEquals(new Testie(123, 'ok'), $response['testie']);
        $this->assertTrue($response['saved']);
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Service "Avris\Micrus\Controller\RequestProviderInterface" should be an instance of Avris\Forms\Widget\Widget to use it as a form widget
     */
    public function testFormInvalid()
    {
        $this->controller->formInvalidAction();
    }

    public function testRenderJson()
    {
        $response = $this->controller->testRenderJsonAction();

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(json_encode(['foo' => 'bar']), $response->getContent());
        $this->assertExpectedCookies($response);
    }

    public function testRedirectAction()
    {
        $response = $this->controller->testRedirectAction();

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertEquals('', $response->getContent());
        $this->assertEquals('https://avris.it', $response->getHeaders()->get('location'));
        $this->assertExpectedCookies($response);
    }

    public function testRedirectToRouteAction()
    {
        $response = $this->controller->testRedirectToRouteAction();

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertEquals('', $response->getContent());
        $this->assertEquals('/test/ok', $response->getHeaders()->get('location'));
        $this->assertExpectedCookies($response);
    }

    private function assertExpectedCookies(ResponseInterface $response)
    {
        $this->assertEquals(
            ['name' => new Cookie('name', 'value', 0, '/base', 'host.test')],
            $response->getCookies()->all()
        );
    }
}
