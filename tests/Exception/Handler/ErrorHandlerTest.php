<?php
namespace Avris\Micrus\Exception\Handler;

use Avris\Dispatcher\EventDispatcherInterface;
use Avris\FunctionMock\FunctionMock;
use Avris\Http\Response\Response;
use Avris\Http\Response\ResponseInterface;
use Avris\Micrus\Exception\Error\ParseException;
use Avris\Micrus\Exception\Http\NotFoundHttpException;
use Avris\Micrus\Tool\DumperInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * @covers \Avris\Micrus\Exception\Handler\ErrorHandler
 */
class ErrorHandlerTest extends TestCase
{
    /** @var LoggerInterface|MockObject */
    private $logger;

    /** @var DumperInterface|MockObject */
    private $dumper;

    /** @var EventDispatcherInterface|MockObject */
    private $dispatcher;

    /** @var  ErrorHandler|MockObject */
    private $handler;

    protected function setUp()
    {
        $this->logger = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $this->dumper = $this->getMockBuilder(DumperInterface::class)->getMock();
        $this->dispatcher = $this->getMockBuilder(EventDispatcherInterface::class)->getMock();

        $this->handler = ErrorHandler::register($this->logger, $this->dumper, $this->dispatcher);
    }

    protected function tearDown()
    {
        ErrorHandler::unregister();
        FunctionMock::clean();
    }

    public function testSingleton()
    {
        $handler2 = ErrorHandler::register($this->logger, $this->dumper, $this->dispatcher);
        $this->assertSame($handler2, $this->handler);
    }

    public function testHandleExceptionSimple()
    {
        $exception = new NotFoundHttpException('fuu');
        $expectedResponse = new Response('DUMP', 404);

        $this->logger->expects($this->once())->method('log')->with(LogLevel::WARNING, $exception);
        $this->dispatcher->expects($this->once())->method('trigger')->willReturn(null);
        $this->dumper->expects($this->once())->method('dumpExceptionResponse')->with($exception)
            ->willReturn($expectedResponse);

        $response = $this->handler->handleException($exception);

        $this->assertSame($expectedResponse, $response);
    }

    public function testHandleExceptionEvent()
    {
        $exception = new NotFoundHttpException('fuu');
        $expectedResponse = new Response('DUMP', 404);

        $this->logger->expects($this->once())->method('log')->with(LogLevel::WARNING, $exception);
        $this->dispatcher->expects($this->once())->method('trigger')->willReturn($expectedResponse);
        $this->dumper->expects($this->never())->method('dumpExceptionResponse');

        $response = $this->handler->handleException($exception);

        $this->assertSame($expectedResponse, $response);
    }

    public function testHandleExceptionEventException()
    {
        $exception = new NotFoundHttpException('fuu');
        $secondException = new \RuntimeException('buu');
        $expectedResponse = new Response('DUMP', 500);

        $this->logger->expects($this->exactly(2))->method('log')
            ->withConsecutive([LogLevel::WARNING, $exception], [LogLevel::ERROR, $secondException]);
        $this->dispatcher->expects($this->once())->method('trigger')->willThrowException($secondException);
        $this->dumper->expects($this->once())->method('dumpExceptionResponse')
            ->with($secondException)->willReturn($expectedResponse);

        $response = $this->handler->handleException($exception);

        $this->assertSame($expectedResponse, $response);
    }

    public function testHandleError()
    {
        FunctionMock::create(__NAMESPACE__, 'error_reporting', 1);

        try {
            $this->handler->handleError(E_PARSE, 'prs', __FILE__, 123);
            $this->fail('Exception expected');
        } catch (ParseException $e) {
            $this->assertEquals('prs', $e->getMessage());
            $this->assertEquals(500, $e->getCode());
            $this->assertEquals(E_PARSE, $e->getSeverity());
            $this->assertEquals(__FILE__, $e->getFile());
            $this->assertEquals(123, $e->getLine());
        }
    }

    public function testHandleErrorSilenced()
    {
        FunctionMock::create(__NAMESPACE__, 'error_reporting', 0);
        FunctionMock::create(__NAMESPACE__, 'error_get_last', [
            'type' => E_PARSE,
            'message' => 'prs',
            'file' => __FILE__,
            'line' => 123,
        ]);

        $this->handler->handleError(E_PARSE, 'prs', __FILE__, 123);

        $this->handler->handleShutdown();

        $this->assertTrue(true); // no exceptions thrown
    }

    public function testHandleShutdown()
    {
        FunctionMock::create(__NAMESPACE__, 'error_reporting', 1);
        FunctionMock::create(__NAMESPACE__, 'error_get_last', [
            'type' => E_PARSE,
            'message' => 'prs',
            'file' => __FILE__,
            'line' => 123,
        ]);

        $response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $response->expects($this->once())->method('send');

        $this->logger->expects($this->once())->method('log')->with(LogLevel::ERROR, new ParseException(
            'prs', 500, E_PARSE, __FILE__, 123
        ));
        $this->dispatcher->expects($this->once())->method('trigger')->willReturn($response);
        $this->dumper->expects($this->never())->method('dumpExceptionResponse');

        $this->handler->handleShutdown();
    }
}
