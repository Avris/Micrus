<?php
namespace Avris\Micrus\Exception\Handler;

use Avris\Micrus\Exception\Handler\ErrorEvent;
use Avris\Micrus\Exception\Handler\ErrorTemplate;
use Avris\Micrus\Exception\Http\HttpException;
use Avris\Micrus\View\TemplaterInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Exception\Handler\ErrorTemplate
 */
class ErrorTemplateTest extends TestCase
{
    public function testDebug()
    {
        $templater = $this->getMockBuilder(TemplaterInterface::class)->getMock();
        $templater->expects($this->never())->method('render');

        $template = new ErrorTemplate($templater, true);

        $this->assertNull($template->onError(new ErrorEvent(new \RuntimeException())));
    }

    public function testNoDebug()
    {
        $exception = new \RuntimeException('test');

        $templater = $this->getMockBuilder(TemplaterInterface::class)->getMock();
        $templater->expects($this->once())->method('render')
            ->with('Error/error', ['errorCode' => 500, 'exception' => $exception])
            ->willReturn('ok');

        $template = new ErrorTemplate($templater, false);

        $this->assertEquals('ok', $template->onError(new ErrorEvent($exception)));
    }

    public function testNoDebugHttp()
    {
        $exception = new HttpException(406);

        $templater = $this->getMockBuilder(TemplaterInterface::class)->getMock();
        $templater->expects($this->once())->method('render')
            ->with('Error/error', ['errorCode' => 406, 'exception' => $exception])
            ->willReturn('ok');

        $template = new ErrorTemplate($templater, false);

        $this->assertEquals('ok', $template->onError(new ErrorEvent($exception)));
    }
}
