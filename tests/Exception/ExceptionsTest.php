<?php
namespace Avris\Micrus\Controller;

use Avris\Micrus\Exception\Http\BadRequestHttpException;
use Avris\Micrus\Exception\Http\ForbiddenHttpException;
use Avris\Micrus\Exception\Http\HttpException;
use Avris\Micrus\Exception\Http\NotFoundHttpException;
use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;

class ExceptionsTest extends TestCase
{
    /**
     * @covers \Avris\Micrus\Exception\Http\BadRequestHttpException
     * @covers \Avris\Micrus\Exception\Http\ForbiddenHttpException
     * @covers \Avris\Micrus\Exception\Http\NotFoundHttpException
     * @covers \Avris\Micrus\Exception\Http\HttpException
     */
    public function testHttpExceptions()
    {
        $badRequest = new BadRequestHttpException();
        $this->assertSame(400, $badRequest->getCode());
        $this->assertSame(LogLevel::WARNING, $badRequest->getLevel());

        $forbidden = new ForbiddenHttpException();
        $this->assertSame(403, $forbidden->getCode());
        $this->assertSame(LogLevel::WARNING, $forbidden->getLevel());

        $notFound = new NotFoundHttpException();
        $this->assertSame(404, $notFound->getCode());
        $this->assertSame(LogLevel::WARNING, $notFound->getLevel());

        $server = new HttpException(500);
        $this->assertSame(500, $server->getCode());
        $this->assertSame(LogLevel::ERROR, $server->getLevel());
    }
}
