<?php
namespace Avris\Micrus\Console;

use Avris\Container\ContainerInterface;
use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Micrus\Test\TestCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;

/**
 * @covers \Avris\Micrus\Console\ConsoleApplication
 */
class ConsoleApplicationTest extends TestCase
{
    public function testConsoleApplication()
    {
        $dispatcher = $this->getMockBuilder(EventDispatcherInterface::class)->disableOriginalConstructor()->getMock();
        $dispatcher->expects($this->once())->method('trigger');

        $container = $this->getMockBuilder(ContainerInterface::class)->disableOriginalConstructor()->getMock();
        $container->expects($this->once())->method('getByTag')->with('command')->willReturn([new TestCommand()]);
        $container->expects($this->once())->method('get')->with(EventDispatcherInterface::class)->willReturn($dispatcher);

        $app = $this->getMockBuilder(AbstractApp::class)->disableOriginalConstructor()->getMock();
        $app->expects($this->once())->method('warmup')->willReturn($container);

        $consoleApp = new ConsoleApplication($app);

        $this->assertEquals('Micrus', $consoleApp->getName());
        $this->assertEquals(AbstractApp::VERSION, $consoleApp->getVersion());
        $this->assertTrue($consoleApp->getDefinition()->hasOption('env'));
        $this->assertTrue($consoleApp->getDefinition()->hasOption('no-debug'));

        $r = new \ReflectionProperty(Application::class, 'commands');
        $r->setAccessible(true);
        $commands = array_keys($r->getValue($consoleApp));
        $this->assertEquals(['help', 'list', 'test'], $commands);

        $this->assertSame($container, $consoleApp->getContainer());
    }
}