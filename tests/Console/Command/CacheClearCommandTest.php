<?php
namespace Avris\Micrus\Console\Command;

use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Micrus\Tool\Cache\CacheClearEvent;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @covers \Avris\Micrus\Console\Command\CacheClearCommand
 */
class CacheClearCommandTest extends AbstractConsoleTestCase
{
    public function testExecute()
    {
        $dispatcher = $this->getMockBuilder(EventDispatcherInterface::class)->getMock();
        $dispatcher->expects($this->once())->method('trigger')->with(new CacheClearEvent());

        $app = new Application();
        $app->add(new CacheClearCommand($dispatcher, 'test'));

        $command = $app->find('cache:clear');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
        ]);

        $this->assertTextSimilar(
            'Cache cleared for env: test',
            $commandTester->getDisplay()
        );
    }
}