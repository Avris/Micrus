<?php
namespace Avris\Micrus\Console\Command;

use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Micrus\Tool\Cache\CacheWarmupEvent;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @covers \Avris\Micrus\Console\Command\CacheWarmupCommand
 */
class CacheWarmupCommandTest extends AbstractConsoleTestCase
{
    public function testExecute()
    {
        $dispatcher = $this->getMockBuilder(EventDispatcherInterface::class)->getMock();
        $dispatcher->expects($this->once())->method('trigger')->with(new CacheWarmupEvent(false));

        $app = new Application();
        $app->add(new CacheWarmupCommand($dispatcher, 'test'));

        $command = $app->find('cache:warmup');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
        ]);

        $this->assertTextSimilar(
            'Cache warmed up for env: test',
            $commandTester->getDisplay()
        );
    }
}