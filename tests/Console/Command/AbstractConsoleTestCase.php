<?php
namespace Avris\Micrus\Console\Command;

use PHPUnit\Framework\TestCase;

abstract class AbstractConsoleTestCase extends TestCase
{
    protected function assertTextSimilar($expected, $actual)
    {
        $this->assertEquals($expected, preg_replace("#[\r\n ]+#", " ", trim($actual)));
    }
}
