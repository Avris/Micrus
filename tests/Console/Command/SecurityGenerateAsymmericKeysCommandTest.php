<?php
namespace Avris\Micrus\Console\Command;

use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Micrus\Tool\Cache\CacheClearEvent;
use Avris\Micrus\Tool\Security\CryptInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @covers \Avris\Micrus\Console\Command\SecurityGenerateAsymmericKeysCommand
 */
class SecurityGenerateAsymmericKeysCommandTest extends AbstractConsoleTestCase
{
    public function testExecute()
    {
        $crypt = $this->getMockBuilder(CryptInterface::class)->getMock();
        $crypt->expects($this->once())->method('generateAsymmetricKeys')->willReturn(['PRIV', 'PUB']);

        $app = new Application();
        $app->add(new SecurityGenerateAsymmericKeysCommand($crypt));

        $command = $app->find('security:keys:asym');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
        ]);

        $this->assertTextSimilar(
            'PRIV PUB',
            $commandTester->getDisplay()
        );
    }
}