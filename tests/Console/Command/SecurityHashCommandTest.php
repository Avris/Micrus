<?php
namespace Avris\Micrus\Console\Command;

use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Micrus\Tool\Cache\CacheClearEvent;
use Avris\Micrus\Tool\Security\CryptInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @covers \Avris\Micrus\Console\Command\SecurityPasswordHashCommand
 */
class SecurityPasswordHashCommandTest extends AbstractConsoleTestCase
{
    public function testExecute()
    {
        $crypt = $this->getMockBuilder(CryptInterface::class)->getMock();
        $crypt->expects($this->once())->method('passwordHash')->with('pwd')->willReturn('HASH');

        $app = new Application();
        $app->add(new SecurityPasswordHashCommand($crypt));

        $command = $app->find('security:password:hash');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'password' => 'pwd',
        ]);

        $this->assertTextSimilar(
            'HASH',
            $commandTester->getDisplay()
        );
    }

    public function testExecuteInteractive()
    {
        if ( !posix_isatty(STDOUT) ) {
            $this->markTestSkipped('No interactive mode');
        }

        $crypt = $this->getMockBuilder(CryptInterface::class)->getMock();
        $crypt->expects($this->once())->method('passwordHash')->with('pwd')->willReturn('HASH');

        $app = new Application();
        $app->add(new SecurityPasswordHashCommand($crypt));

        $command = $app->find('security:password:hash');
        $commandTester = new CommandTester($command);
        $commandTester->setInputs(['pwd']);
        $commandTester->execute([
            'command' => $command->getName(),
        ]);

        $this->assertTextSimilar(
            'Please type a password to be hashed: HASH',
            $commandTester->getDisplay()
        );
    }
}