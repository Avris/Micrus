<?php
namespace Avris\Micrus\Console\Command;

use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Micrus\Test\TestModule;
use Avris\Micrus\Test\TestModuleProvider;

/**
 * @covers \Avris\Micrus\Console\Command\FillEnvCommand
 */
class FillEnvCommandTest extends AbstractConsoleTestCase
{
    public function testExecute()
    {
        $app = $this->getMockBuilder(AbstractApp::class)->disableOriginalConstructor()->getMock();
        $app->expects($this->once())->method('getModules')->willReturn([
            new TestModule(),
            new TestModuleProvider(),
        ]);

        $command = new FillEnvCommand($app);

        $r = new \ReflectionMethod(FillEnvCommand::class, 'getDefaults');
        $r->setAccessible(true);
        $defaults = iterator_to_array($r->invoke($command));

        $this->assertEquals([
            'Avris\Micrus\Test' => [
                'FOO' => 'foo',
                'BAR' => 'bar',
            ],
        ], $defaults);
    }
}
