<?php
namespace Avris\Micrus\Console;

use Avris\Bag\Bag;
use Avris\Http\Request\FileBag;
use Avris\Http\Header\HeaderBag;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Console\CliRequest
 */
class CliRequestTest extends TestCase
{
    public function testRequest()
    {
        $request = new CliRequest(new Bag([
            'scheme' => 'https',
            'host' => 'micrus.dev',
            'base' => '/base',
        ]));

        $this->assertEquals('CLI', (string) $request);
        $this->assertEquals('CLI', $request->getMethod());
        $this->assertFalse($request->isPost());
        $this->assertEquals('https', $request->getScheme());
        $this->assertEquals('micrus.dev', $request->getHost());
        $this->assertEquals('https://micrus.dev', $request->getAbsoluteBase());
        $this->assertEquals('/base', $request->getBase());
        $this->assertEquals('', $request->getFrontController());
        $this->assertEquals('', $request->getUrl());
        $this->assertEquals('', $request->getCleanUrl());
        $this->assertEquals('https://micrus.dev/base', $request->getFullUrl());
        $this->assertEquals('127.0.0.1', $request->getIp());
        $this->assertFalse($request->isAjax());

        $this->assertEquals(new Bag(), $request->getQuery());
        $this->assertEquals(new Bag(), $request->getData());
        $this->assertEquals(new Bag(), $request->getCookies());
        $this->assertEquals(new Bag(), $request->getServer());
        $this->assertEquals(new FileBag(), $request->getFiles());
        $this->assertEquals(new HeaderBag(), $request->getHeaders());

        $this->assertEquals('', $request->getBody());
        $this->assertEquals([], $request->getJsonBody());
    }
}