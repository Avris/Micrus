<?php
namespace Avris\Micrus\View;

use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\SecurityManagerInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\View\AppVar
 */
class AppVarTest extends TestCase
{
    public function testBasic()
    {
        $user = $this->getMockBuilder(UserInterface::class)->getMock();

        $sm = $this->getMockBuilder(SecurityManagerInterface::class)->getMock();
        $sm->expects($this->once())->method('getUser')->willReturn($user);

        $flashBag = $this->getMockBuilder(FlashBag::class)->disableOriginalConstructor()->getMock();

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $routeMatch = $this->getMockBuilder(RouteMatch::class)->disableOriginalConstructor()->getMock();

        $requestProvider = $this->getMockBuilder(RequestProviderInterface::class)->getMock();
        $requestProvider->expects($this->once())->method('getRequest')->willReturn($request);
        $requestProvider->expects($this->once())->method('getRouteMatch')->willReturn($routeMatch);

        $session = $this->getMockBuilder(SessionInterface::class)->disableOriginalConstructor()->getMock();

        $appVar = new AppVar($sm, $flashBag, $requestProvider, $session);

        $this->assertSame($user, $appVar->getUser());
        $this->assertSame($flashBag, $appVar->getFlashBag());
        $this->assertSame($request, $appVar->getRequest());
        $this->assertSame($routeMatch, $appVar->getRouteMatch());
        $this->assertSame($session, $appVar->getSession());
    }
}
