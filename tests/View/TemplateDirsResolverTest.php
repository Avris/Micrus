<?php
namespace Avris\Micrus\View;

use Avris\Micrus\Test\TestModuleFlex;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\View\TemplateDirsResolver
 */
class TemplateDirsResolverTest extends TestCase
{
    public function testResolver()
    {
        $resolver = new TemplateDirsResolver([
            new TestModuleFlex('Module1', __DIR__ . '/../_help/templates/Module1'),
            new TestModuleFlex('Module2', __DIR__ . '/../_help/templates/Module2'),
            new TestModuleFlex('Module3', __DIR__ . '/../_help/templates/Module3'),
        ]);

        $this->assertEquals([
            __DIR__ . '/../_help/templates/Module3/templates',
            __DIR__ . '/../_help/templates/Module1/templates',
        ], $resolver->resolve());
    }
}
