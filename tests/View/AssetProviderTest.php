<?php
namespace Avris\Micrus\View;

use Avris\Bag\Bag;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\View\AssetProvider
 */
class AssetProviderTest extends TestCase
{
    public function testBasic()
    {
        $provider = new AssetProvider(
            new Bag([
                'manifest' => __DIR__ . '/../_help/base/assets/manifest.json',
                'prefixAll' => 'assets/',
            ]),
            '/base',
            __DIR__ . '/../_help/base'
        );

        $this->assertEquals('/base/assets/ass1.abcdef.txt', $provider->getLink('ass1.txt'));
        $this->assertEquals('/base/assets/ass2.txt', $provider->getLink('ass2.txt'));
        $this->assertEquals('/base/assets/ass3.txt', $provider->getLink('ass3.txt'));
        $this->assertEquals('https://avris.it/assetic/gfx/logoText.svg', $provider->getLink('external.svg'));

        $this->assertEquals('/base/assets/ass1.abcdef.txt?' . $this->booster('ass1.abcdef.txt'), $provider->getLink('ass1.txt', true));
        $this->assertEquals('/base/assets/ass2.txt?' . $this->booster('ass2.txt'), $provider->getLink('ass2.txt', true));
        $this->assertEquals('/base/assets/ass3.txt', $provider->getLink('ass3.txt', true));
        $this->assertEquals('https://avris.it/assetic/gfx/logoText.svg', $provider->getLink('external.svg', true));

        $this->assertEquals('ok1', $provider->read('ass1.txt'));
        $this->assertEquals('ok2', $provider->read('ass2.txt'));
        $this->assertEquals('', $provider->read('ass3.txt'));
    }

    private function booster(string $filename)
    {
        return substr(md5(filemtime(__DIR__ . '/../_help/base/assets/' . $filename)), 0, 6);
    }
}
