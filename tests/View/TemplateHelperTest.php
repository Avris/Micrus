<?php
namespace Avris\Micrus\View;

use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Tool\FlashBag;
use Avris\Micrus\Tool\Security\GuardianInterface;
use Avris\Micrus\Tool\Security\RoleCheckerInterface;
use Avris\Micrus\Tool\Security\SecurityManagerInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\View\TemplateHelper
 */
class TemplateHelperTest extends TestCase
{
    public function testAll()
    {
        $router = $this->getMockBuilder(RouterInterface::class)->getMock();
        $router->expects($this->once())->method('generate')->with('route', ['foo' => 'bar'])->willReturn('/ok');
        $router->expects($this->once())->method('routeExists')->with('nope')->willReturn(false);

        $user = $this->getMockBuilder(UserInterface::class)->getMock();
        $sm = $this->getMockBuilder(SecurityManagerInterface::class)->getMock();
        $sm->expects($this->exactly(3))->method('getUser')->willReturn($user);

        $roleChecker = $this->getMockBuilder(RoleCheckerInterface::class)->getMock();
        $roleChecker->expects($this->once())->method('isUserGranted')->with('ROLE_ADMIN', $user)->willReturn(true);

        $object = new \stdClass;
        $guardian = $this->getMockBuilder(GuardianInterface::class)->getMock();
        $guardian->expects($this->once())->method('check')->with($user, 'edit', $object)->willReturn(false);

        $flashBag = $this->getMockBuilder(FlashBag::class)->disableOriginalConstructor()->getMock();

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();

        $routeMatch = $this->getMockBuilder(RouteMatch::class)->disableOriginalConstructor()->getMock();

        $requestProvider = $this->getMockBuilder(RequestProviderInterface::class)->getMock();
        $requestProvider->expects($this->once())->method('getRequest')->willReturn($request);
        $requestProvider->expects($this->once())->method('getRouteMatch')->willReturn($routeMatch);

        $session = $this->getMockBuilder(SessionInterface::class)->disableOriginalConstructor()->getMock();

        $assetProvider = $this->getMockBuilder(AssetProvider::class)->disableOriginalConstructor()->getMock();
        $assetProvider->expects($this->once())->method('getLink')->with('logo.png', false)->willReturn('assets/logo.png');

        $helper = new TemplateHelper(
            $router,
            $sm,
            $roleChecker,
            $guardian,
            $flashBag,
            $requestProvider,
            $session,
            $assetProvider
        );

        $appVar = $helper->getApp();

        $this->assertSame($user, $appVar->getUser());
        $this->assertSame($flashBag, $appVar->getFlashBag());
        $this->assertSame($request, $appVar->getRequest());
        $this->assertSame($routeMatch, $appVar->getRouteMatch());
        $this->assertSame($session, $appVar->getSession());

        $this->assertEquals('/ok', $helper->route('route', ['foo' => 'bar']));
        $this->assertFalse($helper->routeExists('nope'));
        $this->assertEquals('assets/logo.png', $helper->asset('logo.png'));

        $this->assertTrue($helper->isGranted('ROLE_ADMIN'));
        $this->assertFalse($helper->canAccess('edit', $object));
    }
}
