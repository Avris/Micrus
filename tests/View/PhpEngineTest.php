<?php
namespace Avris\Micrus\View;

use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Test\TestModuleFlex;
use function foo\func;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\View\PhpEngine
 */
class PhpEngineTest extends TestCase
{
    /** @var PhpEngine */
    private $engine;

    protected function setUp()
    {
        $user = $this->getMockBuilder(UserInterface::class)->getMock();
        $user->expects($this->atMost(1))->method('getIdentifier')->willReturn('ADMIN');

        $appVar = $this->getMockBuilder(AppVar::class)->disableOriginalConstructor()->getMock();
        $appVar->expects($this->atMost(1))->method('getUser')->willReturn($user);

        $helper = $this->getMockBuilder(TemplateHelper::class)->disableOriginalConstructor()->getMock();
        $helper->expects($this->once())->method('getApp')->willReturn($appVar);

        $helper->expects($this->any())->method('route')->with('route')->willReturn('/rrr');
        $helper->expects($this->any())->method('routeExists')->with('route')->willReturn(true);
        $helper->expects($this->any())->method('asset')->with('img/test.png')->willReturn('/assets/img/test.png');
        $helper->expects($this->any())->method('isGranted')->willReturnCallback(function($role) {
            return $role === 'ROLE_USER';
        });
        $helper->expects($this->any())->method('canAccess')->with('edit', new \stdClass)->willReturn(true);

        $this->engine = new PhpEngine(
            [__DIR__ . '/../_help/templates/Module1/templates'],
            $helper
        );
    }

    public function testRender()
    {
        $output = preg_replace("#[\r\n ]+#", " ", $this->engine->render('home', ['info' => 'ablabla']));

        $this->assertEquals(
            '<strong>ablabla</strong> ADMIN <a href="/rrr">LINK</a> <img src="/assets/img/test.png"/> ' .
            'PARTIAL not admin user can edit',
            $output
        );
    }

    /**
     * @expectedException \Avris\Micrus\Exception\NotFoundException
     * @expectedExceptionMessage Template "nope.phtml" cannot be found in any of the registered template directories
     */
    public function testRenderNonexistent()
    {
        $this->engine->render('nope', []);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage test
     */
    public function testRenderInvalid()
    {
        $this->engine->render('invalid', []);
    }

    public function testHasTemplate()
    {
        $this->assertTrue($this->engine->hasTemplate('home'));
        $this->assertFalse($this->engine->hasTemplate('nope'));
    }
}
