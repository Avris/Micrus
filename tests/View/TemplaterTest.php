<?php
namespace Avris\Micrus\View;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\View\Templater
 */
class TemplaterTest extends TestCase
{
    /** @var Templater */
    private $templater;

    protected function setUp()
    {
        $engine = $this->getMockBuilder(EngineInterface::class)->getMock();
        $engine->expects($this->any())->method('hasTemplate')->willReturnCallback(function ($name) {
            return $name === 'foo';
        });
        $engine->expects($this->any())->method('render')->willReturnCallback(function ($template, $vars) {
            return $template . json_encode($vars);
        });

        $this->templater = new Templater([$engine]);
    }

    public function testHasTempalate()
    {
        $this->assertTrue($this->templater->hasTemplate('foo'));
        $this->assertFalse($this->templater->hasTemplate('bar'));
    }

    public function testRenderExists()
    {
        $this->assertEquals(
            'foo{"a":8}',
            $this->templater->render('foo', ['a' => 8])
        );
    }

    /**
     * @expectedException \Avris\Micrus\Exception\NotFoundException
     * @expectedExceptionMessage Template "bar" not found
     */
    public function testRenderNotExists()
    {
        $this->templater->render('bar', ['a' => 8]);
    }
}
