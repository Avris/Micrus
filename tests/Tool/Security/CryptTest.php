<?php
namespace Avris\Micrus\Tool\Security;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Security\Crypt
 */
class CryptTest extends TestCase
{
    /** @var CryptInterface */
    private $crypt;

    protected function setUp()
    {
        $this->crypt = new Crypt('481e403ce5b670c0e7f4dce58cef5404205ffc19426539576f7135c7572df454');
    }

    public function testPasswordHashing()
    {
        $hash = $this->crypt->passwordHash('pass');
        $this->assertFalse($this->crypt->passwordNeedsRehash($hash));
        $this->assertTrue($this->crypt->passwordVerify('pass', $hash));
    }

    public function testEncryption()
    {
        $encrypted = $this->crypt->encrypt('secret');
        $this->assertNotEquals($encrypted, 'secret');
        $this->assertEquals('secret', $this->crypt->decrypt($encrypted));
    }

    public function testDecryptInvalid()
    {
        $this->assertNull($this->crypt->decrypt('foobar'));
        $this->assertNull($this->crypt->decrypt('foobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobarfoobar'));
    }

    /**
     * @expectedException \Avris\Micrus\Exception\ConfigException
     * @expectedExceptionMessage To encrypt securely, you must generate a secret (Run "bin/micrus security:secret")
     */
    public function testBlockInvalidSecret()
    {
        (new Crypt(''))->encrypt('secret');
    }

    /**
     * @expectedException \Avris\Micrus\Exception\ConfigException
     * @expectedExceptionMessage Unrecognised encryption method: FOO
     */
    public function testInvalidMethod()
    {
        new Crypt('foo', 'FOO');
    }

    public function testGenerateSymmetricKey()
    {
        $key = ($this->crypt->generateSymmetricKey());

        $this->assertRegExp('#^[0-9a-z]{64}$#i', $key);
    }

    public function testGenerateAsymmetricKeys()
    {
        list($priv, $pub) = ($this->crypt->generateAsymmetricKeys());

        $this->assertContains('-----BEGIN PRIVATE KEY-----', $priv);
        $this->assertContains('-----END PRIVATE KEY-----', $priv);
        $this->assertContains('-----BEGIN PUBLIC KEY-----', $pub);
        $this->assertContains('-----END PUBLIC KEY-----', $pub);
    }
}
