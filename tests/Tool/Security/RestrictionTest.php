<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Model\User\UserInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Security\Restriction
 */
class RestrictionTest extends TestCase
{
    public function testFromConfig()
    {
        $restrictions = Restriction::fromConfig([
            'foo' => [
                'pattern' => 'ptrn',
                'target' => 'trgt',
                'roles' => ['ROLE_ADMIN'],
                'check' => 'edit',
                'object' => new \stdClass(),
            ],
            'bar' => null,
        ]);

        $expectedRestrictions = [
            'foo' => new Restriction('ptrn', 'trgt', ['ROLE_ADMIN'], 'edit', new \stdClass()),
            'bar' => new Restriction(),
        ];

        $this->assertEquals($expectedRestrictions, $restrictions);
    }

    public function testMatchesPattern()
    {
        $restriction = new Restriction('^/admin', 'Foo\Bar/baz');

        $request = $this->getMockBuilder(RequestInterface::class)->disableOriginalConstructor()->getMock();
        $request->expects($this->once())->method('getCleanUrl')->willReturn('/admin/post/list');

        $this->assertTrue($restriction->matches($request, 'Admin\Post/list'));
    }

    public function testMatchesTargetController()
    {
        $restriction = new Restriction('^/admin', 'Foo\Bar');

        $request = $this->getMockBuilder(RequestInterface::class)->disableOriginalConstructor()->getMock();
        $request->expects($this->once())->method('getCleanUrl')->willReturn('/foo');

        $this->assertTrue($restriction->matches($request, 'Foo\Bar/baz'));
    }

    public function testMatchesTargetAction()
    {
        $restriction = new Restriction('^/admin', 'Foo\Bar/baz');

        $request = $this->getMockBuilder(RequestInterface::class)->disableOriginalConstructor()->getMock();
        $request->expects($this->once())->method('getCleanUrl')->willReturn('/foo');

        $this->assertTrue($restriction->matches($request, 'Foo\Bar/baz'));
    }

    public function testMatchesTargetNothing()
    {
        $restriction = new Restriction('^/admin', 'Foo\Bar/baz');

        $request = $this->getMockBuilder(RequestInterface::class)->disableOriginalConstructor()->getMock();
        $request->expects($this->once())->method('getCleanUrl')->willReturn('/foo');

        $this->assertFalse($restriction->matches($request, 'Foo\Bar/abc'));
    }

    public function testValidateRolesEmpty()
    {
        $restriction = new Restriction();

        $user = $this->getMockBuilder(UserInterface::class)->getMock();
        $checker = $this->getMockBuilder(RoleCheckerInterface::class)->getMock();
        $checker->expects($this->never())->method('isUserGranted');

        $restriction->validateRoles($checker, $user);
    }

    public function testValidateRolesValid()
    {
        $restriction = new Restriction(null, null, ['ROLE_ADMIN']);

        $user = $this->getMockBuilder(UserInterface::class)->getMock();
        $checker = $this->getMockBuilder(RoleCheckerInterface::class)->getMock();
        $checker->expects($this->once())->method('isUserGranted')->with(['ROLE_ADMIN'], $user)->willReturn(true);

        $restriction->validateRoles($checker, $user);
    }

    /**
     * @expectedException \Avris\Micrus\Exception\Http\ForbiddenHttpException
     * @expectedExceptionMessage Required role: ROLE_ADMIN
     */
    public function testValidateRolesInvalid()
    {
        $restriction = new Restriction(null, null, ['ROLE_ADMIN']);

        $user = $this->getMockBuilder(UserInterface::class)->getMock();
        $checker = $this->getMockBuilder(RoleCheckerInterface::class)->getMock();
        $checker->expects($this->once())->method('isUserGranted')->with(['ROLE_ADMIN'], $user)->willReturn(false);

        $restriction->validateRoles($checker, $user);
    }

    public function testValidateCheckEmpty()
    {
        $restriction = new Restriction();

        $user = $this->getMockBuilder(UserInterface::class)->getMock();
        $guardian = $this->getMockBuilder(GuardianInterface::class)->getMock();
        $guardian->expects($this->never())->method('check');

        $restriction->validateCheck($guardian, $user, []);
    }

    public function testValidateCheckValid()
    {
        $restriction = new Restriction(null, null, [], 'edit', 'post');

        $user = $this->getMockBuilder(UserInterface::class)->getMock();
        $guardian = $this->getMockBuilder(GuardianInterface::class)->getMock();
        $guardian->expects($this->once())->method('check')->with($user, 'edit', new \stdClass)->willReturn(true);

        $restriction->validateCheck($guardian, $user, ['post' => new \stdClass]);
    }

    /**
     * @expectedException \Avris\Micrus\Exception\Http\ForbiddenHttpException
     * @expectedExceptionMessage Unmet condition "edit"
     */
    public function testValidateCheckInvalid()
    {
        $restriction = new Restriction(null, null, [], 'edit', 'post');

        $user = $this->getMockBuilder(UserInterface::class)->getMock();
        $guardian = $this->getMockBuilder(GuardianInterface::class)->getMock();
        $guardian->expects($this->once())->method('check')->with($user, 'edit', new \stdClass())->willReturn(false);

        $restriction->validateCheck($guardian, $user, ['post' => new \stdClass]);
    }
}
