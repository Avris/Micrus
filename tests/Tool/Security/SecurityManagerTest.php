<?php
namespace Avris\Micrus\Tool\Security;

use Avris\FunctionMock\FunctionMock;
use Avris\Micrus\Controller\ControllerEvent;
use Avris\Http\Cookie\Cookie;
use Avris\Http\Cookie\CookieBag;
use Avris\Http\Response\RedirectResponse;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Model\User\MemoryUser;
use Avris\Micrus\Model\User\UserProviderInterface;
use Avris\Micrus\Test\FakeSession;
use Avris\Bag\Bag;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Security\SecurityManager
 */
class SecurityManagerTest extends TestCase
{
    /** @var SecurityManager */
    private $sm;

    /** @var RequestInterface|MockObject */
    private $request;

    /** @var RequestProviderInterface|MockObject */
    private $requestProvider;

    /** @var FakeSession */
    private $session;

    /** @var RouterInterface|MockObject */
    private $router;

    /** @var UserProviderInterface|MockObject */
    private $userProvider;

    /** @var RoleCheckerInterface */
    private $roleChecker;

    /** @var GuardianInterface|MockObject */
    private $guardian;

    protected function setUp()
    {
        $config = [
            'loginPath' => 'login',
            'afterLogout' => 'home',
            'rememberMeForDays' => 14,
            'restrictions' => [
                ['controller' => '@service/test', 'roles' => ['ROLE_ADMIN']],
                ['pattern' => '^/admin', 'roles' => ['ROLE_ADMIN']],
                ['pattern' => '^/post/add', 'roles' => ['ROLE_ADMIN', 'ROLE_USER']],
                ['pattern' => '^/post/(\d+)/edit', 'roles' => ['ROLE_ADMIN', 'ROLE_USER'], 'check' => 'edit', 'object' => 'post']
            ],
            'public' => [
                ['pattern' => '^/admin/public']
            ],
        ];

        $this->request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $this->request->expects($this->any())->method('getBase')->willReturn('/');
        $this->request->expects($this->any())->method('getHost')->willReturn('micrus.test');
        $this->requestProvider = $this->getMockBuilder(RequestProviderInterface::class)->getMock();
        $this->requestProvider->expects($this->any())->method('getRequest')->willReturn($this->request);

        $this->session = new FakeSession([
            'foo' => 'bar',
        ]);

        $this->router = $this->getMockBuilder(RouterInterface::class)->getMock();
        $this->router->expects($this->any())->method('generate')->willReturn('/login?url=%2Furl');

        $this->userProvider = $this->getMockBuilder(UserProviderInterface::class)->getMock();

        $this->roleChecker = new RoleChecker(new Bag);
        $this->guardian = $this->getMockBuilder(GuardianInterface::class)->getMock();

        $this->sm = new SecurityManager(
            new Bag($config),
            $this->requestProvider,
            $this->session,
            $this->router,
            $this->userProvider,
            $this->roleChecker,
            $this->guardian
        );
    }

    protected function tearDown()
    {
        FunctionMock::clean();
    }

    private function controllerEventFactory(
        string $url,
        string $role = null,
        bool $sessionHasUser = true
    ): ControllerEvent
    {
        $this->userProvider->expects($this->any())->method('getUser')->willReturnCallback(
            function () use ($role) {
                if (!$role) {
                    return null;
                }

                return new MemoryUser('tester', [
                    'authenticators' => [
                        'password' => 'testerpassword',
                        'cookie' => 'testercookie',
                    ],
                    'roles' => [$role],
                ]);
            }
        );

        $this->session->set('_user', $sessionHasUser ? 'tester' : null);

        $this->request->expects($this->any())->method('getCleanUrl')->willReturn($url);
        $this->request->expects($this->any())->method('getCookies')->willReturn(new Bag);

        return new ControllerEvent(
            $this->request,
            sprintf(
                '%s/%s',
                substr($url, 0, 5) == '/test' ? 'Test' : 'Free',
                substr($url, 0, 5) == '/test' ? substr($url, 6) : 'free'
            ),
            function () {},
            []
        );
    }

    public function testRequestEventHomeUnlogged()
    {
        $event = $this->controllerEventFactory('/', null, false);
        $this->sm->onController($event);
        $this->assertNull($event->getResponse());
    }

    public function testRequestEventHomeLogged()
    {
        $event = $this->controllerEventFactory('/', 'ROLE_USER');
        $this->sm->onController($event);
        $this->assertNull($event->getResponse());
    }

    public function testRequestEventAdminRedirected()
    {
        $event = $this->controllerEventFactory('/admin/whatever', null, false);
        $this->sm->onController($event);
        $this->assertInstanceOf(RedirectResponse::class, $event->getResponse());
    }

    /**
     * @expectedException \Avris\Micrus\Exception\Http\ForbiddenHttpException
     */
    public function testRequestEventAdminBlocked()
    {
        $event = $this->controllerEventFactory('/admin/whatever', 'ROLE_USER');
        $this->sm->onController($event);
    }

    public function testRequestEventAdminAllowed()
    {
        $event = $this->controllerEventFactory('/admin/whatever', 'ROLE_ADMIN');
        $this->sm->onController($event);
        $this->assertNull($event->getResponse());
    }

    public function testRequestEventAdminPublic()
    {
        $event = $this->controllerEventFactory('/admin/public', 'ROLE_USER');
        $this->sm->onController($event);
        $this->assertNull($event->getResponse());
    }

    public function testRequestEventPostCheckAllow()
    {
        $this->guardian->expects($this->once())->method('check')->willReturn(true);

        $event = $this->controllerEventFactory('/post/123/edit', 'ROLE_USER');
        $this->sm->onController($event);
        $this->assertNull($event->getResponse());
    }

    /**
     * @expectedException \Avris\Micrus\Exception\Http\ForbiddenHttpException
     */
    public function testRequestEventPostCheckBlocked()
    {
        $this->guardian->expects($this->once())->method('check')->willReturn(false);

        $event = $this->controllerEventFactory('/post/999/edit', 'ROLE_USER');
        $this->sm->onController($event);
    }

    public function testRequestEventEmptyRestrictions()
    {
        $emptySm = new SecurityManager(
            new Bag([]),
            $this->requestProvider,
            $this->session,
            $this->router,
            $this->userProvider,
            $this->roleChecker,
            $this->guardian
        );
        $event = $this->controllerEventFactory('/admin/whatever', 'ROLE_USER');
        $emptySm->onController($event);
        $this->assertNull($event->getResponse());
    }

    public function testRequestEventInvalidInSession()
    {
        $event = $this->controllerEventFactory('/admin/whatever', null, true);
        $this->sm->onController($event);
        $this->assertInstanceOf(RedirectResponse::class, $event->getResponse());
        $this->assertNull($this->session->get('_user'));
        $this->assertEquals('bar', $this->session->get('foo'));
    }

    public function testRequestEventLoginFromRememberMe()
    {
        $this->request->expects($this->any())->method('getCookies')->willReturn(new Bag([
            SecurityManager::COOKIE_NAME => 'tester|testercookie',
        ]));
        $event = $this->controllerEventFactory('/admin/whatever', 'ROLE_ADMIN', false);
        $this->sm->onController($event);
        $this->assertNull($event->getResponse());
    }

    public function testRequestEventLoginFromRememberMeFailed()
    {
        $this->request->expects($this->any())->method('getCookies')->willReturn(new Bag([
            SecurityManager::COOKIE_NAME => 'tester|zzz',
        ]));
        $event = $this->controllerEventFactory('/admin/whatever', 'ROLE_ADMIN', false);
        $this->sm->onController($event);
        $this->assertInstanceOf(RedirectResponse::class, $event->getResponse());
    }

    public function testRequestEventLoginFromRememberMeFailedNoSuchUser()
    {
        $this->request->expects($this->any())->method('getCookies')->willReturn(new Bag([
            SecurityManager::COOKIE_NAME => 'nonexistent|nonexistentcookie',
        ]));
        $event = $this->controllerEventFactory('/admin/whatever', null, true);
        $this->sm->onController($event);
        $this->assertInstanceOf(RedirectResponse::class, $event->getResponse());
    }

    public function testRequestEventLoginFromRememberMeWrong()
    {
        $this->request->expects($this->any())->method('getCookies')->willReturn(new Bag([
            SecurityManager::COOKIE_NAME => 'tester',
        ]));
        $event = $this->controllerEventFactory('/admin/whatever', 'ROLE_ADMIN', false);
        $this->sm->onController($event);
        $this->assertInstanceOf(RedirectResponse::class, $event->getResponse());
    }

    public function testLogin()
    {
        FunctionMock::create(__NAMESPACE__, 'hash', 'abcdef');

        $user = new MemoryUser('tester', []);
        $cookies = new CookieBag();
        $this->session->delete('_user');

        $returnedUser = $this->sm->login($user, $cookies);
        $this->assertSame($user, $returnedUser);
        $this->assertSame($user, $this->sm->getUser());
        $this->assertEquals('tester', $this->session->get('_user'));

        $auths = $user->getAuthenticators();
        $auth = reset($auths);
        $this->assertEquals(SecurityManager::AUTHENTICATOR_COOKIE, $auth->getType());
        $this->assertEquals('abcdef', $auth->getPayload());

        $cookie = $cookies->all()[SecurityManager::COOKIE_NAME];

        $this->assertEquals(
            new Cookie(SecurityManager::COOKIE_NAME, 'tester|abcdef', $cookie->getExpire(), '/', 'micrus.test'),
            $cookie
        );

        $this->assertEquals(
            (int) (new \DateTime('+14 days'))->format('U'),
            $cookie->getExpire(),
            '',
            5
        );
    }

    public function testLogout()
    {
        $user = new MemoryUser('tester', []);

        $this->sm->login($user);
        $this->assertEquals([
            'foo' => 'bar',
            '_user' => 'tester',
        ], $this->session->all());

        $cookies = new CookieBag([
            SecurityManager::COOKIE_NAME => new Cookie(SecurityManager::COOKIE_NAME, 'tester', 0, '/', 'micrus.test'),
        ]);

        $redirectRoute = $this->sm->logout($cookies);
        $this->assertEquals('home', $redirectRoute);

        $this->assertEquals([], $this->session->all());
        $this->assertEquals([
            SecurityManager::COOKIE_NAME => new Cookie(SecurityManager::COOKIE_NAME, '', 1, '/', 'micrus.test')
        ], $cookies->all());
    }
}