<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Container\ContainerInterface;
use Avris\FunctionMock\FunctionMock;
use Avris\Http\Cookie\Cookie;
use Avris\Http\Cookie\CookieBag;
use Avris\Http\Request\FileBag;
use Avris\Http\Header\HeaderBag;
use Avris\Http\Request\Request;
use Avris\Micrus\Controller\RequestEvent;
use Avris\Http\Response\Response;
use Avris\Micrus\Controller\ResponseEvent;
use Avris\Bag\Bag;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Security\SecurityEnhancer
 */
class SecurityEnhancerTest extends TestCase
{
    /** @var CryptInterface|MockObject */
    protected $crypt;

    /** @var FunctionMock */
    protected $iniSet;

    protected static $iniSettings = [];

    protected function setUp()
    {
        $this->crypt = $this->getMockBuilder(CryptInterface::class)->getMock();
        $this->crypt->expects($this->any())->method('encrypt')
            ->willReturnCallback(function ($value) { return $value . 'xxx'; });
        $this->crypt->expects($this->any())->method('decrypt')
            ->willReturnCallback(function ($value) { return substr($value, -3) === 'xxx' ? substr($value, 0, -3) : false; });

        self::$iniSettings = [];

        FunctionMock::create(__NAMESPACE__, 'session_id', '');
        FunctionMock::create(__NAMESPACE__, 'session_name', 'PHPSESSID');

        $this->iniSet = FunctionMock::create(__NAMESPACE__, 'ini_set', function($key, $value) {
            SecurityEnhancerTest::$iniSettings[$key] = $value;
        });
    }

    protected function tearDown()
    {
        FunctionMock::clean();
    }

    public function testOnWarmupAlreadyHasSession()
    {
        FunctionMock::create(__NAMESPACE__, 'session_id', 'SESSIONID');
        $se = new SecurityEnhancer($this->crypt, new Bag([
            'preventSessionFixation' => true,
            'cookiesOnlyHttp' => true,
            'ssl' => true,
        ]));

        $se->onWarmup();

        $this->assertEquals([], $this->iniSet->getInvocations());
        $this->assertEquals([], self::$iniSettings);
    }

    public function testOnWarmupNone()
    {
        $se = new SecurityEnhancer($this->crypt, new Bag());

        $se->onWarmup();

        $this->assertEquals([], $this->iniSet->getInvocations());
        $this->assertEquals([], self::$iniSettings);
    }

    public function testOnWarmupOnlyFixation()
    {
        $se = new SecurityEnhancer($this->crypt, new Bag([
            'preventSessionFixation' => true,
        ]));

        $se->onWarmup();

        $this->assertCount(3, $this->iniSet->getInvocations());
        $this->assertEquals([
            'session.use_cookies' => 1,
            'session.use_only_cookies' => 1,
            'session.use_trans_sid' => 1,
        ], self::$iniSettings);
    }

    public function testOnWarmupNonSSL()
    {
        $se = new SecurityEnhancer($this->crypt, new Bag([
            'preventSessionFixation' => true,
            'cookiesOnlyHttp' => true,
        ]));

        $se->onWarmup();

        $this->assertCount(4, $this->iniSet->getInvocations());
        $this->assertEquals([
            'session.use_cookies' => 1,
            'session.use_only_cookies' => 1,
            'session.use_trans_sid' => 1,
            'session.cookie_httponly' => 1,
        ], self::$iniSettings);
    }

    public function testOnWarmupAll()
    {
        $se = new SecurityEnhancer($this->crypt, new Bag([
            'preventSessionFixation' => true,
            'cookiesOnlyHttp' => true,
            'ssl' => true,
        ]));

        $se->onWarmup();

        $this->assertCount(5, $this->iniSet->getInvocations());
        $this->assertEquals([
            'session.use_cookies' => 1,
            'session.use_only_cookies' => 1,
            'session.use_trans_sid' => 1,
            'session.cookie_httponly' => 1,
            'session.cookie_secure' => 1,
        ], self::$iniSettings);
    }

    public function testOnRequestNoEncryption()
    {
        $se = new SecurityEnhancer($this->crypt, new Bag([
            'encryptCookies' => false,
        ]));

        $cookies = new Bag([
            'foo' => 'bar',
            'lorem' => 'ipsum',
        ]);

        $requestEvent = new RequestEvent(
            $this->getMockBuilder(ContainerInterface::class)->getMock(),
            new Request(new HeaderBag, new Bag, new Bag, new Bag, $cookies, new FileBag)
        );

        $se->onRequest($requestEvent);

        $this->assertEquals([
            'foo' => 'bar',
            'lorem' => 'ipsum',
        ], $cookies->all());
    }

    public function testOnRequestWithEncryption()
    {
        $se = new SecurityEnhancer($this->crypt, new Bag([
            'encryptCookies' => true,
        ]));

        FunctionMock::create(__NAMESPACE__, 'session_id', 'SESSIONID');

        $cookies = new Bag([
            'PHPSESSID' => 'whatever',
            'foo' => 'barxxx',
            'lorem' => 'ipsum',
        ]);

        $requestEvent = new RequestEvent(
            $this->getMockBuilder(ContainerInterface::class)->getMock(),
            new Request(new HeaderBag, new Bag, new Bag, new Bag, $cookies, new FileBag)
        );

        $se->onRequest($requestEvent);

        $this->assertEquals([
            'PHPSESSID' => 'whatever',
            'foo' => 'bar',
            'lorem' => '',
        ], $cookies->all());
    }

    public function testOnResponseNoEncryption()
    {
        $se = new SecurityEnhancer($this->crypt, new Bag());

        $cookies = new CookieBag([
            'foo' => 'bar',
            'lorem' => 'ipsum',
        ]);

        $responseEvent = new ResponseEvent(
            new Response('', 200, [], $cookies)
        );

        $se->onResponse($responseEvent);

        $this->assertEquals([
            'foo' => new Cookie('foo', 'bar'),
            'lorem' => new Cookie('lorem', 'ipsum'),
        ], $cookies->all());
    }

    public function testOnResponseWithEncryption()
    {
        $se = new SecurityEnhancer($this->crypt, new Bag([
            'encryptCookies' => true,
            'ssl' => true,
            'cookiesOnlyHttp' => true,
        ]));

        $cookies = new CookieBag([
            'foo' => 'bar',
            'lorem' => 'ipsum',
        ]);

        $responseEvent = new ResponseEvent(
            new Response('', 200, [], $cookies)
        );

        $se->onResponse($responseEvent);

        $this->assertEquals([
            'foo' => new Cookie('foo', 'barxxx', 0, '/', '', true, true),
            'lorem' => new Cookie('lorem', 'ipsumxxx', 0, '/', '', true, true),
        ], $cookies->all());
    }

    public function testOnResponseSecureHeaders()
    {
        $se = new SecurityEnhancer($this->crypt, new Bag([
            'secureHeaders' => [
                'enabled' => true,
                'Content-Security-Policy' => 'script-src "self"',
                'X-Content-Type-Options' => null,
            ],
        ]));

        $responseEvent = new ResponseEvent(
            $response = new Response('', 200, [
                'X-FOO' => 'bar',
                'X-Powered-By' => 'Apache',
                'X-XSS-Protection' => '0',
                'X-Content-Type-Options' => 'sniff',
            ], $cookies = new CookieBag)
        );

        $se->onResponse($responseEvent);

        $this->assertEquals([], $cookies->all());

        $this->assertEquals([
            'X-Foo' => 'bar',
            'X-Powered-By' => '',
            'X-Xss-Protection' => '1; mode=block',
            'X-Content-Type-Options' => 'sniff',
            'Strict-Transport-Security' => 'max-age=31536000; includeSubDomains',
            'X-Frame-Options' => 'SAMEORIGIN',
            'Server' => '',
            'Content-Security-Policy' => 'script-src "self"',
        ], $response->getHeaders()->all());
    }

    public function testOnResponseNoAction()
    {
        $se = new SecurityEnhancer($this->crypt, new Bag());

        $responseEvent = new ResponseEvent(
            $response = new Response('', 200, [
                'X-FOO' => 'bar',
                'X-Powered-By' => 'Apache',
                'X-XSS-Protection' => '0',
                'X-Content-Type-Options' => 'sniff',
            ], $cookies = new CookieBag)
        );

        $se->onResponse($responseEvent);

        $this->assertEquals([], $cookies->all());

        $this->assertEquals([
            'X-Powered-By' => 'Apache',
            'X-Content-Type-Options' => 'sniff',
            'X-Foo' => 'bar',
            'X-Xss-Protection' => '0',
        ], $response->getHeaders()->all());
    }
}