<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Http\Cookie\CookieBag;
use Avris\Http\Response\RedirectResponse;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Security\LogoutController
 */
class LogoutControllerTest extends TestCase
{
    public function testController()
    {
        $sm = $this->getMockBuilder(SecurityManagerInterface::class)->getMock();
        $sm->expects($this->once())->method('logout')->with(new CookieBag)->willReturn('home');

        $router = $this->getMockBuilder(RouterInterface::class)->getMock();
        $router->expects($this->once())->method('generate')->with('home')->willReturn('/');

        $controller = new LogoutController();

        $response = $controller->logoutAction($router, $sm);

        $this->assertEquals(
            new RedirectResponse('/', 302, new CookieBag()),
            $response
        );
    }
}
