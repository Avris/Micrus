<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Http\Cookie\CookieBag;
use Avris\Http\Response\RedirectResponse;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Model\User\MemoryUser;
use Avris\Micrus\Test\TestGuard;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Security\Guardian
 */
class GuardianTest extends TestCase
{
    public function testCheck()
    {
        $author = new MemoryUser('author', []);
        $otherUser = new MemoryUser('other', []);

        $guardian = new Guardian([
            new TestGuard('edit', 'post', $author),
            new TestGuard('delete', 'post', null),
        ]);

        $this->assertTrue($guardian->check($author, 'edit', 'post'));
        $this->assertFalse($guardian->check($otherUser, 'edit', 'post'));

        $this->assertFalse($guardian->check($author, 'delete', 'post'));
        $this->assertFalse($guardian->check($otherUser, 'delete', 'post'));

        $this->assertTrue($guardian->check($author, 'foo', 'post'));
        $this->assertTrue($guardian->check($otherUser, 'foo', 'post'));
    }
}
