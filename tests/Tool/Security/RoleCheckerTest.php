<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Bag\Bag;
use Avris\Micrus\Model\User\MemoryUser;
use PHPUnit\Framework\TestCase;

class RoleCheckerTest extends TestCase
{
    /** @var RoleChecker */
    protected $roleChecker;

    protected function setUp()
    {
        $this->roleChecker = new RoleChecker(new Bag([
            'ROLE_ADMIN' => 'ROLE_MANAGER',
            'ROLE_MANAGER' => ['ROLE_READER', 'ROLE_WRITER'],
            'ROLE_READER' => 'ROLE_USER',
            'ROLE_WRITER' => 'ROLE_USER',
        ]));
    }

    public function testRoles()
    {
        $expected = [
            'ROLE_ADMIN' => ['ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_READER', 'ROLE_USER', 'ROLE_WRITER' ],
            'ROLE_MANAGER' => ['ROLE_MANAGER', 'ROLE_READER', 'ROLE_USER', 'ROLE_WRITER'],
            'ROLE_READER' => ['ROLE_READER', 'ROLE_USER'],
            'ROLE_WRITER' => ['ROLE_WRITER', 'ROLE_USER'],
        ];

        $this->assertEquals($expected, $this->roleChecker->getRoles());
    }

    /**
     * @dataProvider isUserGrantedProvider
     */
    public function testIsUserGranted($requiredRoles, $userRoles, $expectedResult)
    {
        $user = $userRoles === null ? null : new MemoryUser('test', ['roles' => (array) $userRoles]);

        $result = $this->roleChecker->isUserGranted($requiredRoles, $user);

        $this->assertSame($expectedResult, $result);
    }

    public function isUserGrantedProvider()
    {
        return [
            ['ROLE_USER', null, false],
            ['ROLE_ADMIN', 'ROLE_ADMIN', true],
            ['ROLE_ADMIN', ['ROLE_ADMIN', 'ROLE_USER'], true],
            ['ROLE_USER', ['ROLE_ADMIN', 'ROLE_USER'], true],
            ['ROLE_ADMIN', 'ROLE_WRITER', false],
            ['ROLE_USER', 'ROLE_WRITER', true],
            [['ROLE_ADMIN', 'ROLE_MANAGER'], 'ROLE_WRITER', false],
            [['ROLE_ADMIN', 'ROLE_MANAGER'], 'ROLE_MANAGER', true],
            [[], [], true],
        ];
    }
}