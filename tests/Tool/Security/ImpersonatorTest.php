<?php
namespace Avris\Micrus\Tool\Security;

use Avris\Bag\Bag;
use Avris\Container\ContainerInterface;
use Avris\Http\Response\RedirectResponse;
use Avris\Micrus\Controller\RequestEvent;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Model\User\UserProviderInterface;
use Avris\Micrus\Test\FakeSession;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Security\Impersonator
 */
class ImpersonatorTest extends TestCase
{
    const SESSION_IMPERSONATED = [
        '_impersonator' => 'admin',
        '_impersonator_data' => [
            'foo' => 'bar',
        ],
    ];

    const SESSION_CLEAR = [
        'foo' => 'bar'
    ];

    /** @var SecurityManagerInterface|MockObject */
    private $sm;

    /** @var UserProviderInterface|MockObject */
    private $userProvider;

    /** @var RoleCheckerInterface|MockObject */
    private $roleChecker;

    /** @var FakeSession */
    private $session;

    /** @var Impersonator */
    private $impersonator;

    /** @var UserInterface */
    private $admin;

    /** @var UserInterface */
    private $user;

    protected function setUp()
    {
        $this->sm = $this->getMockBuilder(SecurityManagerInterface::class)->getMock();
        $this->userProvider = $this->getMockBuilder(UserProviderInterface::class)->getMock();
        $this->roleChecker = $this->getMockBuilder(RoleCheckerInterface::class)->getMock();
        $this->session = new FakeSession(self::SESSION_CLEAR);

        $this->admin = $this->getMockBuilder(UserInterface::class)->getMock();
        $this->admin->expects($this->any())->method('getIdentifier')->willReturn('admin');
        $this->user = $this->getMockBuilder(UserInterface::class)->getMock();
        $this->user->expects($this->any())->method('getIdentifier')->willReturn('user');

        $this->impersonator = new Impersonator(
            $this->sm,
            $this->userProvider,
            $this->roleChecker,
            $this->session
        );
    }

    public function testImpersonate()
    {
        $this->sm->expects($this->once())->method('getUser')->willReturn($this->admin);
        $this->roleChecker->expects($this->once())->method('isUserGranted')
            ->with('ROLE_IMPERSONATOR', $this->admin)->willReturn(true);

        $this->sm->expects($this->once())->method('login')->with($this->user);

        $this->impersonator->impersonate($this->user);

        $this->assertEquals(self::SESSION_IMPERSONATED, $this->session->all());
    }

    /**
     * @expectedException \Avris\Micrus\Exception\Http\ForbiddenHttpException
     * @expectedExceptionMessage Role ROLE_IMPERSONATOR is required to impersonate
     */
    public function testImpersonateNotAllowed()
    {
        $this->sm->expects($this->once())->method('getUser')->willReturn($this->admin);
        $this->roleChecker->expects($this->once())->method('isUserGranted')
            ->with('ROLE_IMPERSONATOR', $this->admin)->willReturn(false);

        $this->sm->expects($this->never())->method('login')->with($this->user);

        $this->impersonator->impersonate($this->user);
    }

    public function testExitImpersonation()
    {
        $this->session->setData(self::SESSION_IMPERSONATED);

        $this->session->set('bar', 'baz');

        $this->userProvider->expects($this->once())->method('getUser')->with('admin')->willReturn($this->admin);
        $this->sm->expects($this->once())->method('login')->with($this->admin);

        $this->impersonator->exitImpersonation();

        $this->assertEquals(self::SESSION_CLEAR, $this->session->all());
    }

    public function testExitImpersonationIfNotImpersonating()
    {
        $this->sm->expects($this->never())->method('login');

        $this->impersonator->exitImpersonation();

        $this->assertEquals(self::SESSION_CLEAR, $this->session->all());
    }

    /**
     * @expectedException \Avris\Micrus\Exception\Http\NotFoundHttpException
     * @expectedExceptionMessage User "admin" does not exist
     */
    public function testExitImpersonationAdminDoesNotExist()
    {
        $this->session->setData(self::SESSION_IMPERSONATED);

        $this->session->set('bar', 'baz');

        $this->userProvider->expects($this->once())->method('getUser')->with('admin')->willReturn(null);
        $this->sm->expects($this->never())->method('login');

        $this->impersonator->exitImpersonation();
    }

    public function testEventNotInterested()
    {
        $this->sm->expects($this->never())->method('login');

        $this->assertNull(
            $this->impersonator->onRequest($this->buildRequestEvent('/foo', null))
        );
    }

    /**
     * @expectedException \Avris\Micrus\Exception\Http\NotFoundHttpException
     * @expectedExceptionMessage User "user" does not exist
     */
    public function testEventUserDoesNotExist()
    {
        $this->userProvider->expects($this->once())->method('getUser')->with('user')->willReturn(null);
        $this->sm->expects($this->never())->method('login');

        $this->impersonator->onRequest($this->buildRequestEvent('/foo?_impersonate=user', 'user'));
    }

    public function testEventImpersonate()
    {
        $this->sm->expects($this->once())->method('getUser')->willReturn($this->admin);
        $this->roleChecker->expects($this->once())->method('isUserGranted')
            ->with('ROLE_IMPERSONATOR', $this->admin)->willReturn(true);

        $this->userProvider->expects($this->once())->method('getUser')->with('user')->willReturn($this->user);

        $this->sm->expects($this->once())->method('login')->with($this->user);

        $this->assertEquals(
            new RedirectResponse('/foo'),
            $this->impersonator->onRequest($this->buildRequestEvent('/foo?_impersonate=user', 'user'))
        );

        $this->assertEquals(self::SESSION_IMPERSONATED, $this->session->all());
    }

    public function testEventExitImpersonation()
    {
        $this->session->setData(self::SESSION_IMPERSONATED);

        $this->session->set('bar', 'baz');

        $this->userProvider->expects($this->once())->method('getUser')->with('admin')->willReturn($this->admin);
        $this->sm->expects($this->once())->method('login')->with($this->admin);

        $this->assertEquals(
            new RedirectResponse('/foo?x=y&osiem=8'),
            $this->impersonator->onRequest($this->buildRequestEvent('/foo?x=y&_impersonate=_exit&osiem=8', '_exit'))
        );

        $this->assertEquals(self::SESSION_CLEAR, $this->session->all());
    }

    private function buildRequestEvent(string $url, ?string $parameter): RequestEvent
    {
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $request->expects($this->any())->method('getUrl')->willReturn($url);
        $request->expects($this->any())->method('getQuery')->willReturn(new Bag(['_impersonate' => $parameter]));

        return new RequestEvent(
            $this->getMockBuilder(ContainerInterface::class)->getMock(),
            $request
        );
    }
}