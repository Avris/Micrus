<?php
namespace Avris\Micrus\Tool;

use Avris\FunctionMock\FunctionMock;
use Avris\Http\Response\Response;
use Avris\Micrus\Exception\Http\NotFoundHttpException;
use Avris\Micrus\Test\FakeSession;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Dumper
 */
class DumperTest extends TestCase
{
    public function testDumperDebug()
    {
        $dumper = new Dumper(true);

        $var = 'foo';
        $exception = new NotFoundHttpException('osiem');

        $this->assertEquals('"foo"' . PHP_EOL, $dumper->dump($var));
        $this->assertEquals(new Response('"foo"' . PHP_EOL, 204), $dumper->dumpResponse($var, 204));
        $response = $dumper->dumpExceptionResponse($exception);
        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(404, $response->getStatus());
        $this->assertContains(NotFoundHttpException::class, $response->getContent());
    }

    public function testDumperNoDebug()
    {
        $dumper = new Dumper(false);

        $var = 'foo';
        $exception = new NotFoundHttpException('osiem');

        $this->assertEquals('', $dumper->dump($var));
        $this->assertEquals(new Response('', 204), $dumper->dumpResponse($var, 204));
        $this->assertEquals(new Response('', 404), $dumper->dumpExceptionResponse($exception));
    }
}
