<?php
namespace Avris\Micrus\Tool\Cache;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Cache\CacheItem
 */
class CacheItemTest extends TestCase
{
    public function testMiss()
    {
        $item = new CacheItem('key', false);
        $this->assertFalse($item->isHit());
    }

    public function testHit()
    {
        $item = new CacheItem('key', true);
        $this->assertEquals('key', $item->getKey());
        $this->assertNull($item->get());
        $this->assertTrue($item->isHit());

        $item->set('val');
        $this->assertEquals('val', $item->get());
    }

    public function testExpiration()
    {
        $item = new CacheItem('key', true, 'val', time() - 60);
        $this->assertEquals('key', $item->getKey());
        $this->assertNull($item->get());
        $this->assertFalse($item->isHit());

        $item->expiresAfter(1);
        $this->assertTrue($item->isHit());
        $this->assertEquals('val', $item->get());
        sleep(1);
        $this->assertFalse($item->isHit());

        $item->expiresAt(new \DateTime('+1 second'));
        $this->assertTrue($item->isHit());
        sleep(2);
        $this->assertFalse($item->isHit());

        $default = (int) (new \DateTime('+' . CacheItem::DEFAULT_TTL . ' seconds'))->format('U');
        $item->expiresAt(null);
        $this->assertEquals($default, $item->getExpiresAt());
        $item->expiresAfter(null);
        $this->assertEquals($default, $item->getExpiresAt());
    }
}
