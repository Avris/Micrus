<?php
namespace Avris\Micrus\Tool\Cache;

use Avris\Micrus\Test\TestCacheItem;
use PHPUnit\Framework\TestCase;
use Psr\Cache\CacheItemPoolInterface;

/**
 * @covers \Avris\Micrus\Tool\Cache\Cacher
 */
class CacherTest extends TestCase
{
    public function testCacheDebug()
    {
        $pool = $this->getMockBuilder(CacheItemPoolInterface::class)->getMock();
        $pool->expects($this->never())->method('getItem');
        $pool->expects($this->never())->method('save');

        $cacher = new Cacher($pool, true);

        $this->assertSame($pool, $cacher->getPool());

        $result = $cacher->cache('key', function () {
            return 'ok';
        });

        $this->assertEquals('ok', $result);
    }

    public function testCacheNoDebug()
    {
        $pool = $this->getMockBuilder(CacheItemPoolInterface::class)->getMock();
        $pool->expects($this->exactly(2))->method('getItem')->withConsecutive(['foo_bar'], ['osiem'])
            ->willReturnCallback(function(string $key) {
                if ($key === 'osiem') {
                    return new TestCacheItem($key, true, '8');
                }
                return new TestCacheItem($key, false);
            });
        $pool->expects($this->once())->method('save')->with(
            new TestCacheItem('foo_bar', true, 'FOO')
        );

        $cacher = new Cacher($pool, false);

        $this->assertSame($pool, $cacher->getPool());

        $result1 = $cacher->cache('foo:bar', function () {
            return 'FOO';
        });

        $this->assertEquals('FOO', $result1);

        $result2 = $cacher->cache('osiem', function () {
            return '123';
        });

        $this->assertEquals('8', $result2);
    }
}
