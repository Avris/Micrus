<?php
namespace Avris\Micrus\Tool\Cache;

use Avris\Micrus\Tool\DirectoryCleaner;
use PHPUnit\Framework\TestCase;
use Psr\Cache\CacheItemPoolInterface;

/**
 * @covers \Avris\Micrus\Tool\Cache\CacheCleaner
 */
class CacheCleanerTest extends TestCase
{
    public function testBasic()
    {
        $pool = $this->getMockBuilder(CacheItemPoolInterface::class)->getMock();
        $pool->expects($this->once())->method('clear');

        $dir = '/tmp/cache';
        $dirCleaner = $this->getMockBuilder(DirectoryCleaner::class)->disableOriginalConstructor()->getMock();
        $dirCleaner->expects($this->once())->method('removeDir')->with($dir);

        $cleaner = new CacheCleaner($pool, $dirCleaner, $dir);
        $cleaner->clearCache();
    }
}
