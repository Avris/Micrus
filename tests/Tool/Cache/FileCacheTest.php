<?php
namespace Avris\Micrus\Tool\Cache;

use Avris\FunctionMock\FunctionMock;
use Avris\Micrus\Tool\DirectoryCleaner;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Cache\FileCache
 */
class FileCacheTest extends TestCase
{
    /** @var DirectoryCleaner */
    private $cleaner;

    /** @var string */
    private $dir;
    
    protected function setUp()
    {
        $this->cleaner = new DirectoryCleaner();
        $this->dir = __DIR__ . '/../../_help/cache';

        $this->cleaner->removeDir($this->dir);


    }

    protected function tearDown()
    {
        $this->cleaner->removeDir($this->dir);
        FunctionMock::clean();
    }

    /**
     * @expectedException \Avris\Micrus\Exception\ConfigException
     * @expectedExceptionMessage Cannot write cache in
     */
    public function testDirNotWritable()
    {
        FunctionMock::create(__NAMESPACE__, 'mkdir', false);
        new FileCache($this->dir, $this->cleaner);
    }

    public function testFlow()
    {
        $pool = new FileCache($this->dir, $this->cleaner);

        $this->assertFalse($pool->hasItem('foo'));
        $item1 = $pool->getItem('foo');
        $this->assertInstanceOf(CacheItem::class, $item1);
        $this->assertEquals('foo', $item1->getKey());
        $this->assertFalse($item1->isHit());
        $this->assertNull($item1->get());
        $this->assertFileNotExists($this->dir . '/foo.obj');

        $item1->set('ok');
        $this->assertTrue($pool->save($item1));

        $this->assertTrue($pool->hasItem('foo'));
        $item2 = $pool->getItem('foo');
        $this->assertInstanceOf(CacheItem::class, $item2);
        $this->assertEquals('foo', $item2->getKey());
        $this->assertTrue($item2->isHit());
        $this->assertEquals('ok', $item2->get());
        $this->assertFileExists($this->dir . '/foo.obj');
        $this->assertEquals('a:2:{i:0;s:2:"ok";i:1;N;}', file_get_contents($this->dir . '/foo.obj'));

        $items = $pool->getItems(['foo', 'bar']);
        $this->assertCount(2, $items);
        $this->assertEquals(new CacheItem('foo', true, 'ok'), $items['foo']);
        $this->assertEquals(new CacheItem('bar', false), $items['bar']);

        $this->assertTrue($pool->saveDeferred(new CacheItem('bar', true, 'BAR')));
        $this->assertFalse($pool->hasItem('bar'));
        $this->assertFileNotExists($this->dir . '/bar.obj');

        $this->assertTrue($pool->commit());
        $this->assertTrue($pool->hasItem('bar'));
        $this->assertFileExists($this->dir . '/bar.obj');

        $this->assertTrue($pool->deleteItems(['bar', 'baz']));
        $this->assertTrue($pool->hasItem('foo'));
        $this->assertFileExists($this->dir . '/foo.obj');
        $this->assertFalse($pool->hasItem('bar'));
        $this->assertFileNotExists($this->dir . '/bar.obj');

        $this->assertTrue($pool->clear());

        $this->assertFalse(file_exists($this->dir));
        $this->assertFalse($pool->hasItem('foo'));
        $this->assertFileNotExists($this->dir . '/foo.obj');
        $this->assertFalse($pool->hasItem('bar'));
        $this->assertFileNotExists($this->dir . '/bar.obj');
        $this->assertFalse($pool->hasItem('baz'));
        $this->assertFileNotExists($this->dir . '/baz.obj');
    }
}
