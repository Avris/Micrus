<?php
namespace Avris\Micrus\Tool;

use Avris\FunctionMock\FunctionMock;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\DefaultLogger
 */
class DefaultLoggerTest extends TestCase
{
    public function testLogger()
    {
        $fwrite = FunctionMock::create(__NAMESPACE__, 'fwrite', true);

        $logger = new DefaultLogger();

        $logger->log('error', 'msg', ['foo' => 'bar']);

        $this->assertCount(1, $fwrite->getInvocations());
        $this->assertEquals(
            [STDERR, '['. date('Y-m-d H:i:s') .'] app.error: msg {"foo":"bar"}'],
            $fwrite->getInvocations()[0]
        );
    }
}
