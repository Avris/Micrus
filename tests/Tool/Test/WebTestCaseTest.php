<?php
namespace Avris\Micrus\Tool\Test;

use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Micrus\Model\User\MemoryUser;
use Avris\Micrus\Test\TestWebTestCase;
use Avris\Micrus\Tool\DirectoryCleaner;
use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\BrowserKit as Kit;

/**
 * @covers \Avris\Micrus\Tool\Test\WebTestCase
 */
class WebTestCaseTest extends TestCase
{
    /** @var TestWebTestCase */
    private $case;

    /**
     * @expectedException \Avris\Micrus\Exception\ConfigException
     * @expectedExceptionMessage You need to invoke WebTestCase::init() in your bootstrap file
     */
    public function testSetUpWithoutInit()
    {
        $this->case = new TestWebTestCase();
        TestWebTestCase::setUpBeforeClass();
    }

    public function testSetUpAndTearDown()
    {
        $app = $this->getMockBuilder(AbstractApp::class)->disableOriginalConstructor()->getMock();
        WebTestCase::init($app);
        $this->case = new TestWebTestCase();
        TestWebTestCase::setUpBeforeClass();

        $this->assertInstanceOf(WebClient::class, $this->case->getClient());

        TestWebTestCase::tearDownAfterClass();
        $this->assertInstanceOf(WebClient::class, $this->case->getClient());
    }

    public function testAsserts()
    {
        $app = $this->getMockBuilder(AbstractApp::class)->disableOriginalConstructor()->getMock();

        WebTestCase::init($app);
        $this->case = new TestWebTestCase();
        WebTestCase::setUpBeforeClass();

        $this->assertInstanceOf(WebClient::class, $this->case->getClient());

        $client = $this->getMockBuilder(WebClient::class)->disableOriginalConstructor()->getMock();
        $client->expects($this->any())->method('getRequest')
            ->willReturn(new Kit\Request('https://micrus.test/foobar', 'POST'));
        $client->expects($this->any())->method('getResponse')
            ->willReturn(new Kit\Response('foo', 404, ['Content-Type' => 'text/html']));
        $client->expects($this->any())->method('getCurrentUser')
            ->willReturn(new MemoryUser('tester', ['roles' => ['ROLE_TESTER']]));

        $r = new \ReflectionProperty(WebTestCase::class, 'client');
        $r->setAccessible(true);
        $r->setValue(null, $client);

        putenv('URL_SCHEME=https');
        putenv('URL_HOST=micrus.test');

        $this->assertPasses('assertUrl', ['/foobar']);
        $this->assertFails('assertUrl', ['/lorem'], 'Failed asserting that request URI https://micrus.test/foobar matches expected https://micrus.test/lorem');

        $this->assertPasses('assertUrlMatches', ['/%abar']);
        $this->assertFails('assertUrl', ['/%aem'], 'Failed asserting that request URI https://micrus.test/foobar matches expected https://micrus.test/%aem');

        $this->assertFails('assertSuccessful', [], 'Failed asserting that response status 404 matches expected 200');

        $this->assertPasses('assertStatus', [404]);
        $this->assertFails('assertStatus', [500], 'Failed asserting that response status 404 matches expected 500');

        $this->assertFails('assertContentTypeJson', [], 'Failed asserting that response content type text/html matches expected application/json');

        $this->assertPasses('assertContentType', ['text/html']);
        $this->assertFails('assertContentType', ['application/json'], 'Failed asserting that response content type text/html matches expected application/json');

        $this->assertFails('assertLoggedOut', [], 'Failed asserting that no user is logged in');

        $this->assertPasses('assertLoggedInAs', ['tester']);
        $this->assertFails('assertLoggedInAs', ['admin'], 'Failed asserting that current user is "admin"');

        $this->assertPasses('assertLoggedInAsRole', ['ROLE_TESTER']);
        $this->assertFails('assertLoggedInAsRole', ['ROLE_ADMIN'], 'Failed asserting that current user has role "ROLE_ADMIN"');

        $newClient = $this->getMockBuilder(WebClient::class)->disableOriginalConstructor()->getMock();
        $newClient->expects($this->any())->method('getRequest')
            ->willReturn(new Kit\Request('https://micrus.test/foobar', 'POST'));
        $newClient->expects($this->any())->method('getResponse')
            ->willReturn(new Kit\Response('foo', 200, ['Content-Type' => 'application/json']));
        $newClient->expects($this->any())->method('getCurrentUser')
            ->willReturn(null);

        $r->setValue(null, $newClient);

        $this->assertPasses('assertSuccessful');

        $this->assertPasses('assertStatus', [200]);
        $this->assertFails('assertStatus', [404], 'Failed asserting that response status 200 matches expected 404');

        $this->assertPasses('assertContentTypeJson');

        $this->assertPasses('assertContentType', ['application/json']);
        $this->assertFails('assertContentType', ['text/html'], 'Failed asserting that response content type application/json matches expected text/html');

        $this->assertPasses('assertLoggedOut');
        $this->assertFails('assertLoggedInAs', ['tester'], 'Failed asserting that user is logged in');
        $this->assertFails('assertLoggedInAsRole', ['ROLE_TESTER'], 'Failed asserting that user is logged in');
    }

    private function assertPasses($method, $params = [])
    {
        $this->case->gateway($method, $params);
    }

    private function assertFails($method, $params = [], $message = '')
    {
        try {
            $this->case->gateway($method, $params);
            $this->fail('Exception expected');
        } catch (ExpectationFailedException $e) {
            $this->assertStringStartsWith($message, $e->getMessage());
        }
    }

    public function testOnNotSuccessfulTest()
    {
        $app = $this->getMockBuilder(AbstractApp::class)->disableOriginalConstructor()->getMock();
        $cleaner = new DirectoryCleaner();
        $dir = __DIR__ . '/../../_help/fails';
        $cleaner->removeDir($dir);

        WebTestCase::init($app, $dir);
        $this->case = new TestWebTestCase();
        WebTestCase::setUpBeforeClass();

        $client = $this->getMockBuilder(WebClient::class)->disableOriginalConstructor()->getMock();
        $client->expects($this->any())->method('getResponse')
            ->willReturn(new Kit\Response('content'));

        $this->assertInstanceOf(WebClient::class, $this->case->getClient());

        $r = new \ReflectionProperty(WebTestCase::class, 'client');
        $r->setAccessible(true);
        $r->setValue(null, $client);

        $exception = new \RuntimeException('fuuu');
        try {
            $this->case->gateway('onNotSuccessfulTest', [$exception]);
        } catch (\Exception $e) {
            $this->assertSame($exception, $e);
        }

        $filepath = $dir . '/Avris_Micrus_Test_TestWebTestCase_.html';
        $this->assertFileExists($filepath);
        $this->assertEquals('content', file_get_contents($filepath));

        $cleaner->removeDir($dir);
    }
}
