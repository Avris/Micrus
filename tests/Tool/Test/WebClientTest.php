<?php
namespace Avris\Micrus\Tool\Test;

use Avris\Container\ContainerInterface;
use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Http\Cookie\Cookie;
use Avris\Http\Cookie\CookieBag;
use Avris\Http\Request\RequestInterface;
use Avris\Http\Response\Response;
use Avris\Http\Response\ResponseInterface;
use Avris\Micrus\Model\User\MemoryUser;
use Avris\Micrus\Tool\Security\SecurityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\BrowserKit as Kit;
use Symfony\Component\DomCrawler\Crawler;

/**
 * @covers \Avris\Micrus\Tool\Test\WebClient
 */
class WebClientTest extends TestCase
{
    const EXPECTED_RESPONSE = [
        'headers' => [
            'User-Agent' => 'Symfony BrowserKit',
            'Host'  => 'micrus.test',
        ],
        'server' => [
            'HTTPS' => true,
            'REQUEST_METHOD' => 'POST',
            'REQUEST_SCHEME' => 'https',
            'PATH_INFO' => '/test',
            'QUERY_STRING' => 'foo=bar',
            'REQUEST_URI' => '/test?foo=bar'
        ],
        'query' => [
            'foo' => 'bar'
        ],
        'data' => [
            'lorem' => 'ipsum'
        ],
        'file' => 'Fajel'
    ];

    public function testBasic()
    {
        $user = new MemoryUser('tester', []);

        $sm = $this->getMockBuilder(SecurityManagerInterface::class)->getMock();
        $sm->expects($this->once())->method('getUser')->willReturn($user);

        $container = $this->getMockBuilder(ContainerInterface::class)->getMock();
        $container->expects($this->once())->method('get')->with(SecurityManagerInterface::class)->willReturn($sm);

        $app = $this->getMockBuilder(AbstractApp::class)->disableOriginalConstructor()->getMock();
        $app->expects($this->any())->method('getContainer')->willReturn($container);
        $app->expects($this->once())->method('handle')->willReturnCallback(
            function (RequestInterface $request) {
                return new Response(
                    json_encode([
                        'headers' => $request->getHeaders()->all(),
                        'server' => $request->getServer()->all(),
                        'query' => $request->getQuery()->all(),
                        'data' => $request->getData()->all(),
                        'file' => $request->getFiles()->get('foo')->get('bar')->getClientFilename(),
                    ]),
                    200,
                    [],
                    new CookieBag([
                        'n' => new Cookie('n', 'v', 0, '', '', false, false),
                    ])
                );
            }
        );

        putenv('URL_SCHEME=https');
        putenv('URL_HOST=micrus.test');

        $client = new WebClient($app);
        $crawler = $client->request(
            'POST',
            '/test?foo=bar',
            ['lorem' => 'ipsum'],
            [
                'foo' => [
                    'bar' => [
                        'name' => 'Fajel',
                        'type' => 'image/jpeg',
                        'tmp_name' => '/tmp/abc.jpg',
                        'error' => 0,
                        'size' => 69,
                    ]
                ],
            ]
        );
        $this->assertInstanceOf(Crawler::class, $crawler);

        $response = $client->getResponse();
        $this->assertInstanceOf(Kit\Response::class, $response);
        $this->assertEquals(json_encode(self::EXPECTED_RESPONSE), $response->getContent());
        $cookies = $response->getHeader('Set-Cookie', false);
        $this->assertCount(1, $cookies);
        $this->assertInstanceOf(Kit\Cookie::class, $cookies[0]);
        $this->assertEquals('n', $cookies[0]->getName());
        $this->assertEquals('v', $cookies[0]->getValue());

        $micrusResponse = $client->getMicrusResponse();
        $this->assertInstanceOf(ResponseInterface::class, $micrusResponse);
        $this->assertEquals(json_encode(self::EXPECTED_RESPONSE), $micrusResponse->getContent());

        $this->assertInstanceOf(ContainerInterface::class, $client->getContainer());

        $this->assertInstanceOf(RequestInterface::class, $client->getMicrusRequest());

        $this->assertSame($user, $client->getCurrentUser());
    }
}
