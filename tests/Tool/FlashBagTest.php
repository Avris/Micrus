<?php
namespace Avris\Micrus\Tool;

use Avris\FunctionMock\FunctionMock;
use Avris\Micrus\Test\FakeSession;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\FlashBag
 */
class FlashBagTest extends TestCase
{
    public function testFlashBag()
    {
        $session = new FakeSession(['_flash_bag' => [['type' => 'success', 'message' => 'ok']]]);

        $flashBag = new FlashBag($session);
        $flashBag->add('success', 'again', false);
        $flashBag->add('success', 'deferred');

        $this->assertEquals([
            ['type' => 'success', 'message' => 'ok'],
            ['type' => 'success', 'message' => 'again'],
        ], $flashBag->all());

        $this->assertEquals([
            '_flash_bag' => [],
        ], $session->all());

        $flashBag->update();

        $this->assertEquals([
            '_flash_bag' => [['type' => 'success', 'message' => 'deferred']],
        ], $session->all());
    }
}
