<?php
namespace Avris\Micrus\Tool\Config;

use Avris\Bag\Bag;
use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Micrus\Test\TestModuleFlex;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Config\ConfigLoader
 */
class ConfigLoaderTest extends TestCase
{
    public function testLoad()
    {
        $module = new TestModuleFlex('mod_name', __DIR__ . '/../../_help/');

        $loader = new ConfigLoader(new ConfigBag('test', true, __DIR__,[]));
        $loader->load($module, 'config');
        $loader->load($module, 'config/test');

        $expectedConfig = new Bag([
            'foo' => [
                'bar' => 'BAR!',
                'baz' => 'BZZZ!',
                'dir' => $module->getDir(),
            ],
            'services' => [
                'logger1' => 'L1',
                'logger2' => 'L2',
                'service1' => 'S1',
                'service2' => 'S2',
            ],
        ]);

        $this->assertEquals($expectedConfig, $loader->get());
    }
}
