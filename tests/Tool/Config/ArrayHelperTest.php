<?php
namespace Avris\Micrus\Tool\Config;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Config\ArrayHelper
 */
class ArrayHelperTest extends TestCase
{
    public function testMerge()
    {
        $helper = new ArrayHelper();

        $arr1 = [
            'foo' => 'FOO',
            'bar' => [
                'a' => 'A',
                'b' => 'B',
            ],
            'wrong' => 'WRONG',
        ];

        $arr2 = [
            'baz' => 'BAZ',
            'bar' => [
                'b' => 'BBB',
                'c' => 'C',
            ],
            '_delete' => ['wrong'],
        ];

        $expected = [
            'foo' => 'FOO',
            'bar' => [
                'a' => 'A',
                'b' => 'BBB',
                'c' => 'C',
            ],
            'baz' => 'BAZ',
        ];

        $result = $helper->merge($arr1, $arr2);

        $this->assertEquals($expected, $result);
    }

    public function testExtras()
    {
        $helper = new ArrayHelper();

        $data = [
            '_defaults' => [
                'public' => false,
            ],
            '_import' => 'ext',
            'foo' => [
                'a' => 'faa',
                'public' => true,
            ],
            'bar' => [
                'a' => 'A',
                'b' => 'B',
            ],
        ];

        $expected = [
            'ext' => [
                'osiem' => 8,
            ],
            'foo' => [
                'a' => 'faa',
                'public' => true,
            ],
            'bar' => [
                'public' => false,
                'a' => 'A',
                'b' => 'B',
            ],
        ];

        $result = $helper->extras($data, function ($filename) {
            return [ $filename => ['osiem' => 8] ];
        });

        $this->assertEquals($expected, $result);
    }
}
