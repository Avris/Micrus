<?php
namespace Avris\Micrus\Tool\Config;

use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Micrus\Test\TestModuleFlex;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\Config\ConfigBag
 */
class ConfigBagTest extends TestCase
{
    public function testBasic()
    {
        $bag = new ConfigBag('test', true, __DIR__, [AbstractApp::CACHE_DIR => 'var/cache']);

        $this->assertEquals('test', $bag->getEnv());
        $this->assertTrue($bag->isDebug());

        $this->assertEquals(__DIR__, $bag->getDir());
        $this->assertEquals(__DIR__, $bag->getDir(AbstractApp::PROJECT_DIR));
        $this->assertEquals(__DIR__ . '/var/cache', $bag->getDir(AbstractApp::CACHE_DIR));

        putenv("CB_TEST=abc");

        $this->assertEquals(
            'MOD_DIR|MOD_NAME|test|1|' . date('Y-m-d') . '|' . __DIR__ . '/var/cache|abc',
            $bag->replaceParameters(
                '%MODULE_DIR%|%MODULE_NAME%|%APP_ENV%|%APP_DEBUG%|%DATE(Y-m-d)%|%CACHE_DIR%|%CB_TEST%',
                new TestModuleFlex('MOD_NAME', 'MOD_DIR')
            )
        );
    }

    /**
     * @expectedException \Avris\Micrus\Exception\NotFoundException
     * @expectedExceptionMessage Directory "DIR_FOO" is not defined
     */
    public function testDirInvalid()
    {
        $bag = new ConfigBag('test', true, __DIR__, []);

        $bag->getDir('DIR_FOO');
    }

    /**
     * @expectedException \Avris\Micrus\Exception\NotFoundException
     * @expectedExceptionMessage Undefined environmental variable CB_NONEXISTENT
     */
    public function testParameterInvalid()
    {
        $bag = new ConfigBag('test', true, __DIR__, []);

        $bag->getParameter('CB_NONEXISTENT');
    }
}
