<?php
namespace Avris\Micrus\Tool;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Micrus\Tool\DirectoryCleaner
 */
class DirectoryCleanerTest extends TestCase
{
    const DIR = __DIR__ . '/../_help/removal';

    public function testCleaner()
    {
        $this->assertFalse(is_dir(self::DIR));

        $cleaner = new DirectoryCleaner();
        $this->assertTrue($cleaner->removeDir(self::DIR));
        $this->assertFalse(is_dir(self::DIR));

        $this->assertTrue($cleaner->emptyDir(self::DIR));
        $this->assertFalse(is_dir(self::DIR));

        $this->populate();

        $this->assertTrue($cleaner->emptyDir(self::DIR));
        $this->assertTrue(is_dir(self::DIR));
        $this->assertFalse(is_dir(self::DIR . '/foo'));

        rmdir(self::DIR);
        $this->populate();

        $this->assertTrue($cleaner->removeDir(self::DIR));
        $this->assertFalse(is_dir(self::DIR));
        $this->assertFalse(is_dir(self::DIR . '/foo'));
    }

    private function populate()
    {
        mkdir(self::DIR);
        mkdir(self::DIR . '/foo');
        mkdir(self::DIR . '/bar');
        file_put_contents(self::DIR . '/foo/ok.txt', 'ok');
        symlink(self::DIR . '/foo/ok.txt', self::DIR . '/bar/link.txt');

        $this->assertTrue(is_dir(self::DIR));
    }
}
