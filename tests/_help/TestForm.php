<?php
namespace Avris\Micrus\Test;

use App\Entity\Testie;
use Avris\Http\Request\RequestInterface;
use Avris\Forms\Form;

final class TestForm extends Form
{
    /** @var bool */
    private $configured = false;

    /** @var Testie|null */
    protected $object = null;

    /** @var RequestInterface|null */
    private $request = null;

    protected function configure()
    {
        $this->configured = true;
    }

    protected function setCsrfToken()
    {
    }

    public function bindObject($object)
    {
        $this->object = $object;

        return $this;
    }

    public function bindRequest(RequestInterface $request): bool
    {
        $this->request = $request;
        $this->object->setId($request->getData()->get('id', 0));
        $this->object->setValue($request->getData()->get('value'));

        return true;
    }

    public function isValid($ignoreBound = false): bool
    {
        return $this->configured
            && $this->object
            && $this->request
            && $this->request->getMethod() === 'POST'
            && $this->request->getData()->get('id')
            && $this->request->getData()->get('value');
    }

    public function buildObject($object = null)
    {
        if (!$this->configured) {
            throw new \RuntimeException('Form not configured');
        }

        if (!$this->object) {
            throw new \RuntimeException('Form object not bound');
        }

        if (!$this->request) {
            throw new \RuntimeException('Form request not bound');
        }

        return $this->object;
    }
}
