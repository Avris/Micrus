<?php
namespace Avris\Micrus\Test;

use Avris\Micrus\Tool\Test\WebTestCase;

class TestWebTestCase extends WebTestCase
{
    public function gateway($method, $params = [])
    {
        return call_user_func_array([$this, $method], $params);
    }

    public function getClient()
    {
        return static::$client;
    }
}
