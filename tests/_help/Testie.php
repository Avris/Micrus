<?php
namespace App\Entity;

final class Testie
{
    /** @var int */
    private $id;

    /** @var mixed|null */
    private $value;

    public function __construct($id = 0, $value = null)
    {
        $this->id = $id;
        $this->value = $value;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value): self
    {
        $this->value = $value;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->value;
    }
}
