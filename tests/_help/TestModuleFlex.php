<?php
namespace Avris\Micrus\Test;

use Avris\Micrus\Bootstrap\ModuleInterface;

class TestModuleFlex implements ModuleInterface
{
    /** @var string */
    private $name;

    /** @var string */
    private $dir;

    public function __construct(string $name, string $dir)
    {
        $this->name = $name;
        $this->dir = $dir;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDir(): string
    {
        return $this->dir;
    }
}
