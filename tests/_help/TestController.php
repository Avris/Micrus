<?php
namespace Avris\Micrus\Test;

use App\Entity\Testie;
use Avris\Micrus\Controller\Controller;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Http\Response\Response;
use Avris\Container\ContainerInterface;
use Avris\Micrus\Tool\FlashBag;

class TestController extends Controller
{
    public function handlerAction(int $id)
    {
        return new Response($id, 204);
    }

    public function handlerViewAction(int $id)
    {
        return $id;
    }

    public function testAction()
    {
        $this->addFlash(FlashBag::INFO, 'INFO', false);

        return [
            $this->get('env'),
            $this->trigger(new TestEvent('test')),
            $this->getProjectDir(),
            $this->get(FlashBag::class)->all(),
            $this->getUser()->getIdentifier(),
            $this->isGranted('ROLE_TESTER'),
            $this->canAccess('canTest', $this),
            $this->generateUrl('test', ['foo' => 'ok']),
        ];
    }

    public function formAction(RequestInterface $request)
    {
        $testie = new Testie();
        $saved = false;

        $form = $this->form(TestForm::class, $testie, $request);
        if ($this->handleForm($form)) {
            $saved = true;
        }

        return ['form' => $form, 'testie' => $testie, 'saved' => $saved];
    }

    public function formInvalidAction()
    {
        $this->form(RequestProviderInterface::class);
    }

    public function testRenderAction()
    {
        $this->setCookie('name', 'value');

        return $this->render(['foo' => 'bar']);
    }

    public function testRenderJsonAction()
    {
        $this->setCookie('name', 'value');

        return $this->renderJson(['foo' => 'bar']);
    }

    public function testRedirectAction()
    {
        $this->setCookie('name', 'value');

        return $this->redirect('https://avris.it');
    }

    public function testRedirectToRouteAction()
    {
        $this->setCookie('name', 'value');

        return $this->redirectToRoute('test', ['foo' => 'ok']);
    }

    public function testMatcher1Action()
    {
    }

    public function testMatcher2Action(TestService $service, Testie $testie = null)
    {
    }

    public function testMatcher3Action(ContainerInterface $container)
    {
    }

    public function testMatcher4Action(TestService $service, Testie $testie)
    {
    }

    public static function testMatcherStaticAction(Testie $testie, $foo)
    {
    }

    public function __invoke(Testie $testie)
    {
    }
}
