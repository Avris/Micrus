<?php
namespace Avris\Micrus\Test;

use Avris\Micrus\Controller\Routing\Model\CompiledRoute;
use Avris\Micrus\Controller\Routing\Model\Route;
use Avris\Micrus\Controller\Routing\Service\RouterExtension;

final class TestRouterExtension implements RouterExtension
{
    public function add(Route $route): Route
    {
        return new Route(
            'added_' . $route->getName(),
            $route->getPattern(),
            $route->getTarget()
        );
    }

    public function generate(string $name, ?CompiledRoute $route, array $params = []): array
    {
        if ($name === 'generateTest') {
            return ['/generated', $route, $params];
        }

        $params['foo'] = 'bar';

        return [null, $route, $params];
    }
}
