<?php
namespace Avris\Micrus\Test;

use Symfony\Component\Console\Command\Command;

final class TestCommand extends Command
{
    protected function configure()
    {
        $this->setName('test');
    }
}
