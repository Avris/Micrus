<?php
namespace TestApp;

use Avris\Micrus\Bootstrap\AbstractApp;
use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;
use Avris\Micrus\Tool\Cache\Cacher;
use Avris\Micrus\Tool\Cache\CacherInterface;
use Symfony\Component\Cache\Adapter\NullAdapter;

class TestApp extends AbstractApp implements ModuleInterface
{
    use ModuleTrait;

    protected function registerModules(): iterable
    {
        yield $this;
    }

    protected function buildProjectDir(): string
    {
        return dirname(__DIR__);
    }

    protected function buildCacher(): CacherInterface
    {
        return new Cacher(new NullAdapter(), $this->configBag->isDebug());
    }
}
