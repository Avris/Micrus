<?php
namespace TestApp;

use Avris\Micrus\Controller\ControllerInterface;
use Avris\Http\Response\Response;
use Avris\Micrus\Exception\Http\NotFoundHttpException;

class HomeController implements ControllerInterface
{
    public function homeAction()
    {
        return new Response('OK');
    }

    public function failAction()
    {
        throw new NotFoundHttpException('Nah');
    }
}
