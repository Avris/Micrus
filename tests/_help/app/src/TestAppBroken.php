<?php
namespace TestApp;

use Avris\Container\ContainerInterface;
use Avris\Http\Request\RequestInterface;

class TestAppBroken extends TestApp
{
    public function warmup(RequestInterface $request = null): ContainerInterface
    {
        throw new \RuntimeException('Emulate broken warmup');
    }
}
