<?php
namespace Avris\Micrus\Test;

use Psr\Log\AbstractLogger;

class NoopLogger extends AbstractLogger
{
    public function log($level, $message, array $context = array())
    {
    }
}
