<?php
namespace Avris\Micrus\Test;

use Avris\Micrus\Model\User\UserInterface;
use Avris\Micrus\Tool\Security\GuardInterface;

final class TestGuard implements GuardInterface
{
    /** @var string */
    private $supportedCheck;

    /** @var mixed */
    private $supportedObejct;

    /** @var UserInterface|null */
    private $allowedUser;

    public function __construct(string $supportedCheck, $supportedObejct, ?UserInterface $allowedUser)
    {
        $this->supportedCheck = $supportedCheck;
        $this->supportedObejct = $supportedObejct;
        $this->allowedUser = $allowedUser;
    }

    public function supports(string $check, $object = null): bool
    {
        return $this->supportedCheck === $check && $this->supportedObejct === $object;
    }

    public function check(UserInterface $user, string $check, $object = null): bool
    {
        return $this->allowedUser === $user;
    }
}
