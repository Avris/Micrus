<?php
namespace Avris\Micrus\Test;

use Avris\Dispatcher\Event;

final class TestEvent extends Event
{
    private $name;

    public $value;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setValue($value): Event
    {
        $this->value = $value;

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

}
