<?php
namespace Avris\Micrus\Test;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Tool\Config\ParametersProvider;

class TestModuleProvider implements ParametersProvider
{
    public function getName(): string
    {
        return __NAMESPACE__;
    }

    public function getDir(): string
    {
        return __DIR__;
    }

    public function getParametersDefaults(): array
    {
        return [
            'FOO' => 'foo',
            'BAR' => 'bar',
        ];
    }
}
