<?php
namespace Avris\Micrus\Test;

use App\Entity\Testie;
use Avris\Http\Request\Request;
use Avris\Micrus\Model\MatchProvider;
use Avris\Bag\QueueBag;

class TestMatchProvider implements MatchProvider
{
    public function supports(\ReflectionParameter $parameter): bool
    {
        return $parameter->getClass() && in_array($parameter->getClass()->getName(), [
            TestService::class,
            Testie::class
        ]);
    }

    public function fetch(\ReflectionParameter $parameter, QueueBag $tags)
    {
        if ($parameter->getClass()->getName() === TestService::class) {
            return new TestService();
        }

        if ($tags->isEmpty() && $parameter->allowsNull()) {
            return null;
        }

        list(, $value) = $tags->dequeue();

        return new Testie($value);
    }
}
