<?php
namespace Avris\Micrus\Test;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;

class TestModule implements ModuleInterface
{
    use ModuleTrait;
}
