## Console tasks ##

All the actions that don't require GUI, like administrative stuff (load fixtures, manual password generation)
or scheduled jobs (using cron) can be run from the console on a server.
Just `cd` to the project directory and run `bin/micrus` (or `php bin/micrus` on Windows).
You will see a list of available commands to run.

Micrus uses a great tool for that -- [Symfony Console Component](http://symfony.com/doc/current/components/console/introduction.html).
Head to its documentation for more information.

To register a command just create a service extending `Command` and tagged `command` (will be tagged automatically).
 