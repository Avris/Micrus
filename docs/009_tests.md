## Testing

Micrus recommends using [PHPUnit](http://phpunit.de) for both unit and functional tests.
They should be put in the `tests` directory and should keep the same namespace structure as in `src` folder.

For the functional tests, Micrus provides a wrapper for
[Symfony BrowserKit](http://symfony.com/components/BrowserKit)
and uses [Symfony DomCrawler](http://symfony.com/doc/current/components/dom_crawler.html).
So the usage is quite similar to what's presented in the
[Symfony documentation](http://symfony.com/doc/current/book/testing.html).

Remember to explicitly include dependencies:

    composer require --dev symfony/browser-kit symfony/css-selector

To use it, you need to initialise the app for tests in your `tests/_bootstrap.php` file:

    $app = new \App\App('test', true);
    \Avris\Micrus\Tool\Test\WebTestCase::init($app, __DIR__ . '/../tests/_output/fail'); 
    
The second argument is the directory where the HTTP responses of the failed tests will be saved for the purposes of debugging.

Your functional test cases have to extend `Avris\Micrus\Tool\Test\WebTestCase`.
A client will be created for the whole test suite so that it can be shared between tests.
It is accessible via `static::$client`. 

You should create *one story per class*.

You can access the container via `static::$client->getContainer()`.

For performance sake consider running your functional tests on a in-memory SQLite database
or encapsulating your stories in a transaction. 

For examples of functional and unit tests, please turn to the [Micrus Demo Project](https://gitlab.com/Avris/Micrus-Demo).
