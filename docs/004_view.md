## View ##

Micrus is template engine agnostic
(plain PHP and [Twig](gitlab.com/Avris/Micrus-Twig) are supported).

Whichever one you're using, those Micrus-specific variables, functions and filters will be available:

* variable **app** with such methods:
 * **getUser** -- currently logged in user, or null
 * **getFlashBag** -- see [Routing and controllers](#routing-and-controllers)
 * **getRequest**,
 * **getRouteMatch**,
 * **getSession**,
* function **route(name, params = [])** which generates an URL for the given route name and the (optional) array of parameters,
* function **routeExists(name)**,
* function **asset(filename)** which generates an filename for an asset that will work regardless of current URL or app's position in the filesystem, for instance: `<script src="{{ asset('asset/main.js') }}"></script>`,
* function **isGranted(role)**,
* function **canAccess(check, object)**,

Note that Micrus's extensions use Twig, not plain PHP.
They can provide their own variables, functions, filters etc. to Twig.

We recommend using manifest-based asset versioning.
If your asset compiling tool, like [Webpack](https://github.com/webpack/webpack), generates a `manifest.json` file,
put it in `public/assets/manifest.json` (or whichever part you specify in `@config.assets.manifest`).
Micrus will use that file to locate the assets referenced with `{{ asset('filename.js') }}`.

If the manifest is not there, it will just look for them in the `public` directory.
