## Services ##

### Dependency Injection ###

Services are classes that are supposed to be shared throughout the application,
that do a specific job (like handling event or sending emails)
and that generally (but necessarily) have at most one instance in the system.

Micrus uses [Avris Container](https://gitlab.com/Avris/Container).
Please check out its documentation to learn about how it works.
All that Micrus itself adds to this Container implementation
is the ability to configure it using `.yml` files like this:

    App\:
      dir: '%MODULE_DIR%/src/'
      exclude:
        - '#^Entity/#'

    App\Service\Mailer:
      params: [@config.parameters.?mailer]
      calls:
        - [setLogger, [@?Psr\Log\LoggerInterface]]
      tags: [defaultParameters]

    App\Service\ResponseListener:
      arguments: 
        $logger: @Psr\Log\LoggerInterface
      events: [response]

    App\Service\WaveTwigExtension:
      tags: [twigExtension]

    countedProduct:
      class: App\Service\CountedProduct

    countedProductFactory:
      class: App\Service\CountedProduct
      factory: true
