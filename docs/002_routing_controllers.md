## Routing and controllers

### Routing

Routing is a table of rules that determine which URL should be handled by which controller.
It can be specified in **config/routing.yml** and looks like this:

	home:
	  pattern: /
	  target: App\Controller\HomeController/home

	helloUser:
	  pattern: /hello/{id}
	  target: App\Controller\HomeController/helloUser
	  requirements: { id: \d+ }

	helloName:
	  pattern: /hello/{name}
	  target: App\Controller\HomeController/helloName
	  defaults: { name: Stranger }
	  methods: GET
	  
Or with a shorthand notation:

	home: 		/ -> Home/home
	helloUser:	/hello/{int:id} -> Home/helloUser
	helloName:	GET /hello/{name=Stranger} -> Home/helloName

There should always be a route called `home` (it's used for instance as a fallback redirect after logging out
or switching locale of the page). For every route, fields `pattern` and `target` are required.

In our example, when URL `/` is requested, router gives control to the controller `App\Controller\HomeController/home`
(that means: service `App\Controller\HomeController`, method `homeAction`).

If the requested URL doesn't match `/`, the next route is checked.
This one catches everything that starts with `/hello/` and then follows with one or mode decimals (because of the `requirements` field).
URLs `/hello/5` and `/hello/123` do match, but `/hello/foo`, `/hello/123/456` or `/hello` don't.

The next route catches everything like `/hello/Thomas`, `/hello/Josh` or `/hello/Meteo` or even `/hello` alone
 -- it this case parameter `name` is set to default value `Stranger`.

Example flows:
 * for URL `/` first route is matched,
 * for URL `/hello/25` first route is not matched, but second one is, so the router sends to the controller ID of a user to welcome,
 * for URL `/hello/Kate` first route is matched, second one would be, if it wasn't for requirements, but the third one is -- so controller receives the name to greet the user with,
 * for URL `/whatever` first rule is checked, then the second, then the third, but none is matched, so `NotFoundHttpException` is thrown.
 
If `method` is set (to GET, POST, PUT or DELETE), router will only accept that given HTTP method for this route.
 
Routes can also be defined using [Annotations](https://gitlab.com/Avris/Micrus-Annotations).
 
### Handling requests

The simplest controller to handle the request matching our `helloName` route looks like this:

	public function helloNameAction(string $name)
    {
        return new Response(sprintf('Hello, %s!', $name);
    }

It receives `$name` from the URL and creates a `Response` object.
Every controller must return an object implementing `Avris\Micrus\Controller\Http\ResponseInterface`.

There are some shorthands available for generating responses:

  * `$this->render(['name' => $name], 'Foo/bar');` -- will use the templating engine to render the `Foo/bar` template with the `name` variable. 
  * `$this->redirect('https://google.com')`
  * `$this->redirectToRoute('helloName', ['name' => 'Sir ' . $name])`
  * `$this->renderJson($array)`
  
The base `Controller` class also provides some other helpers:

  * `$this->get('service_name')` -- fetches service from the Container
  * `$this->trigger($event)` -- triggers an event in the Dispatcher
  * `$this->getProjectDir()`
  * `$this->setCookie('name', 'value')`
  * `$this->addFlash('success', 'message')` -- saves a message in the session to be displayed once in the next request and removed.
  * `$this->getUser()`
  * `$this->isGranted('ROLE_ADMIN')`
  * `$this->canAccess('edit', $post)`
  * `$this->generateUrl('routeName', ['foo' => 'bar'])`
  * `$this->form($className, $object, $request)` -- see [Avris Forms](https://gitlab.com/Avris/Forms)
  * `$this->handleForm($form)` -- see [Avris Forms](https://gitlab.com/Avris/Forms)

### Automatching

Based on what parameters does your action expect, Micrus can automatically pass some values to it. For example:

    // route: /test/{id}/{name}
    public function testAction(RequestInterface $request, User $user, string $name) { ... } 

This action requires `RequestInterface`, which is a public service in the container, so it will be passed.
The next value has a type `User`, and if you have [Micrus Doctrine](https://gitlab.com/Avris/Micrus-Doctrine) installed
and `User` is a Doctrine entity, then it will be looked up in the database by `id`.
The `$name` parameter will be simply transfered from the URL to the action.

You can add your own automatchers creating a service that implements `MatchProvider` interface 
and is tagged `matchProvider` (will be tagged automatically).
