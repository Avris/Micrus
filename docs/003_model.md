## Model ##

Micrus is ORM agnostic, so you can use whatever ORM you want
([Doctrine](https://gitlab.com/Avris/Micrus-Doctrine) is supported).
For all the ORM-specific stuff, check out their documentation.

All the Entity classes should to be put in the `Entity` sub-namespace of a given module.
