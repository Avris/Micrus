### Disclaimer ###

I believe that an example is worth a thousand words.
That's why **[Micrus Demo Project](https://gitlab.com/Avris/Micrus-Demo)
is the main source of the documentation** for the framework,
while the following document might be sketchy and incomplete.

And also: Micrus should be simple enough to just quickly check out parts of its source
instead of reading the docs.

## Getting started ##

### MVC ###

Micrus is a **full-stack MVC framework**. What does that mean?

**Framework** is a special kind of library, that's supposed to organize your code,
enforce the project's workflow and integrate it with other libraries.

One of the most popular ways of organizing the code of web applications
 is the [**MVC** architectural pattern](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller),
which stands for **Model-View-Controller**.
It means that three main "layers" of the project are:

* **Model** - an abstraction layer for the objects that you operate on,
 like "Article", "Comment", "User" or "Message".
 Model layer takes care of managing their content and persisting it in the database.
* **Controller** - handles the user's request, does proper operations on the model and returns the result to the View.
* **View** - this layer receives data from the Controller
 and takes care of rendering it and displaying the response for the user.

There are microframeworks and full-stack frameworks.
The first one is just a loose frame that doesn't include a lot of features and leaves the developer with big possibilities of adjustment.
The latter is fully equipped with features and has a concrete structure.
Micrus tries to be small and simple, while also being fully stacked and extendable.

### HTTP and application flow ###

When opening a website in the browser,
your computer communicates with the server via the **[HTTP protocole](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol)**.
It's fairly simple, text-based and stateless.
The browser sends a **Request**, which is just a text document containing set of key-value headers, and a content.

Let's say it's a request with `POST` method to `http://myawesomepage.com/post/14/edit`
with content that is an array of values such as title, text, publication date etc.
Micrus wraps this request in an abstraction layer,
allowing you to easily access all the received data.

Micrus app has a routing table that binds specific URLs to specific controllers.
This table says that the URL `POST /post/14/edit` should be handled by the action `editAction` inside the controller `PostController`.
Before executing that controller, Micrus might execute additional code (see: [Event Dispatcher](#event-dispatcher)). 
For instance, it checks, if the user has the credentials to edit posts.
It retrieves a session key from headers and either authorises the user to edit the post, or redirects to the login page.

The controller receives a Request object and has to return a Response object.
It's gonna contain the HTML code with, among others, a message that the post has been successfully updated (or not).
It might also set some values in the headers or modify the default `200` response code
(like the famous `404` if the post with id=14 doesn't exist in the database -- but that case would already be handled by Micrus).
So, to sum up: the Controller receives a Request, then does some operations on the Model(s) (maybe using some external services),
asks the View to render an expected HTML code, and then returns a Response with that HTML.

Read more about the Request/Response abstraction layer in [Avris Http](https://gitlab.com/Avris/Http).

### Directory structure

	- public
	   - index.php
	- config
       - services.yml
       - prod
          - services.yml
	- src
	   - Command
	   - Controller
	   - Form
	   - Entity
	   - Service
	   - App.php
    - templates
	     - layout.html.twig
    - translations
       - app.en.yml
	- bin
	  - micrus
    - assets
	- var
	  - cache
	  - logs
	- tests
	- vendor
	- .env

The **public** directory should be the only one that's published by the web server.
It contains all the frontend data, such as css files, scripts and images, as well as the front controller.
Front controller (`index.php`) is a tiny php files -- a "gateway" to all the rest of the backend code,
which is hidden in different directories, so in case of server misconfiguration it doesn't leak.

**src** directory is a place for your PHP code.
It corresponds to the `App` namespace
(PSR-4, for instance class `App\Entity\Post` should be located in `src/Entity/Post.php`).

The following structure is recommended, but not always required
(any service can be a controller or a command, regardless of its location
-- but on the other hand [Micrus Doctrine](https://gitlab.com/Avris/Micrus-Doctrine)
looks for entities only in the `Entity` directory):
* **src/Entity** contains entities, classes that represent objects like a post, a comment, an user, a token, etc.
  They may or may not be stored in the database.
* **src/Controller** contains the classes that receive a Request and produce a Response.
* **src/Form** is for the abstraction layer between model and it's representation in html `<form>`-related tags
  (requires [Avris Forms](https://gitlab.com/Avris/Forms)).
* **src/Service** contains the [services](#services) -- classes that are supposed to be shared between the application modules
 (controllers, views, other services), that do a specific job (like handling events or sending emails),
 and that generally (but necessarily) have at most one instance in the system.
* **src/Command** directory is a place for the classes with code to be executed without graphical interface, using the `bin/micrus` file.

**templates** contains the templates (`.phtml`, `.twig` or else, depending on which template engine you're using).

**config**, obviously, contains all the config files for your project.
They use [YAML format](http://yaml.org/).
More about the config in the [config section](#getting-started-config). 

The **vendor** directory contains all the 3-rd party code: Micrus itself and other libraries.

The **var** directory should contain all the files that get created during runtime:
cache, logs, user uploads etc. *Make sure that is writable by the server!*

The **tests** directory should contain all the tests for your project, and preserve the namespace structure of the **src** dir.

Directory **bin** should contain the console executables.
Located there is `bin/micrus`, which executes Micrus-related commands like clearing cache or creating the database schema.
This also includes your own custom commands.

Only **public** and **var** directories could be writable -- the rest should be treated as read-only once deployed to a server.

### Modules

Micrus is built out of modules so that functionalities be easily added, removed and switched.
A module contains a set of files like PHP classes, tests, Twig templates, translations, configs etc.
The best thing about the modules is that **everything is a module**, including your whole app and Micrus itself.
This means that the directory structure of a module is the same as the one presented above.
All the configs, templates, translations etc. provided by modules can be loaded in a consistent manner.

For instance the `Templater` service receives a list of all the modules
and checks all of them for the existence of a `templates` directory.
If it's there, this directory is registered as a source of templates.

You can register and unregister the enabled modules in the `src/App.php` file:

    protected function registerModules(): iterable
    {
        // \Avris\Micrus\MicrusModule is added automatically as the first one
        yield new \Avris\Micrus\Twig\TwigModule;
        yield new \Avris\Micrus\Doctrine\DoctrineModule;
        yield new \Avris\Localisator\LocalisatorModule;
        yield new \Avris\Forms\FormsModule;
        yield $this;
    }

Modules lower in this least overwrite the ones higher.
For instance `MicrusModule` can define a service,
but `TwigModule` could modify this definition a little bit,
and then your `App` will unregister this service completely.
That's why your `App` module (`$this`) should be the last one,
so that you can overwrite whichever default config, templates etc. are provided by external libraries.  

### Environmental variables

Sensitive and environment-dependent configs (like database connection, mailer configuration etc.)
should be kept in the environment variables.
On the production/staging/qa servers they should be provided by the webserver,
but on local dev environment they could be loaded form the `.env` using [Avris Dotenv](https://gitlab.com/Avris/Dotenv).

When you run `bin/env`, Micrus will ask all registered modules for the expected environmental variables,
will ask you for their values, and store them in the `.env` file.

Two important env vars are `APP_ENV` and `APP_DEBUG`.
The first one defines a name of your environment (`dev`, `test`, `prod` etc.) so that you can use different configs for different environments.
The other one is boolean (`0` or `1`) and determines: whether debug messages or nice 404 etc. pages should be shown, whether to cache configs or not etc.

You can pass an env var to a service by using its name between percentages: `%APP_ENV%`.
They can also be autowired, if an argument name in you constructor starts with `$env`: `bool $envAppDebug`.

### Config

Micrus's config is composed out of the parts provided by its modules.
For instance if one one module provides the following files:

`config/services.yml`:

    Module1\Service:
        public: true
        
    Avris\Micrus\View\TemplaterInterface: Avris\Micrus\View\Templater
        
`config/mailer.yml`:

    sender: foo@bar.com

And the other module provides the following `config/services.yml`:

    Avris\Micrus\View\TemplaterInterface: Module2\Templater
    Module2\Templater:
        arguments:
            $env: '%APP_ENV%'

Then the resulting config variable will look like this:

    [
        'services' => [
            'Module1\Service' => ['public' => true],
            'Avris\Micrus\View\TemplaterInterface' => 'Module2\Templater',
            'Module2\Templater' => [ 'arguments' => ['$env' => 'prod'] ],
        ],
        'mailer' => [
            'sender' => 'foo@bar.com',
        ],
    ]
    
If a module provides a `config/prod/services.yml` file, then its content will also be merged to the config,
but only if `APP_ENV` = `prod`.

You can import other config files:

`config/services.yml`

    _import: ['_logger']

This will paste the full content of `config/_logger.yml` to `config/services.yml`.
There won't be a separate `_logger` entry in the config, since files starting with `_` are ignored.

To delete an entry that was added by another module, you can specify it like this:

    _delete: [key_to_be_removed]
    
You can also specify default values for all the entries in a specific file:

`services.php`:

    _defaults:
        public: true
        
All the services defined in this file will be public by default.

You can pass part of the config to a service by using the syntax: `@config.mailer`.
It can also be autowired, if an argument in you constructor is a `Bag` object and its name starts with `$config`:
`Bag $configMailer`.
