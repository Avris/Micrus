### Event Dispatcher ###

Micrus uses [Avris Dispatcher](https://gitlab.com/Avris/Dispatcher).
Please check out its documentation to learn about how it works.

Micrus provides and triggers some events that you can listen to and intercept the execution of your app:

 * `warmup` -- before handling an event
 * `request` -- before a request gets handled at all
 * `controller` -- right before a controller gets executed (can for instance switch to a different one)
 * `view` -- right after a controller gets executed (can for instance convert its return value to a `ResponseInterface` instance)
 * `response` -- right before a response is sent (can for instance add some additional headers to all responses)
 * `terminate` -- after sending a response (can for instance send an email in the background)
 * `error` -- when an exception is thrown or an error triggered (can for instance generate an error page or send a notification email)

Also for the CLI commands
 * `consoleWarmup` -- before running a command
 * `cacheClear` -- when `cache:clear` is run
 * `cacheWarmup` -- when `cache:warmup` is run

To create your own event, just extend `Avris\Micrus\Tool\Bootstrap\Event`.
